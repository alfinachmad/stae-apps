<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| $T@32017
| claudio
*/

define('TITLE_APP', 'SECRETARIADO TÉCNICO DA ADMINISTRAÇÃO ELEITORAL');
define('TITLE_ACRONYM', 'STAE');
define('SESSION_DEFAULT', getenv('SESSION_DEFAULT_TOKEN'));
define('SESSION_TABULASI', getenv('SESSION_TABULASI'));

// Route Untuk Alias:
Route::get('/', function () {
    return view('welcome');
});
Route::get('/phpinfo', function(){
    return phpinfo();
});
Route::get('/exp', 'STAEExperimentController@index');
Route::get('/clockinfo', 'Informasi\MediaController@_clockinfo');
Route::get('/media', function(){
    return redirect()->route('media-informasi', ['tipe' => 'media']);
});
Route::get('/aset', function(){
    return redirect()->route('aset-default');
});
Route::get('/lt', function(){
    return redirect()->route('pr2017ltable');
});
Route::get('/counting-down/', function(){
    return redirect()->route('pr2017');
});
Route::get('/t{type}{period}{round?}', function($type,$period,$round=''){
    $name   = $type.$period.$round;
    return redirect()->route($name);
});
Route::get('/api/404no', function(){
    return '404 NOT ALLOWED !';
})->name('api404');
Route::get('/api/video/{file}', function($file=''){
    $controller = new \App\Http\Controllers\API\APIVideoController();
    return $controller->index($file);
})->name('api-video');
// End;


// Route Untuk Login Single Sign On:
Route::get('/sso/{modul}/', function($modul){
    $controller = new \App\Http\Controllers\Autentikasi\AutentikasiController();
    return $controller->{$modul}();
})->where(
    [
        'modul' => '(?:keluar|masuk)'
    ]
)->name('stae-sso');
Route::post('sso/masuk', 'Autentikasi\AutentikasiController@PostProsesMasuk')->name('stae-sso-masuk-proses');
// End;

// Route Untuk Media Informasi dan Pemilih
Route::get('/info/{tipe}', function($tipe){
    $class      = '\\App\\Http\\Controllers\\Informasi\\';
    $method     = '';
    if($tipe == 'media'):
        $class  .= 'MediaController';
        $method = 'index';
    else:
        $arrayFunc  = ['registrasi-dalam-negeri' => 'dalamnegeri', 'registrasi-luar-negeri' => 'luarnegeri', 'total-registrasi' => 'keseluruhan'];
        $class  .= 'PemilihController';
        $method = $arrayFunc[$tipe];
    endif;
    $controller = new $class();
    return $controller->{$method}();
})->where(['tipe'  => '[a-z-_]+'])->name('media-informasi');

// Route Untuk Hasil Tabulasi
//Route::get('/counting-down/', 'Informasi\MediaController@countingDown')->name('cd2017');

Route::get('/tabulasi/presiden/2017/tabular', 'Tabulasi\PresidenController@hasil2017')->name('p2017R1');
Route::get('/tabulasi/parlemen/2012/tabular', 'Tabulasi\ParlemenController@hasil2012')->name('pr2012');
Route::get('/tabulasi/presiden/2012/R1/tabular', 'Tabulasi\PresidenController@hasil2012')->name('p2012R1');
Route::get('/tabulasi/presdien/2012/R2/tabular', 'Tabulasi\PresidenController@hasil2012')->name('p2012R2');

Route::get('/tabulasi/parlemen/2017/livetable', 'Tabulasi\ParlemenController@hasil2017')->name('pr2017ltable');

// TABULASI
Route::group(['middleware' => 'CheckAuthenticationTab'], function(){
    Route::get('/tabulasi/beranda', 'STAEController@beranda')->name('tabDefault');

    // * ----- GROUP TABULASI ---- * //
    Route::get('/tabulasi/parlemen/2017/tabular', 'Tabulasi\ParlemenController@hasil2017')->name('pr2017');
    Route::get('/tabulasi/parlemen/2017/bar', 'Tabulasi\ParlemenController@hasil2017')->name('pr2017bar');
    Route::get('/tabulasi/parlemen/2017/pie', 'Tabulasi\ParlemenController@hasil2017')->name('pr2017pie');
    Route::get('/tabulasi/parlemen/2017/SBar', 'Tabulasi\ParlemenController@hasil2017')->name('pr2017sbar');
    Route::get('/tabulasi/parlemen/2017/SPie', 'Tabulasi\ParlemenController@hasil2017')->name('pr2017spie');
    Route::get('/tabulasi/parlemen/2017/party', 'Tabulasi\ParlemenController@hasil2017')->name('pr2017p');
    Route::get('/tabulasi/parlemen/2017/maps', 'Tabulasi\ParlemenController@hasil2017')->name('pr2017map');
    Route::get('/tabulasi/parlemen/2017/tv', 'Tabulasi\ParlemenController@hasil2017')->name('pr2017tv');
    Route::get('api/tabulasi/parlemen2017/{tipe?}/hasil', function($tipe='all'){
        $controller = new \App\Http\Controllers\API\APITabulasiController();
        return $controller->AmbilDataParlemen2017($tipe);
    })->where(['tipe' => '(?:nasional|all|party|data)'])->name('api-tabulasi-parlemen2017');
});

// ASET
Route::group(['middleware' => 'CheckAuthentication'], function(){
    // Route Untuk Sistem Asset

    // * ----- GROUP BACKEND ---- *//
    Route::get('/aset/beranda/', function(){
        $controller = new \App\Http\Controllers\Aset\BerandaController();
        return $controller->beranda();
    })->name('aset-default');

    Route::get('/aset/pengaturan/{modul}', function($modul){
        $controller = new \App\Http\Controllers\Aset\PengaturanController();
        return $controller->{$modul}();
    })->where(['modul' => '(?:bahasa|pengguna|halaman|sql-console)'])->name('aset-pengaturan');

    Route::get('/aset/master-data/{modul}/{aksi?}', function($modul='',$aksi='index'){
        $function   = ucfirst($modul) . 'Controller';
        $class      = 'App\\Http\\Controllers\\Aset\\Master\\'.$function;
        $controller = new $class();
        return $controller->{$aksi}();
    })->where(
        [
            'modul' => '(?:vendor|kategori|barang|departemen|media|identifikasi)',
            'aksi'  => '(?:tambah|ubah|hapus)'
        ]
    )->name('aset-master');

    Route::get('/aset/transaksi/{modul}/{aksi?}', function($modul='',$aksi='index'){
        $arrayfunc  = [
            'barang-masuk'  => [
                'controller'    => 'BarangController',
                'method'        => 'index'
            ],
            'barang-keluar'  => [
                'controller'    => 'BarangController',
                'method'        => 'index'
            ],
            'mutasi'  => [
                'controller'    => 'BarangController',
                'method'        => 'index'
            ],
        ];

        $class      = 'App\\Http\\Controllers\\Aset\\Transaksi\\'.$arrayfunc[$modul]['controller'];
        $controller = new $class();
        $function   = '';
        $method     = $arrayfunc[$modul]['method'];
        return $controller->{$method}();
    })->where(
        [
            'modul' => '(?:barang-masuk|barang-keluar|mutasi)',
            'aksi'  => '(?:tambah|ubah|hapus)'
        ]
    )->name('aset-transaksi');

    Route::get('/aset/laporan/{modul}/{aksi?}', function($modul='',$aksi='index'){
        $class      = 'App\\Http\\Controllers\\Aset\\Laporan\\LaporanController';
        $controller = new $class();
        $function   = strtolower(str_replace('-','',$modul));
        return $controller->{$function}($aksi);
    })->where(
        [
            'modul' => '(?:barang-masuk|barang-masuk|mutasi|posisi-barang|riwayat-barang)',
            'aksi'  => '(?:tambah|ubah|hapus)'
        ]
    )->name('aset-laporan');

    Route::get('/aset/informasi/{modul}/{tahun?}/{periode?}/{view?}', function($modul='',$tahun='p2017',$periode='R1',$view='bagan'){
        // modul = presiden|parlemen & tahun = p2012|p2017 & periode = R1|R2 & view = bagan|tabel|grafik
        // modul = media & subpemilu = presiden|parlement & periode = R1|R2 & view = bagan|tabel|grafik

        $class      = 'App\\Http\\Controllers\\Aset\\Informasi\\'.ucfirst($modul) . 'Controller';
        $controller = new $class();
        return $controller->{$tahun}($periode);
    })->where(
        [
            'modul' => '(?:p2012|p2017|media|registrasi-pemilih)',
            'aksi'  => '(?:presiden|parlemen)'
        ]
    )->name('aset-informasi');

    Route::get('aset/cetak/{modul}/{id?}',function($modul,$id=''){
        $class      = "\App\Http\Controllers\Aset\Master\CetakController";
        $controller = new $class();
        return $controller->{$modul}($id);
    })->where(['modul' => '(?:cetaknoseri)'])->name('aset-cetak');

    // Aset Post
    Route::post('aset/master-data/departemen/tambah', 'Aset\Master\DepartemenController@ProsesTambahDepartemen')->name('aset-tambah-departemen');
    Route::post('aset/master-data/departemen/ubah', 'Aset\Master\DepartemenController@ProsesUbahDepartemen')->name('aset-ubah-departemen');
    Route::post('aset/master-data/departemen/hapus', 'Aset\Master\DepartemenController@ProsesHapusDepartemen')->name('aset-hapus-departemen');

    Route::post('aset/master-data/vendor/tambah', 'Aset\Master\VendorController@ProsesTambahVendor')->name('aset-tambah-vendor');
    Route::post('aset/master-data/vendor/ubah', 'Aset\Master\VendorController@ProsesUbahVendor')->name('aset-ubah-vendor');
    Route::post('aset/master-data/vendor/hapus', 'Aset\Master\VendorController@ProsesHapusVendor')->name('aset-hapus-vendor');

    Route::post('aset/master-data/kategori/tambah', 'Aset\Master\KategoriController@ProsesTambahKategori')->name('aset-tambah-kategori');
    Route::post('aset/master-data/kategori/ubah', 'Aset\Master\KategoriController@ProsesUbahKategori')->name('aset-ubah-kategori');
    Route::post('aset/master-data/kategori/hapus', 'Aset\Master\KategoriController@ProsesHapusKategori')->name('aset-hapus-kategori');

    Route::post('aset/master-data/barang/tambah', 'Aset\Master\BarangController@ProsesTambahBarang')->name('aset-tambah-barang');
    Route::post('aset/master-data/barang/ubah', 'Aset\Master\BarangController@ProsesUbahBarang')->name('aset-ubah-barang');
    Route::post('aset/master-data/barang/hapus', 'Aset\Master\BarangController@ProsesHapusBarang')->name('aset-hapus-barang');
    Route::post('aset/master-data/barang/noseri', 'Aset\Master\BarangController@ProsesIsiNoSeri')->name('aset-tambah-noseri');

    Route::post('aset/master-data/media/tambah', 'Aset\Master\MediaController@ProsesTambahMediaBarang')->name('aset-tambah-media-barang');


    Route::post('aset/transaksi/barang-masuk', 'Aset\Transaksi\BarangController@ProsesBarangMasuk')->name('aset-transaksi-barang-masuk');
    Route::post('aset/transaksi/barang-keluar', 'Aset\Transaksi\BarangController@ProsesBarangKeluar')->name('aset-transaksi-barang-keluar');

    Route::post('aset/pengaturan/pengguna/tambah', 'Aset\POST\PostPenggunaController@PostTambahPengguna')->name('aset-tambah-pengguna');
    Route::post('aset/pengaturan/pengguna/ubah', 'Aset\POST\PostPenggunaController@PostUbahPengguna')->name('aset-ubah-pengguna');
    Route::post('aset/pengaturan/pengguna/hapus', 'Aset\POST\PostPenggunaController@PostHapusPengguna')->name('aset-hapus-pengguna');

    // Aset API
    Route::get('api/departemen/{methode}/{id?}', function($metode,$id=''){
        $controller = new \App\Http\Controllers\API\APIDepartemenController();
        return $controller->{$metode}($id);
    })->where(['metode' => '[a-z]+'])->name('api-aset-departemen');

    Route::get('api/vendor/{methode}/{id?}', function($metode,$id=''){
        $controller = new \App\Http\Controllers\API\APIVendorController();
        return $controller->{$metode}($id);
    })->where(['metode' => '[a-z]+'])->name('api-aset-vendor');

    Route::get('api/kategori/{methode}/{id?}', function($metode,$id=''){
        $controller = new \App\Http\Controllers\API\APIKategoriController();
        return $controller->{$metode}($id);
    })->where(['metode' => '(:?aktifsemua|aktifrecord)'])->name('api-aset-kategori');

    Route::get('api/barang/{methode}/{id?}', function($metode,$id=''){
        $controller = new \App\Http\Controllers\API\APIBarangController();
        return $controller->{$metode}($id);
    })->where(['metode' => '[a-z]+'])->name('api-aset-barang');

    Route::get('api/pengguna/{metode}/{id?}', function($metode,$id=''){
        $controller = new \App\Http\Controllers\API\APIPenggunaController();
        return $controller->{$metode}($id);
    })->where(['metode' => '[a-z]+'])->name('api-aset-pengguna');

    // TYPEHEAD
    Route::get('api/typehaead/kategori', 'API\APIGeneralController@AmbilKodeKategori')->name('api-aset-kategoriTypehead');
    Route::get('api/typehaead/vendor', 'API\APIGeneralController@AmbilKodeVendor')->name('api-aset-vendorTypehead');
    Route::get('api/typehaead/departemen', 'API\APIGeneralController@AmbilKodeDepartemen')->name('api-aset-departemenTypehead');
    Route::get('api/typehaead/barang', 'API\APIGeneralController@AmbilKodeBarang')->name('api-aset-barangTypehead');
    Route::get('api/typehaead/barang-keluar', function(\Illuminate\Http\Request $request){
        $class      = new \App\Http\Controllers\API\APIGeneralController();
        $req        = $request->all();
        return $class->AmbilLibraryDataBarangSiapKeluar($req['d'],$req['q']);
    })->name('api-aset-barang-keluarTypehead');

});
// * ----- END GROUP BACKEND ---- *//

// API Informasi
Route::group(['middleware' => 'CheckAuthenticationAPI'], function(){
    Route::get('/api/informasi/{key}/{tipe}', function($key='',$tipe=''){
        $controller = new \App\Http\Controllers\API\APIInformasiController();
        return $controller->index($tipe);
    })->where(['tipe' => '(:?tab|abd)'])->name('api-informasi');
});

// Redirect Route to RootPath
