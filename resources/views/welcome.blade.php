<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ TITLE_APP }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
        <link href="/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .border {
                border: 1px solid #CCC;
            }

            .full-height {
                height: 90vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .title span {
                display: block;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
                margin-top: 50px;
            }
            .list {
                list-style-type: none !important;
            }
            .list li {
                display: inline;
                margin: 0px auto;
                padding: 10px;
                border: 1px solid #CCC;
            }
            .list li a {
                text-decoration: none;
            }
            .btn-group {
                margin-top: 25px;
            }
            .btn-group .btn {
                border-radius: 0px !important;
                font-family: 'Roboto Slab', serif !important;
                color: #555 !important;
                font-size: 20px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <img src="{{ asset('logo.png') }}" width="150" />
                <div class="title m-b-md">
                    <span>BEMVINDU</span>
                </div>


                <div class="links">
                    <a href="https://laravel.com/docs">SECRETARIADO T&#xC9;CNICO DE ADMINISTRA&#xC7;&#xC3;O ELEITORAL</a>
                    <p>Rua Caicoli, Dili, Timor Leste</p>
                </div>
                <div class="btn-group" role="group" aria-label="...">
                    <a class="btn btn-warning" href="{{ route('media-informasi',['tipe' => 'registrasi-dalam-negeri']) }}" target="_blank">STAE RE-ABD</a>
                    <a class="btn btn-info" href="{{ route('media-informasi',['tipe' => 'media']) }}" target="_blank">STAE MEDIA</a>
                    <a class="btn btn-danger" href="{{ route('pr2017') }}" target="_blank">STAE ELEISAUN</a>
                </div>
            </div>
        </div>

        <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
        <script src="/apps/scripts/apps-stae.js" type="text/javascript"></script>
        <script src="/apps/scripts/stae-library.js" type="text/javascript"></script>
        <script>
            (function() {

                var quotes = $(".texxt span");
                var quoteIndex = -1;
                
                function showNextQuote() {
                    ++quoteIndex;
                    quotes.eq(quoteIndex % quotes.length)
                        .fadeIn(2000)
                        .delay(2000)
                        .fadeOut(2000, showNextQuote);
                }
                
                showNextQuote();
                
            })();
        </script>
    </body>
</html>
