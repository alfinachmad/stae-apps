<?php $fieldname = 'ubah'.$field; ?>
<div id="<?=$fieldname?>" class="modal fade" tabindex="-1" data-width="760" data-backdrop="static" data-keyboard="false">
    <div class="modal-header bg-blue-steel font-white">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Ubah {{$field}}</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" role="form" method="post" action="{{ URL::route('aset-ubah-'.$modul) }}" autocomplete="off">
                    {!! csrf_field() !!}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Kode Kategori</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-sticky-note"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" id="kd-kategori" class="form-control hanyaAngka besarSemuaAwalKata text-right" placeholder="EL-001" maxlength="10" required autofocus>
                                    <input type="hidden" readonly name="<?=$fieldname?>[]" id="id-kategori" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama Kategori</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-sticky-note-o"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" id="nm-kategori" class="form-control besarSemuaAwalKata text-uppercase" placeholder="Maksimal 200 Karakter" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Keterangan Kategori</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-sticky-note-o"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" id="k-kategori" class="form-control besarSemuaAwalKata text-capitalize" placeholder="Maksimal 200 Karakter" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn bg-blue-steel font-white">Ubah</button>
                                <button type="button" data-dismiss="modal" class="btn btn-outline dark">Batal</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>