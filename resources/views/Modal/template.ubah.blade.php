<?php $fieldname = 'ubah'.$field; ?>
<div id="<?=$fieldname?>" class="modal fade" tabindex="-1" data-width="760" data-backdrop="static" data-keyboard="false">
    <div class="modal-header bg-blue-steel font-white">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Ubah {{$field}}</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" role="form" method="post" action="{{ URL::route('aset-ubah-'.$modul) }}" autocomplete="off">

                </form>
            </div>
        </div>
    </div>
</div>