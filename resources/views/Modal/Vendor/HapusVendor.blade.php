<div id="hapus{{$field}}" class="modal fade" tabindex="-1" data-width="760" data-backdrop="static" data-keyboard="false">
    <form role="form" method="post" action="{{ URL::route('aset-hapus-'.$modul) }}" autocomplete="off">
        {{ csrf_field() }}
        <div class="modal-header bg-blue-steel font-white">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Hapus {{$field}}</h4>
        </div>
        <div class="modal-body">
            Menghapus Data {{$field}} dengan Nama {{$field}}: [<span id="idcallbackdel" class="bold"></span>]. Apakah Anda yakin ?
            <input type="hidden" name="id" id="idcallbackdel-hiddenVal" />
        </div>
        <div class="modal-footer">
            <button type="submit" class="konfirmasi btn bg-blue-steel font-white">Hapus</button>
            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Batal</button>
        </div>
    </form>
</div>