<?php $fieldname = 'tambahMediaBesar'; ?>
<div id="<?=$fieldname?>" class="modal fade bs-modal-lg" tabindex="-1" data-width="1024" data-backdrop="static" data-keyboard="false">
    <div class="modal-header bg-blue-steel font-white">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Unggah Media Gambar</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form id="tambahmediagambar" class="form-horizontal" role="form" method="post" action="{{ URL::route('aset-tambah-media-barang') }}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Kode {{$field}}</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-sticky-note"></i>
                                    </span>
                                    <input type="text" name="image-kb" id="media-title" class="form-control hurufBesar text-right" readonly autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Unggah Gambar</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-image"></i>
                                    </span>
                                    <input type="file" name="media-gambar[]" id="input-id" class="file-loading" data-preview-file-type="text" multiple>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn bg-blue-steel font-white">Unggah</button>
                                <button type="button" data-dismiss="modal" class="btn btn-outline dark">Batal</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>