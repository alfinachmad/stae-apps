<?php $fieldname = 'tambah'.$field; ?>
<div id="<?=$fieldname?>" class="modal fade" tabindex="-1" data-width="760" data-backdrop="static" data-keyboard="false">
    <div class="modal-header bg-blue-steel font-white">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Tambah {{$field}}</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" role="form" method="post" action="{{ URL::route('aset-tambah-'.$modul) }}" autocomplete="off">
                    {!! csrf_field() !!}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Kode {{$field}}</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-sticky-note"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" class="form-control hurufBesar besarSemuaAwalKata text-right" placeholder="EL-001" maxlength="10" required autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama {{$field}}</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-sticky-note-o"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" class="form-control besarSemuaAwalKata" placeholder="Maksimal 200 Karakter" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Kategori {{$field}}</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-sticky-note-o"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" class="kategoritype form-control besarSemuaAwalKata text-capitalize" placeholder="Maksimal 200 Karakter" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-sticky-note-o"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" class="kodekategoritype form-control hurufBesar text-uppercase" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nomor Seri {{$field}}</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-sticky-note"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" class="form-control" maxlength="100" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Satuan </label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-asterisk"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" class="form-control hurufBesar text-uppercase" maxlength="10" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Harga </label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-asterisk"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" class="form-control hanyaAngka text-uppercase text-right" maxlength="6" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn bg-blue-steel font-white">Simpan</button>
                                <button type="button" data-dismiss="modal" class="btn btn-outline dark">Batal</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>