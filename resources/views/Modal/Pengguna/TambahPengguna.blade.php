<div id="tambahPengguna" class="modal fade" tabindex="-1" data-width="760" data-backdrop="static" data-keyboard="false">
    <div class="modal-header bg-blue-steel font-white">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Tambah Pengguna</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" role="form" method="post" action="{{ URL::route('aset-tambah-pengguna') }}" autocomplete="off">
                    {!! csrf_field() !!}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama Lengkap</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input type="text" name="tambahPengguna[]" class="form-control hanyaHuruf besarSemuaAwalKata" placeholder="Maksimal 75 Karakter" maxlength="75" required autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">E-Mail</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope-square"></i>
                                    </span>
                                    <input type="text" name="tambahPengguna[]" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Password</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user-secret"></i>
                                    </span>
                                    <input type="password" name="tambahPengguna[]" class="form-control" placeholder="Maksimal 16 Karakter" maxlength="16" required >
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Level Pengguna</label>
                            <div class="col-md-6">
                                <select name="tambahPengguna[]" class="form-control">
                                    <option value="1">Administrator</option>
                                    <option value="2">Viewer</option>
                                    <option value="3">Operator</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn bg-blue-steel font-white">Simpan</button>
                                <button type="button" data-dismiss="modal" class="btn btn-outline dark">Batal</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>