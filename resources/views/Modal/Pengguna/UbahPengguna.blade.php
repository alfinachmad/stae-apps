<div id="ubahPengguna" class="modal fade" tabindex="-1" data-width="760" data-backdrop="static" data-keyboard="false">
    <div class="modal-header bg-blue-steel font-white">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Ubah Pengguna</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" role="form" method="post" action="{{ URL::route('aset-ubah-pengguna') }}" autocomplete="off">
                    {!! csrf_field() !!}
                    <?php $fieldname = 'ubahPengguna'; ?>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama Pengguna</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user-md"></i>
                                    </span>
                                    <input type="text" readonly name="<?=$fieldname?>[]" id="nama-pengguna" class="form-control" placeholder="Maksimal 16 Karakter" maxlength="16">
                                    <input type="hidden" readonly name="<?=$fieldname?>[]" id="id-pengguna" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama Lengkap</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" id="nama-lengkap" class="form-control text-capitalize hanyaHuruf besarSemuaAwalKata" placeholder="Maksimal 75 Karakter" maxlength="75">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">E-Mail</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope-square"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" id="email" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Level Pengguna</label>
                            <div class="col-md-6">
                                <select name="<?=$fieldname?>[]" id="level-pengguna" class="form-control" required>
                                    <option value="1">Administrator</option>
                                    <option value="2">Viewer</option>
                                    <option value="3">Operator</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn bg-blue-steel font-white">Ubah</button>
                                <button type="button" data-dismiss="modal" class="btn btn-outline dark">Batal</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>