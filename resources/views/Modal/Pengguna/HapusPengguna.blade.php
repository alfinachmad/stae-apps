<div id="hapusPengguna" class="modal fade" tabindex="-1" data-width="760" data-backdrop="static" data-keyboard="false">
    <form role="form" method="post" action="{{ URL::route('aset-hapus-pengguna') }}" autocomplete="off">
        {{ csrf_field() }}
        <div class="modal-header bg-blue-steel font-white">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Hapus Pengguna</h4>
        </div>
        <div class="modal-body">
            Menghapus Data Pengguna dengan Nama Pengguna : [<span id="nama-pengguna" class="bold">asd</span>]. Apakah Anda yakin ?
            <input type="hidden" name="id" id="nama-pengguna-hiddenVal" />
        </div>
        <div class="modal-footer">
            <button type="submit" class="konfirmasi btn bg-blue-steel font-white">Hapus</button>
            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Batal</button>
        </div>
    </form>
</div>