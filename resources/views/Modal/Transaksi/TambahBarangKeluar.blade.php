<?php $fieldname = 'tambahBarangKeluar'; ?>
<div id="formTambahBarangKeluar" class="modal fade" tabindex="-1" data-width="760" data-backdrop="static" data-keyboard="false">
    <div class="modal-header bg-blue-steel font-white">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Tambah Barang</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form id="formbarangkeluar" class="form-horizontal" role="form" method="post" autocomplete="off">
                    {!! csrf_field() !!}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama {{$field}}</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-sticky-note-o"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" class="barangkeluar form-control besarSemuaAwalKata" placeholder="lookup" required>
                                    <input type="hidden" name="<?=$fieldname?>[]" class="kb form-control besarSemuaAwalKata">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nomor Seri Aset</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-sticky-note"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" class="nsa form-control" maxlength="100" readonly required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Satuan </label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-asterisk"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" class="st form-control hurufBesar text-uppercase" maxlength="10" readonly required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Harga </label>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-dollar"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" class="hrg form-control hanyaAngka text-uppercase text-right" maxlength="6" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Jumlah </label>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-asterisk"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" class="jmlkeluar form-control hanyaAngka text-uppercase text-right" maxlength="6" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="button" class="btn bg-blue-steel font-white tambahkanBarangKeluar" data-dismiss="modal">Tambahkan</button>
                                <button type="button" class="batal btn btn-outline dark">Batal</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>