@extends('STAE')
@section('header')
    <head>
        <title>{{ TITLE_ACRONYM }} | {{ $title_page }}</title>
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
        <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
        <link href="/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/bootstrap-colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/jquery-minicolors/jquery.minicolors.css" rel="stylesheet" type="text/css" />
        <link href="/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="/apps/css/todo.min.css" rel="stylesheet" type="text/css" />
        <link href="/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="/layouts/layout3/css/themes/blue-steel.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="/apps/css/sticker.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="/apps/css/unslider.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="/apps/css/countdown.min.css" rel="stylesheet" type="text/css" id="style_components" />
        <link href="/apps/css/measure.css" rel="stylesheet" type="text/css" id="style_components" />
        <link href="/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" />
        <style>
            body,h2,h3,h4 {
                font-family: 'Roboto Slab', serif !important;
            }

            .noborder {
                border: none !important;
            }

            .nopaddingside{
                padding-right: 0px !important;
                padding-left: 0px !important;
            }

            .portlet {
                border-top: 2px solid #3598DC;
                box-shadow: 0 1px 3px rgba(0,0,0,0.10), 0 1px 2px rgba(0,0,0,0.15);
            }

            .page-content {
                padding: 5px 0px;
            }

            #content {
                padding-left: 10px !important;
                padding-right: 10px !important;
            }

            .sexy_line {
                display:block;
                border:none;
                color:white;
                height:1px;
                background:black;
                background: -webkit-gradient(radial, 50% 50%, 0, 50% 50%, 850, from(#3598DC), to(#fff));
            }

            .blink_me {
                animation: blinker 3s linear infinite;
            }

            @keyframes blinker {
                50% { opacity: 0; }
            }

            #hasil-pemilihan {
                padding-left: 0px;
                padding-right: 0px;
                margin-top: 20px;
            }

            #hasil-pemilihan ul.media-list li.media {
                border: 0px solid #000 !important;
            }

            #hasil-pemilihan .media-body h4 {
                margin-top: 10px;
                font-size: 22px;
            }

            #hasil-pemilihan .media-body p {
                border: 0px solid #CCC;
                line-height: 0;
                font-size: 18px !important;
                margin-bottom: 24px !important;
            }

            #statistik-pemilihan table, #statistik-pemilihan-tv table {
                box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.1);
            }

            #statistik-pemilihan table thead tr, #statistik-pemilihan-tv table thead tr {
                border: 1px solid #4B77BE;
            }

            #statistik-pemilihan table tbody tr td, #statistik-pemilihan-tv table tbody tr td {
                border: 1px solid #E1E5EC !important;
            }

            #statistik-pemilihan table tbody tr:nth-child(odd), #statistik-pemilihan-tv table tbody tr:nth-child(odd){
                background-color: #EFF3F8;
            }

            #statistik-pemilihan table#tabel-elektor tbody tr td:first-child, #statistik-pemilihan-tv table#tabel-elektor tbody tr td:first-child {
                table-layout: fixed !important;
                width: 45%;
            }

            #statistik-pemilihan table#tabel-akta tbody tr td:nth-child(2), #statistik-pemilihan table#tabel-elektor tbody tr td:nth-child(2),
            #statistik-pemilihan-tv table#tabel-akta tbody tr td:nth-child(2), #statistik-pemilihan-tv table#tabel-elektor tbody tr td:nth-child(2) {
                width: 25%;
            }

            #statistik-pemilihan-tv table td {
                padding: 3px 5px !important;
                font-weight: bolder !important;
				font-size: 16px !important;
				letter-spacing: 2px !important;
				color: black !important;
				border: 1px solid #000 !important;
				text-shadow: 1px 0 #000;
            }

            #grid-partai {
                box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.1);
                border-radius: 2px !important;
            }

            ul#select-slide-index li a:hover {
                background-color: #3598DC !important;
                color: #FFF;
            }

            #bar-img {
                padding: 0px 5px 20px 5px;
            }

            #bar-img img:first-child {
                margin-left: 50px !important;
                margin-right: 10px;
            }

            #bar-img img {
                margin-right: 11.5px;
            }

            .amcharts-pie-slice {
                transform: scale(1);
                transform-origin: 50% 50%;
                transition-duration: 0.3s;
                transition: all .3s ease-out;
                -webkit-transition: all .3s ease-out;
                -moz-transition: all .3s ease-out;
                -o-transition: all .3s ease-out;
                cursor: pointer;
                box-shadow: 0 0 30px 0 #000;
            }

            table#legend-pie {
                width: 400px;
            }
            table#legend-pie td {
                padding: 2px;
                vertical-align: middle !important;
            }
            table#tv th {
                font-weight: bold !important;
				color: #FFF !important;
                border: 1px solid #4c4c4c;
                font-size: 18px;
            }
            table#tv td {
                padding: 0px 3px !important;
                font-weight: bold !important;
                border: 1px solid #000;
                vertical-align: middle !important;
                font-size: 21px;
				color: #000 !important;
            }

            .gps_ring {
                border: 3px solid red;
                border-radius: 30px !important;
                height: 18px;
                width: 18px;
                margin-right: 15px !important;
                position: absolute;
                -webkit-animation: pulsate 1s ease-out;
                -webkit-animation-iteration-count: infinite;
            }

            .flip-clock-wrapper {
                width: 625px !important;
                margin-left: 0px !important;
            }

            table#livetable td {
                padding: 7px 7px  !important;

            }

            #slideshow {
                margin: 0px auto;
                position: relative;
                width: auto;
                height: auto;
            }

            #slideshow > div {
                position: absolute;
                top: 10px;
                left: 10px;
                right: 10px;
                bottom: 10px;
            }

            @-webkit-keyframes pulsate {
                0% {-webkit-transform: scale(0.1, 0.1); opacity: 0.0;}
                50% {opacity: 1.0;}
                100% {-webkit-transform: scale(1.2, 1.2); opacity: 0.0;}
            }

            /* DESKTOP */
            @media (min-width: 1440px) and (max-width: 1920px) {
                figure {
                    font-size: 1.6em !important;
                }

                figure img {
                    width: 115px;
                }

                .portlet {
                    padding-bottom: 3px !important;
                }

                h4#judul-municipiu {
                    font-size: 1.4em !important;
                }

                div.div-hasil {
                    min-height: 772px !important;
                }

                .sexy_line {
                    margin-top: 10px;
                    margin-bottom: 14px;
                }
            }

            /*
              ##Device = Laptops, Desktops
              ##Screen = B/w 1025px to 1280px
            */

            @media (min-width: 1025px) and (max-width: 1399px) {
                figure {
                    font-size: 16px !important;
                }

                figure img {
                    width: 95px;
                }

                h4#judul-municipiu {
                    font-size: 15px !important;
                }

                div.div-hasil {
                    min-height: 446px !important;
                }

                .portlet {
                    padding-bottom: 6px !important;
                }

                .sexy_line {
                    margin-top: 8px;
                    margin-bottom: 5px;
                }

                div.div-hasil {
                    min-height: 515px !important;
                }

                .breakingNews{width:100%; height:30px; background:#FFF; position:relative;  overflow:hidden;}
                .breakingNews>.bn-title{width:auto; height:30px; display:inline-block; background:#2096cd; position:relative;}
                .breakingNews>.bn-title>h2{display:inline-block; margin:0; padding:0 15px; line-height:30px; font-size:13px; color:#FFF; height:30px; box-sizing:border-box;}
                .breakingNews>.bn-title>span{width: 0;position:absolute;right:-10px;top:6px;height: 0;border-style: solid;border-width: 10px 0 10px 10px;border-color: transparent transparent transparent #2096cd;}
                .breakingNews>ul{padding:0; margin:0; list-style:none; position:absolute; left:210px; top:0; right:40px; height:30px; font-size:14px;}
                .breakingNews>ul>li{position:absolute; height:30px; width:100%; line-height:30px; display:none;}
                .breakingNews>ul>li>a{text-decoration:none; color:#333; overflow:hidden; display:block; white-space: nowrap;text-overflow: ellipsis; font-weight:normal;}
                .breakingNews>ul>li>a>span{color:#2096cd;}
                .breakingNews>ul>li>a:hover{color:#2096cd;}

                .select-id button {
                    height: 30px !important;
                    padding-top: 5px !important;
                }

                .flip-clock-wrapper {
                    width: 625px !important;
                    margin-left: 0px !important;
                    border: 1px solid yellow;
                }


            }

            /*
              ##Device = Tablets, Ipads (portrait)
              ##Screen = B/w 768px to 1024px
            */

            @media (min-width: 768px) and (max-width: 1024px) {

            }

            /*
              ##Device = Tablets, Ipads (landscape)
              ##Screen = B/w 768px to 1024px
            */

            @media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {

            }

            /*
              ##Device = Low Resolution Tablets, Mobiles (Landscape)
              ##Screen = B/w 481px to 767px
            */

            @media (min-width: 481px) and (max-width: 767px) {

            }

            /*
              ##Device = Most of the Smartphones Mobiles (Portrait)
              ##Screen = B/w 320px to 479px
            */

            @media (min-width: 320px) and (max-width: 480px) {

            }

            figure img {
                margin-bottom: 10px;
            }
            h4#judul-municipiu {

            }

            #content-body-custom h4 {
                margin-top: 10px;
                margin-bottom: 30px;
                line-height: 0px;
                font-size: 20px;
            }
            table#result-regis-foreign tbody tr td:nth-child(1){
                text-align: left !important;
            }
            table#result-regis-foreign tbody tr td:not(:nth-child(1)){
                text-align: right !important;
            }
            table#result-regis-foreign thead tr th, table#result-regis-foreign tfoot tr td {
                border: 1px solid #E9EDEF !important;
            }
            .portlet {
                margin-bottom: 0px !important;
            }
            .todo-projects-item {
                padding: 15px 20px 0px 20px !important;
            }
            .embed-responsive-16by9 {
                padding: 0% 0% 53.5% 0% !important;
                border: 0px solid #CCC;
            }
            video::-webkit-media-controls-panel {
                display: flex !important;
                opacity: 1 !important;
            }
            .vticker h4 {
                line-height: 0px;
                margin-top: 10px;
            }
        </style>
    </head>
@endsection
@section('footer')
    <script src="/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- MODAL -->
    <script src="/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <script src="/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
    <script src="/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>

    <!-- CHART AM -->
    <script src="/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
    <script src="/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
    <script src="/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
    <script src="/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
    <script src="/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
    <script src="/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
    <script src="/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
    <script src="/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
    <script src="/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
    <script src="/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
    <!-- PLUG CHART -->
    <!-- COLOR -->

    <!-- END COMPONENT -->
    <script src="/global/scripts/app.js" type="text/javascript"></script>
    <script src="/layouts/layout3/scripts/layout.js" type="text/javascript"></script>
    <script src="/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
    <script src="/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <script src="/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
    <script src="/apps/scripts/jquery.easy-ticker.js" type="text/javascript"></script>
    <script src="/apps/scripts/jquery.easing.min.js" type="text/javascript"></script>
    <script src="/apps/scripts/sticker.min.js" type="text/javascript"></script>
    <script src="/apps/scripts/countdown.min.js" type="text/javascript"></script>
    <script src="/apps/scripts/unslider.js" type="text/javascript"></script>
    <script src="/apps/scripts/apps-stae.js" type="text/javascript"></script>
    <script>
    @if(Request::segment(1) == 'tabulasi')

        $(document).ready(function() {

            // [B] PENGATURAN AWAL //
            document.body.onkeydown = function (evt) {
                if((evt.ctrlKey && (evt.keyCode == 82)) || (evt.keyCode == 116)){
                    App.blockUI({
                        target: '#page-render',
                        boxed: true,
                        message: 'Processing...',
                    });

                    window.setTimeout(function () {
                        App.unblockUI('#page-render');
                        window.location.reload();
                    });
                    return false;
                }
            };

            var divHasil    = $('div#content-hasil-pemilu'),
                tbHasil     = $('div#rowcontent-hasil-pemilu'),
                liHasil     = $('li#subcontent-hasil-pemilu'),
                arrDiv      = [],
                chart       = '',
                vAxis       = '',
                graph       = '',
                total       = liHasil.length,
                chartData   = '';

            $.each(liHasil, function(i,v){
                arrDiv.push(v.dataset.title);
            });
            $('h4#judul-municipiu').text(arrDiv[0]);

            divHasil.unslider({
                autoplay: true,
                infinite: false,
                arrows: false,
                delay: 20000
            });

            $("#news").breakingNews({
                effect		:"slide-h",
                autoplay	:true,
                timer		: 10000,
                color		:'purple'
            });

            AmCharts.loadJSON = function(url){
                if(window.XMLHttpRequest){
                    request = new XMLHttpRequest();
                }else{
                    request = new ActiveXObject('Microsoft.XMLHTTP');
                }
                request.open('GET', url, false);
                request.send();
                return eval(request.responseText);
            };
            chartData   = AmCharts.loadJSON('{!! $dtchart[$dtsegment]['apiURL'] !!}');
            // [E] PENGATURAN AWAL //

        @if(Request::segment(4) == 'party' OR Request::segment(4) == 'bar' OR Request::segment(4) == 'tabular')

            // [B] CHART BAR UNTUK PER PARTAI, DISTRIK, NASIONAL
            AmCharts.ready(function(){
                chart   = new AmCharts.AmSerialChart();
                vAxis   = new AmCharts.ValueAxis();
                graph   = new AmCharts.AmGraph();

                chart.dataProvider  = {!! $dtchart[$dtsegment]['dataDef'] !!}

                vAxis.gridAlpha     = 0.2;
                vAxis.dashLength    = 0;

                graph.type              = "column";
                graph.valueField        = "TOTAL";
                graph.labelText         = '[[TOTAL]] : [[PERSEN]]';
                graph.labelPosition     = 'top';
                graph.showAllValueLabels= true;
                graph.labelFunction     = function(item){
                    var perse   = Math.abs(item.dataContext.PERSEN);
                    return to2(item.values.value);
                };
                graph.fillAlphas        = 1;
                graph.fontSize          = 18;
                graph.colorField        = 'COLOR';
                graph.lineColor         = "#BBBBBB";
                graph.lineAlpha         = 0.2;
                graph.balloonText       = "{!! $dtchart[$dtsegment]['text'] !!} ";

                chart.categoryField     = "{!! $dtchart[$dtsegment]['category'] !!}";
                chart.angle             = 30;
                chart.depth3D           = 15;
                chart.effect            = "easeOutSine";
                chart.startDuration     = 2;
                chart.sequencedAnimation    = 1;

                chart.addValueAxis(vAxis);
                chart.addGraph(graph);
                chart.write('{!! $dtchart[$dtsegment]['write'] !!}');
            });
            // [E] CHART UNTUK PER PARTAI DAN DISTRIK

            @if(Request::segment(4) == 'tabular')

            // [B] CHART PIE UNTUK NASIONAL
            var chartSettings = {
                dataProvider: chart.dataProvider = chartData[0],
                angle: 30,
                balloonText: "[[title]]: [[valueTotal]]",
                depth3D: 20,
                labelFunction: function(item){
                    var value = item.value;
                    var percent = item.percents.toFixed(2);
                    return item.title + ": " + to2(value) + " (" + percent + "%)";
                },
                maxAngle: 60,
                maxDepth: 30,
                minAngle: 0,
                minDepth: 1,
                outlineAlpha: 0.4,
                startDuration: 0,
                theme: "light",
                titleField: "NAMA_ACRY",
                type: "pie",
                valueField: "TOTAL",
                colorField: 'COLOR',
                pulledField: 'PULLEDOUT',
                pullOutOnlyOne: true
            },
            chart = AmCharts.makeChart('nasional-pie', chartSettings);
            chart.addListener('rendered', function(event){
                var legend          = document.getElementById('nasional-pie-legend'),
                    text            = '<table id="legend-pie"><tbody>';

                chart.customLegend  = legend;
                for(var i in chart.chartData){
                    var row     = chart.chartData[i],
                        color   = chart.colors[i],
                        dataCtx = row.dataContext,
                        percent = Math.round(dataCtx.PERSEN * 100) / 100,
                        value   = dataCtx.TOTAL;

                    text    += '' +
                        '<tr>' +
                        '<td align="center"><img src="/apps/img/kandidat-partai/i'+ (parseInt(i) + 1) +'.jpg" width="60" style="border: 1px solid #CCC;" /></td>'+
                        '<td>'+(parseInt(i) + 1) +'. '+ dataCtx.NAMA_ACRY+'</td>'+
                        '<td align="right">'+to2(value)+'</td>'+
                        '<td align="right">'+percent+' %</td>'+
                        '</tr>';
                }
                text                += '</tbody></table>';
                legend.innerHTML    = text;
            });
            // [E] CHART PIE UNTUK NASIONAL

            @endif

            // [B] CETAK SLIDER TABULAR DIV
            divHasil.on('unslider.change', function(event,index){
                $('h4#judul-municipiu').text(arrDiv[index]);
                chart.dataProvider  = {!! $dtchart[$dtsegment]['data'] !!}
                {!! $dtchart[$dtsegment]['validate'] !!}
                {!! $dtchart[$dtsegment]['animate'] !!}
            @if(Request::segment(4) == 'tabular')

                if(index == (total - 1)){
                    divHasil.data('unslider').stop();
                    setTimeout(function(){
                        App.blockUI({
                            target: '#page-render',
                            boxed: true,
                            message: 'Processing...',
                        });

                        window.setTimeout(function() {
                            App.unblockUI('#page-render');
                            divHasil.data('unslider').animate(0);
                        }, 3000);
                    }, 60000);
                }else if(index == 19){
                    divHasil.data('unslider').stop();
                    setTimeout(function(){
                        divHasil.data('unslider').next();
                    },60000);
                }else if(index == 20){
                    divHasil.data('unslider').stop();
                    setTimeout(function(){
                        divHasil.data('unslider').next();
                    },60000);
                }

            @else

                if(index == (total - 1)){
                    divHasil.data('unslider').stop();
                    setTimeout(function(){
                        App.blockUI({
                            target: '#page-render',
                            boxed: true,
                            message: 'Processing...',
                        });

                        window.setTimeout(function() {
                            App.unblockUI('#page-render');
                            window.location.reload();
                        }, 3000);
                    }, 20000);
                }

            @endif

            });

            $('a.select-slide').click(function(e) {
                e.preventDefault();
                var index = $('a.select-slide').index(this);
                divHasil.data('unslider').animate(index);
                chart.animateAgain();
            });
            // [E] CETAK SLIDER TABULAR DIV

        @elseif(Request::segment(4) == 'tv')

            tbHasil.unslider({
                autoplay: true,
                animation: 'vertical',
                infinite: false,
                arrows: false,
                delay: 10000
            });

            divHasil.on('unslider.change', function(event,index){
                if(index == (total - 1)){
                    divHasil.data('unslider').stop();
                    setTimeout(function () {
                        App.blockUI({
                            target: '#page-render',
                            boxed: true,
                            message: 'Processing...',
                        });

                        window.setTimeout(function () {
                            App.unblockUI('#page-render');
                            window.location.reload();
                        }, 3000);
                    }, 20000);
                }
            });

        @else



        @endif

        });

    @else

        $(document).ready(function() {
            var clock   = $('.jam').FlipClock(0, {
                countdown: true,
                clockFace: 'DailyCounter',
            });

            $("#news").breakingNews({
                effect		:"slide-h",
                autoplay	:true,
                timer		: 10000,
                color		:'purple'
            });
            setLiveDateTime();
        });

    @endif


    </script>
@endsection
