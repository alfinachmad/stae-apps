<!DOCTYPE html>
<html lang="en">
@yield('header')
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div id="content-body-custom" class="page-wrapper-row full-height" data-attr-server="{{ getMicrotime() }}">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div id="content" class="container-fluid">
                                    <div class="page-content-inner">
                                        <div id="page-render" class="portlet light">
                                            @if(Request::segment(1) == 'counting-down')
                                                @include('Informasi.Counting')
                                            @elseif(Request::segment(2) == 'media')
                                                @include('Informasi.Media.Index')
                                            @elseif(Request::segment(4) == 'tv')
                                                @include('Informasi.Parlemen.TV')
                                            @else
                                                @include('Informasi.Informasi')
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @yield('footer')
    </body>
</html>
