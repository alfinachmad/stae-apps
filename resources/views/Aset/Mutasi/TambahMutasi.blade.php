<div class="container-fluid">
    <div class="page-content-inner">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-folder-open"></i> {{ ucfirst($titleDoc) }}
                </div>
            </div>
            <div class="portlet-body">
                <?php $fieldname = 'inputBarang'; ?>
                <form class="form-horizontal" role="form" method="post" action="http://stae-info.app/aset/pengaturan/pengguna/tambah" autocomplete="off">
                    <input type="hidden" name="_token" value="cEiOv6DWAqplYaL4UkPR674Jf62yooKoJ51tkS7Z">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-1 control-label">No. Faktur</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-sticky-note"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" class="form-control hurufBesar text-right" placeholder="" required="" autofocus="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" class="form-control hanyaHuruf hurufBesar text-center" placeholder="Tanggal Beli" required="" autofocus="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 control-label">Supplier</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-get-pocket"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" class="form-control besarSemuaAwalKata" required="">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-dollar"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" class="form-control text-right hanyaAngka" placeholder="Harga Barang" maxlength="16" required="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 control-label">Departemen Asal</label>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-industry"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" class="form-control besarSemuaAwalKata" required="">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-industry"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" class="form-control besarSemuaAwalKata" placeholder="Departemen Tujuan" required="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 control-label">Keterangan</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-bars"></i>
                                    </span>
                                    <textarea name="<?=$fieldname?>[]" class="form-control" required="" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 control-label"></label>
                            <div class="col-md-11">
                                <a class="btn btn-sm pull-right font-dark" href="{{ URL::route('aset-halaman', ['modul' => 'barang','aksi' => 'tambah']) }}" data-toggle="modal">
                                    <i class="icon-plus"></i> Tambah
                                </a>
                                <div class="table-scrollable">
                                    <table class="table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Kode</th>
                                                <th class="text-center">Nama</th>
                                                <th class="text-center">Satuan</th>
                                                <th class="text-center">Jumlah</th>
                                                <th class="text-center">Harga</th>
                                                <th class="text-center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="7">Data Belum Tersedia</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-1 col-md-9">
                                <button type="submit" class="btn bg-blue-steel font-white">Simpan</button>
                                <button type="button" data-dismiss="modal" class="btn btn-outline dark">Batal</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>