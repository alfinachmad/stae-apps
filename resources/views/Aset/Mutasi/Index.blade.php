<div class="container-fluid">
    <div class="page-content-inner">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-folder-open"></i> {{ ucfirst($titleDoc) }}
                </div>
                <div class="actions">
                    <a class="btn btn-sm bg-blue-steel font-white" href="{{ URL::route('aset-halaman', ['modul' => 'mutasi','aksi' => 'tambah']) }}" data-toggle="modal">
                        <i class="icon-plus"></i> Tambah
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-advance table-hover" id="sample_1">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>Nama Departemen</th>
                        <th class="text-center">Nama Ruangan</th>
                        <th class="text-center">Lantai</th>
                        <th class="text-center">Nama Gedung</th>
                        <th class="text-center">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="6">Data Belum Tersedia</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>