<div class="container-fluid">
    <div class="page-content-inner">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i> {{ ucfirst($titleDoc) }}
                </div>
                <div class="actions">
                    <a class="btn btn-sm bg-blue-steel font-white" href="#tambahPengguna" data-toggle="modal">
                        <i class="icon-plus"></i> Tambah
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-advance table-hover" id="sample_1">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">Nama Lengkap</th>
                        <th class="text-center">Nama Pengguna</th>
                        <th class="text-center">Email</th>
                        <th class="text-center">Tipe Pengguna</th>
                        <th class="text-center">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                        @if($numrows == 0)
                            <tr>
                                <td colspan="6">Data Belum Tersedia</td>
                            </tr>
                        @else
                            @foreach($data as $key => $value)
                                <tr>
                                    <td class="text-center">{{ $key + $data->firstItem() }}</td>
                                    <td>{{ ucwords($value['nama_lengkap']) }}</td>
                                    <td class="text-center">{{ $value['nama_pengguna'] }}</td>
                                    <td class="text-center">{{ $value['email'] }}</td>
                                    <td class="text-center">{{ $level[$value['tipe_pengguna']] }}</td>
                                    <td class="text-center">
                                        <a href="#ubahPengguna" data-toggle="modal" data-idrow="{{ $value['kode_pengguna'] }}" data-pengguna="{{ $value['nama_pengguna'] }}" class="ubah btn blue btn-xs blue-stripe btn-outline uppercase">
                                            <i class="fa fa-edit"></i> Ubah
                                        </a>
                                        <a href="#hapusPengguna" data-toggle="modal" data-idrow="{{ $value['kode_pengguna'] }}" data-pengguna="{{ $value['nama_pengguna'] }}" class="hapus btn red btn-xs red-stripe btn-outline uppercase">
                                            <i class="fa fa-trash"></i> Hapus
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                {{ $data->links('vendor.pagination.default') }}
            </div>
        </div>
    </div>
</div>
@include('Modal.Pengguna.TambahPengguna')
@include('Modal.Pengguna.UbahPengguna')
@include('Modal.Pengguna.HapusPengguna')
<?php $sessionData = session('NoticeSession') ?>
@if(Session::has('NoticeSession'))
    @if($sessionData['status'] == 'failed')
        @include('Modal.ModalFailed')
    @else
        @include('Modal.ModalSuccess')
    @endif
@endif