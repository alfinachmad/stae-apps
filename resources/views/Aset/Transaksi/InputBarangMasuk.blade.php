<div class="container-fluid">
    <div class="page-content-inner">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-folder-open"></i> {{ $titleDoc[Request::segment(3)] }}
                </div>
            </div>
            <div class="portlet-body">
                <?php $fieldname = 'header-masuk'; $field = 'Barang'; $modul = 'barang' ?>
                <form class="form-horizontal" role="form" method="post" action="{{ URL::route('aset-transaksi-barang-masuk') }}" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-1 control-label">No. Faktur</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-sticky-note"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" class="form-control hurufBesar text-right" value="{{ $nobuktimasuk[0]->nobukti }}" readonly autofocus>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" name="<?=$fieldname?>[]" class="form-control date-picker hanyaHuruf hurufBesar text-center" placeholder="Tanggal Beli" value="{{$tanggalhariini}}" required autofocus="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 control-label">Supplier</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-get-pocket"></i>
                                    </span>
                                    <input type="text" class="vendor form-control besarSemuaAwalKata" required placeholder="lookup">
                                    <input type="hidden" name="<?=$fieldname?>[]" class="vendortype" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 control-label">Penerima</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-industry"></i>
                                    </span>
                                    <input type="text" class="departemen form-control besarSemuaAwalKata" required placeholder="lookup">
                                    <input type="hidden" name="<?=$fieldname?>[]" class="departementype" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 control-label">Keterangan</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-bars"></i>
                                    </span>
                                    <textarea name="<?=$fieldname?>[]" class="form-control" required="" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1 control-label"></label>
                            <div class="col-md-11">
                                <a class="btn btn-sm pull-right font-dark" href="#tambahBarang" data-toggle="modal">
                                    <i class="icon-plus"></i> Tambah
                                </a>
                                <div class="table-scrollable">
                                    <table id="barang-masuk" class="table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Kode</th>
                                            <th class="text-center">Nama</th>
                                            <th class="text-center">Satuan</th>
                                            <th class="text-center">Jumlah</th>
                                            <th class="text-center">Harga</th>
                                            <th class="text-center">Total</th>
                                            <th class="text-center">Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr id="nodata">
                                            <td colspan="8">Data Belum Tersedia</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-1 col-md-9">
                                <button type="submit" class="btn bg-blue-steel font-white">Simpan</button>
                                <button type="button" data-dismiss="modal" class="btn btn-outline dark batal">Batal</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@include('Modal.Transaksi.TambahBarang')
@include('Modal.Transaksi.UbahBarang')
<?php $sessionData = session('NoticeSession') ?>
@if(Session::has('NoticeSession'))
    @if($sessionData['status'] == 'failed')
        @include('Modal.ModalFailed')
    @else
        @include('Modal.ModalSuccess')
    @endif
@endif