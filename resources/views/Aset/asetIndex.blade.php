@extends('Aset.aset')
@section('header')
    <head>
        <title>{{ TITLE_ACRONYM .' | SISTEM INFORMASI MANAJEMEN ASET '}}</title>
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
        <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
        <!-- DATE -->
        <link href="/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/clockface/css/clockface.css" rel="stylesheet" type="text/css" />
        <!-- END DATE -->

        <!-- UPLOAD -->
        <link href="/krajee/css/fileinput.min.css" rel="stylesheet" type="text/css" />
        <link href="/krajee/themes/explorer/theme.css" rel="stylesheet" type="text/css" />
        <!-- END -->

        <link href="/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/bootstrap-colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/jquery-minicolors/jquery.minicolors.css" rel="stylesheet" type="text/css" />
        <link href="/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="/layouts/layout3/css/themes/blue-steel.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
        <link href="/apps/css/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" />
        <style>
            .portlet {
                box-shadow: 0 1px 3px rgba(0,0,0,0.10), 0 1px 2px rgba(0,0,0,0.15);
            }
            .portlet-body {
                padding-top: 0px !important;
            }
            @if(Request::segment(3) == 'barang-masuk')
            table#barang-masuk input {
                background-color: transparent !important;
                border: none !important;
            }
            table#barang-masuk input#idbarang{
                width: 20px !important;
            }
            table#barang-masuk input#kdbarang{
                width: 50px !important;
            }
            table#barang-masuk input#nmbarang{
                width: 200px !important;
            }
            table#barang-masuk input#satuan{
                width: 30px !important;
            }
            table#barang-masuk input#jumlah{
                width: 50px !important;
            }
            table#barang-masuk input#harga{
                width: 70px !important;
            }
            table#barang-masuk tr td#aksi{
                width: 200px !important;
            }
            @elseif(Request::segment(3) == 'barang-keluar')
            table#barang-keluar input {
                background-color: transparent !important;
                border: none !important;
            }

            table#barang-keluar input#idbarangkeluar {
                width: 20px !important;
            }
            table#barang-keluar input#kdbarangkeluar {
                width: 50px !important;
            }

            table#barang-keluar input#nsa {
                width: 150px !important;
            }

            table#barang-keluar input#nmbarangkeluar {
                width: 250px !important;
            }

            table#barang-keluar input#satuan {
                width: 50px !important;
            }

            table#barang-keluar input#harga {
                width: 40px !important;
            }

            table#barang-keluar input#jumlah {
                width: 40px !important;
            }

            table#barang-keluar input#total {
                width: 70px !important;
            }

            @endif
        </style>
    </head>
@endsection
@section('footer')
    <script src="/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>

    <!-- DATE -->
    <script src="/global/plugins/moment.min.js" type="text/javascript"></script>
    <script src="/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
    <script src="/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <!-- END DATE -->

    <!-- COLOR PLUGIN -->
    <script src="/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js" type="text/javascript"></script>
    <script src="/global/plugins/jquery-minicolors/jquery.minicolors.min.js" type="text/javascript"></script>
    <script src="/pages/scripts/components-color-pickers.min.js" type="text/javascript"></script>
    <script src="/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
    <script src="/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
    <!-- END COLOR -->

    <!-- CUBEBOX -->

    <!-- END -->

    <!-- UPLOAD -->
    <script src="/krajee/js/plugins/piexif.min.js" type="text/javascript"></script>
    <script src="/krajee/js/plugins/sortable.min.js" type="text/javascript"></script>
    <script src="/krajee/js/plugins/purify.min.js" type="text/javascript"></script>
    <script src="/krajee/js/fileinput.min.js" type="text/javascript"></script>
    <script src="/krajee/themes/fa/theme.js" type="text/javascript"></script>
    <script src="/krajee/js/locales/id.js" type="text/javascript"></script>
    <!-- END -->

    <!-- FANCY -->

    <!-- END -->

    <script src="/global/scripts/app.min.js" type="text/javascript"></script>

    <!-- SCRIPT DEVEL -->

    <script src="/layouts/layout3/scripts/layout.js" type="text/javascript"></script>
    <script src="/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
    <script src="/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <script src="/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
    <script src="/global/scripts/custom.js" type="text/javascript"></script>
    <script src="/apps/scripts/jquery.autocomplete.js" type="text/javascript"></script>
    <script>
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            orientation: "left",
            autoclose: true,
            format: "dd / MM / yyyy"
        });

        $("#input-id").fileinput({
            showRemove: false,
            showUpload: false,
            resizeImage: true,
            allowedFileExtensions : ["jpg", "png"]
        });

        $('.modal').on('hidden.bs.modal', function () {
            $(this).find('form').trigger('reset');
        });

        $(document).ready(function(){
            $('.besarSemuaAwalKata').SemuaAwalKataHurufBesar();
            $('.besarAwalKata').HurufBesarDiAwal();
            $('.hurufKecil').SemuaHurufKecil();
            $('.hurufBesar').SemuaHurufBesar();
            $('.hanyaAngka').HanyaBolehAngka();
            $('.hanyaHuruf').HanyaBolehHuruf();
            $('.hanyaEmail').HanyaEmail();

            var NoticeMessage   = $('#showNotice');
                NoticeMessage.show();
                $('#closeNotice').on('click', function(){
                    NoticeMessage.hide();
                });

            $('.batal').on('click',function(){
                $(this).closest('form').find("input[type=text], textarea").val("");
                $('.modal').modal('hide');
            });

            @if(Request::segment(3) == 'pengguna')
            $('a.ubah').click(function(event){
                event.preventDefault();
                var uid = $(this).data('idrow'),
                    lid = $("#nama-pengguna"),
                    url = "{{ URL::route('api-aset-pengguna', ['metode' => 'aktifsemua']) }}/" + uid,
                    nl  = $("#nama-lengkap"),
                    em  = $("#email"),
                    lp  = $("#level-pengguna"),
                    id  = $("#id-pengguna");

                $.getJSON(url, function(data){
                    var fetch   = data[0];
                    nl.val(fetch['nlengkap']),
                    em.val(fetch['mail']),
                    lp.val(fetch['tpengguna']),
                    lid.val(fetch['npengguna']),
                    id.val(uid);
                });
            });
            $('a.hapus').click(function(event){
                event.preventDefault();
                var dt  = $(this).data('pengguna');

                $("span#nama-pengguna").text(dt);
                $("input#nama-pengguna-hiddenVal").val(dt);
            });
            @elseif(Request::segment(3) == 'departemen')
            $('a.ubah').click(function(event){
                event.preventDefault();
                var uid = $(this).data('idrow'),
                    idc = $("span#idCallback"),
                    idd = $("#id-departemen"),
                    kd  = $("#kd-departemen"),
                    url = "{{ URL::route('api-aset-departemen', ['metode' => 'aktifsemua']) }}/" + uid,
                    nd  = $("#nm-departemen"),
                    nmr = $("#nm-ruangan"),
                    gdg = $("#gedung");

                $.getJSON(url, function(data){
                    var fetch   = data[0];
                        kd.val(fetch['kdept']),
                        idd.val(fetch['iddept']),
                        idc.val(fetch['kdept']),
                        nd.val(fetch['ndept']),
                        nmr.val(fetch['nruangan']),
                        gdg.val(fetch['gdg']);
                });
            });
            $('a.hapus').click(function(event){
                event.preventDefault();
                var dt  = $(this).data('idcallback'),
                    uid = $(this).data('idrow');

                $("span#idcallbackdel").text(dt);
                $("input#idcallbackdel-hiddenVal").val(uid);
            });
            @elseif(Request::segment(3) == 'vendor')
            $('a.ubah').click(function(event){
                event.preventDefault();
                var uid = $(this).data('idrow'),
                    url = "{{ URL::route('api-aset-vendor', ['metode' => 'aktifsemua']) }}/" + uid,
                    idc = $("span#idCallback"),
                    idv = $("#id-vendor"),
                    kv  = $("#kd-vendor"),
                    nv  = $("#nm-vendor"),
                    alm = $("#alamat"),
                    eml = $("#email"),
                    web = $("#website");

                $.getJSON(url, function(data){
                    var fetch   = data[0];
                        idv.val(fetch['idvend']),
                        kv.val(fetch['kvend']),
                        nv.val(fetch['nvend']),
                        alm.val(fetch['alamat']),
                        eml.val(fetch['mail']),
                        web.val(fetch['web']);
                });
            });
            $('a.hapus').click(function(event){
                event.preventDefault();
                var dt  = $(this).data('idcallback'),
                    uid = $(this).data('idrow');

                $("span#idcallbackdel").text(dt);
                $("input#idcallbackdel-hiddenVal").val(uid);
            });
            @elseif(Request::segment(3) == 'kategori')
            $('a.ubah').click(function(event){
                event.preventDefault();
                var uid = $(this).data('idrow'),
                    url = "{{ URL::route('api-aset-kategori', ['metode' => 'aktifsemua']) }}/" + uid,
                    idc = $("span#idCallback"),
                    idk = $("#id-kategori"),
                    kk  = $("#kd-kategori"),
                    ktk = $("#k-kategori"),
                    nk  = $("#nm-kategori");

                $.getJSON(url, function(data){
                    var fetch   = data[0];
                        idk.val(fetch['idkategori']),
                        kk.val(fetch['kkategori']),
                        nk.val(fetch['nkategori']),
                        ktk.val(fetch['ktkategori']);
                });
            });
            $('a.hapus').click(function(event){
                event.preventDefault();
                var dt  = $(this).data('idcallback'),
                    uid = $(this).data('idrow');

                $("span#idcallbackdel").text(dt);
                $("input#idcallbackdel-hiddenVal").val(uid);
            });
            @elseif(Request::segment(3) == 'barang')
            $('a.ubah,a.unggah').click(function(event){
                event.preventDefault();
                var uid = $(this).data('idrow'),
                    url = "{{ URL::route('api-aset-barang', ['metode' => 'aktifsemua']) }}/" + uid,
                    idc = $("span#idCallback"),
                    mt  = $("#media-title"),
                    idb = $("#id-barang"),
                    kdb = $("#kd-barang"),
                    nsb = $("#ns-barang"),
                    nmb = $("#nm-barang"),
                    stb = $("#satuan"),
                    hrb = $("#harga");

                $.getJSON(url, function(data){
                    var fetch   = data[0];
                        idb.val(fetch['idbarang']),
                        mt.val(fetch['kbarang']),
                        kdb.val(fetch['kbarang']),
                        nsb.val(fetch['nsbarang']),
                        nmb.val(fetch['nbarang']);
                        stb.val(fetch['unit']);
                        hrb.val(fetch['price']);
                });
            });
            $('a.hapus').click(function(event){
                event.preventDefault();
                var dt  = $(this).data('idcallback'),
                    uid = $(this).data('idrow');

                $("span#idcallbackdel").text(dt);
                $("input#idcallbackdel-hiddenVal").val(uid);
            });

            var kategori = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace('k'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: '{{ URL::route('api-aset-kategoriTypehead') }}?q=%QUERY%',
                    wildcard: '%QUERY%'
                }
            });

            kategori.initialize();

            $('.kategoritype').typeahead(null, {
                name: 'kategori',
                hint: true,
                highlight: true,
                minLength: 1,
                limit: 10,
                displayKey: 'k',
                source: kategori,
            }).on('typeahead:selected', function (key,val) {
                $('.kodekategoritype').val(val.v);
            });
            @elseif(Request::segment(3) == 'barang-keluar')

            // Digunakan untuk mereset text input menjadi kosong ketika Modal ditampilkan

            // Digunakan untuk mengambil Data Departemen
            var departemen = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace('k'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: '{{ URL::route('api-aset-departemenTypehead') }}?q=%QUERY%',
                    wildcard: '%QUERY%'
                }
            });

            departemen.initialize();

            // Digunakan untuk AutoComplete Data Departemen
            var kodedepartemen;
            var y = $('form#testform').find('input[type="hidden"]').val();
            $('.departemen').typeahead(null, {
                name: 'departemen',
                hint: true,
                highlight: true,
                minLength: 1,
                limit: 10,
                displayKey: 'k',
                source: departemen,
            }).on('typeahead:selected', function (key,datum) {
                $('.departementype').val(datum.v);
            });


            var cc = 0;
            $('.tambahBarangKeluar').on('click', function(e){
                var el = jQuery(this);
                cc += 1;
                if (cc > 1){
                    // deactivate
                    cc.remove();
                }

                y = $('.departementype').val();
                var barangKeluar = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.whitespace('NB'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    remote: {
                        url: '{{ URL::route('api-aset-barang-keluarTypehead') }}?d='+y+'&q=%QUERY%',
                        wildcard: '%QUERY%'
                    }
                });

                barangKeluar.initialize();

                $('.barangkeluar').typeahead(null, {
                    name: 'barangkeluar',
                    hint: true,
                    highlight: true,
                    minLength: 1,
                    limit: 10,
                    displayKey: 'NB',
                    source: barangKeluar
                }).on('typeahead:selected', function (key,val) {
                    $('.nb').val(val.NB);
                    $('.st').val(val.ST);
                    $('.hrg').val(val.HRG);
                    $('.nsa').val(val.NSA);
                    $('.kb').val(val.KB);
                });
            });



            var id = 0,
                txtnsb  = $('.nsb').val(''),
                txtnsa  = $('.nsa').val(''),
                txthrg  = $('.hrg').val(''),
                txtst   = $('.st').val(''),
                txtjml  = $('.jmlkeluar').val(''),
                txtnb   = $('.nb').val(''),
                txtkb   = $('.kb').val('');

            $('.tambahkanBarangKeluar').on('click', function(e){
                $("tr#nodata").remove();
                txtnsb = $('.nsb').val();
                txtnsa = $('.nsa').val();
                txthrg = $('.hrg').val();
                txtst  = $('.st').val();
                txtjml = $('.jmlkeluar').val();
                txtnb  = $('.nb').val();
                txtkb  = $('.kb').val();
                id++;

                var add = $(this).closest('#formbarangkeluar').find('.nsa');
                add.each(function () {
                    var content = $(this).text();
                    if ($('#barang-keluar > tbody tr td:contains(' + content + ')').length == 0) {

                    }else{
                        alert('Barang yang Anda pilih sudah diinput');
                        e.preventDefault();
                        return false;
                    }
                });

                var tr  = ' <tr>' +
                    '<td class="text-center"><input type="text" name="data-keluar['+id+'][]" id="idbarangkeluar" class="text-center" readonly value="'+id+'"/></td>' +
                    '<td class="text-center"><input type="text" name="data-keluar['+id+'][]" id="kdbarangkeluar" class="text-center" readonly value="'+txtkb+'"/></td>' +
                    '<td class="text-center"><input type="text" name="data-keluar['+id+'][]" id="nsa" class="text-center" readonly value="'+txtnsa+'"/></td>' +
                    '<td><input type="text" name="data-keluar['+id+'][]" id="nmbarangkeluar" class="text-capitalize" readonly value="'+txtnb+'"/></td>' +
                    '<td class="text-center text-uppercase"><input type="text" name="data-keluar['+id+'][]" id="satuan" class="text-uppercase text-center" readonly value="'+txtst+'"/></td>' +
                    '<td class="text-right"><input type="text" name="data-keluar['+id+'][]" id="harga" class="text-right" readonly value="$ '+txthrg+'"/></td>' +
                    '<td class="text-right"><input type="text" name="data-keluar['+id+'][]" id="jumlah" class="text-right" readonly value="'+txtjml+'"/></td>' +
                    '<td id="total" class="text-right">$ '+(txthrg * txtjml)+'</td>' +
                    '<td id="aksi" class="text-center">' +
                    '<a href="" data-toggle="modal" data-idrow="" class="ubah btn blue btn-xs blue-stripe btn-outline uppercase"><i class="fa fa-edit"></i> Ubah </a>' +
                    '<a href="" data-toggle="modal" data-idrow="" data-idcallback="" class="hapus btn red btn-xs red-stripe btn-outline uppercase"><i class="fa fa-trash"></i> Hapus</a>' +
                    '</td>' +
                '</tr>';

                $("#barang-keluar tbody").append(tr);
                $('form#formbarangkeluar input[type="text"]').val('');
            });
            @elseif(Request::segment(3) == 'barang-masuk')

            var vendor = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace('k'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: '{{ URL::route('api-aset-vendorTypehead') }}?q=%QUERY%',
                    wildcard: '%QUERY%'
                }
            });
            var departemen = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace('k'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: '{{ URL::route('api-aset-departemenTypehead') }}?q=%QUERY%',
                    wildcard: '%QUERY%'
                }
            });
            var barang = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace('nb'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: '{{ URL::route('api-aset-barangTypehead') }}?q=%QUERY%',
                    wildcard: '%QUERY%'
                }
            });

            vendor.initialize();
            departemen.initialize();
            barang.initialize();

            $('.vendor').typeahead(null, {
                name: 'vendor',
                hint: true,
                highlight: true,
                minLength: 1,
                limit: 10,
                displayKey: 'k',
                source: vendor,
            }).on('typeahead:selected', function (key,val) {
                $('.vendortype').val(val.v);
            });

            $('.departemen').typeahead(null, {
                name: 'departemen',
                hint: true,
                highlight: true,
                minLength: 1,
                limit: 10,
                displayKey: 'k',
                source: departemen,
            }).on('typeahead:selected', function (key,val) {
                $('.departementype').val(val.v);
            });

            $('.barang').typeahead(null, {
                name: 'barang',
                hint: true,
                highlight: true,
                minLength: 1,
                limit: 10,
                displayKey: 'nb',
                source: barang,
            }).on('typeahead:selected', function (key,val) {
                $('.nsb').val(val.nsb);
                $('.kb').val(val.kb);
                $('.nb').val(val.nb);
                $('.hrg').val(val.hrg);
                $('.st').val(val.st);
            });

            var id = 0,
                txtnsb = $('.nsb').val(''),
                txthrg = $('.hrg').val(''),
                txtst  = $('.st').val(''),
                txtjml = $('.jml').val(''),
                txtnb  = $('.nb').val(''),
                txtkb  = $('.kb').val('');

            $('.tambahBarangMasuk').on('click', function(){
                $("tr#nodata").remove();
                    txtnsb = $('.nsb').val();
                    txthrg = $('.hrg').val();
                    txtst  = $('.st').val();
                    txtjml = $('.jml').val();
                    txtnb  = $('.nb').val();
                    txtkb  = $('.kb').val();
                    id++;

                var tr  = ' <tr>' +
                    '<td class="text-center"><input type="text" name="data-masuk['+id+'][]" id="idbarang" class="text-center" readonly value="'+id+'"/></td>' +
                    '<td class="text-center"><input type="text" name="data-masuk['+id+'][]" id="kdbarang" class="text-center" readonly value="'+txtkb+'"/></td>' +
                    '<td><input type="text" name="data-masuk['+id+'][]" id="nmbarang" class="text-capitalize" readonly value="'+txtnb+'"/></td>' +
                    '<td class="text-center text-uppercase"><input type="text" name="data-masuk['+id+'][]" id="satuan" class="text-uppercase text-center" readonly value="'+txtst+'"/></td>' +
                    '<td class="text-right"><input type="text" name="data-masuk['+id+'][]" id="jumlah" class="text-right" readonly value="'+txtjml+'"/></td>' +
                    '<td class="text-right"><input type="text" name="data-masuk['+id+'][]" id="harga" class="text-right" readonly value="$ '+txthrg+'"/></td>' +
                    '<td id="total" class="text-right">$ '+(txthrg * txtjml)+'</td>' +
                    '<td id="aksi" class="text-center">' +
                        '<a href="" data-toggle="modal" data-idrow="" class="ubah btn blue btn-xs blue-stripe btn-outline uppercase"><i class="fa fa-edit"></i> Ubah </a>' +
                        '<a href="" data-toggle="modal" data-idrow="" data-idcallback="" class="hapus btn red btn-xs red-stripe btn-outline uppercase"><i class="fa fa-trash"></i> Hapus</a>' +
                    '</td>' +
                '</tr>';
                $("#barang-masuk tbody").append(tr);
                $('form#formtambahbarang input[type="text"]').val('');
            });

            @elseif(Request::segment(3) == 'identifikasi')
            $('a.isi').click(function(event){
                $("#no-aset").val($(this).data('idcallback'));
            });
            @endif
        });

        function hanyaAngka(evt){
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            return !(charCode > 31 && (charCode < 48 || charCode > 57));
        }

        function hanyaHuruf(evt){
            var keyCode = (evt.keyCode ? evt.keyCode : evt.which);
            if (keyCode > 47 && keyCode < 58) {
                evt.preventDefault();
            }
        }
    </script>
@endsection