<!DOCTYPE html>
<html>
    @yield('header')
    <body class="page-container-bg-solid page-header-menu-fixed page-sidebar-mobile-offcanvas">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header" style="height:0px !important;">

                        <div class="page-header-menu">
                            <div class="container-fluid">
                                <div class="hor-menu">
                                    <ul class="nav navbar-nav pull-left">
                                        <li class="menu-dropdown {{ Request::segment(2) == 'beranda' ? 'active' : '' }}">
                                            <a href="{{ URL::route('aset-default') }}">
                                                <i class="glyphicon glyphicon-home"></i> Beranda
                                                <span class="arrow"></span>
                                            </a>
                                        </li>
                                        <li class="menu-dropdown classic-menu-dropdown {{ Request::segment(2) == 'master-data' ? 'active' : '' }}">
                                            <a href="javascript:;">
                                                <i class="fa fa-folder-o"></i> Master Data
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-left">
                                                <li>
                                                    <a href="{{ URL::route('aset-master', ['modul' => 'departemen']) }}"><i class="fa fa-folder-open"></i> Data Departemen</a>
                                                </li>
                                                <li>
                                                    <a href="{{ URL::route('aset-master', ['modul' => 'vendor']) }}"><i class="fa fa-folder-open"></i> Data Vendor</a>
                                                </li>
                                                <li>
                                                    <a href="{{ URL::route('aset-master', ['modul' => 'kategori']) }}"><i class="fa fa-folder-open"></i> Data Kategori</a>
                                                </li>
                                                <li>
                                                    <a href="{{ URL::route('aset-master', ['modul' => 'barang']) }}"><i class="fa fa-folder-open"></i> Data Barang</a>
                                                </li>
                                                <li>
                                                    <a href="{{ URL::route('aset-master', ['modul' => 'identifikasi']) }}"><i class="fa fa-folder-open"></i> Data Identifikasi Barang</a>
                                                </li>
                                                <li>
                                                    <a href="{{ URL::route('aset-master', ['modul' => 'media']) }}"><i class="fa fa-folder-open"></i> Data Media</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-dropdown classic-menu-dropdown {{ Request::segment(2) == 'transaksi' ? 'active' : '' }}">
                                            <a href="javascript:;">
                                                <i class="fa fa-clone"></i> Transaksi
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-left">
                                                <li>
                                                    <a href="{{ URL::route('aset-transaksi', ['modul' => 'barang-masuk']) }}"><i class="fa fa-clone"></i> Input Barang Masuk</a>
                                                </li>
                                                <li>
                                                    <a href="{{ URL::route('aset-transaksi', ['modul' => 'barang-keluar']) }}"><i class="fa fa-clone"></i> Input Barang Keluar</a>
                                                </li>
                                                <li>
                                                    <a href="{{ URL::route('aset-transaksi', ['modul' => 'mutasi']) }}"><i class="fa fa-clone"></i> Input Mutasi</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-dropdown classic-menu-dropdown {{ Request::segment(2) == 'laporan' ? 'active' : '' }}">
                                            <a href="javascript:;">
                                                <i class="fa fa-map-o"></i> Laporan
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ URL::route('aset-laporan', ['modul' => 'barang-masuk']) }}"><i class="fa fa-map-o"></i> Laporan Barang Masuk</a>
                                                </li>
                                                <li>
                                                    <a href="{{ URL::route('aset-laporan', ['modul' => 'barang-keluar']) }}"><i class="fa fa-map-o"></i> Laporan Barang Keluar</a>
                                                </li>
                                                <li>
                                                    <a href="{{ URL::route('aset-laporan', ['modul' => 'mutasi']) }}"><i class="fa fa-map-o"></i> Laporan Mutasi</a>
                                                </li>
                                                <li>
                                                    <a href="{{ URL::route('aset-laporan', ['modul' => 'posisi-barang']) }}"><i class="fa fa-map-o"></i> Laporan Posisi Barang</a>
                                                </li>
                                                <li>
                                                    <a href="{{ URL::route('aset-laporan', ['modul' => 'riwayat-barang']) }}"><i class="fa fa-map-o   "></i> Laporan Riwayat Barang</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-dropdown classic-menu-dropdown {{ Request::segment(2) == 'informasi' ? 'active' : '' }}">
                                            <a href="javascript:;">
                                                <i class="fa fa-info-circle"></i> Informasi Publik
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ URL::route('aset-informasi', ['modul' => 'presiden','tahun' => '2017']) }}"><i class="fa fa-info-circle"></i> Pemilu 2012</a>
                                                </li>
                                                <li>
                                                    <a href="{{ URL::route('aset-informasi', ['modul' => 'presiden','tahun' => '2012']) }}"><i class="fa fa-info-circle"></i> Pemilu 2017</a>
                                                </li>
                                                <li>
                                                    <a href="{{ URL::route('aset-informasi', ['modul' => 'registrasi-pemilih']) }}"><i class="fa fa-info-circle"></i> Registrasi Pemilih</a>
                                                </li>
                                                <li>
                                                    <a href="{{ URL::route('aset-informasi', ['modul' => 'media']) }}"><i class="fa fa-info-circle"></i> Media</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-dropdown classic-menu-dropdown {{ Request::segment(2) == 'pengaturan' ? 'active' : '' }}">
                                            <a href="javascript:;">
                                                <i class="icon-settings"></i> Pengaturan
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-left">
                                                <li>
                                                    <a href="{{ URL::route('aset-pengaturan', ['modul' => 'bahasa']) }}"><i class="icon-settings"></i> Bahasa </a>
                                                </li>
                                                <li>
                                                    <a href="{{ URL::route('aset-pengaturan', ['modul' => 'pengguna']) }}"><i class="icon-settings"></i> Akun Pengguna </a>
                                                </li>
                                                <li>
                                                    <a href="{{ URL::route('aset-pengaturan', ['modul' => 'halaman']) }}"><i class="icon-settings"></i> Halaman </a>
                                                </li>
                                                <li>
                                                    <a href="{{ URL::route('aset-pengaturan', ['modul' => 'sql-console']) }}"><i class="icon-settings"></i> SQL Console </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-dropdown">
                                            <a href="{{ URL::route('stae-sso', ['modul' => 'keluar']) }}">
                                                <i class="fa fa-sign-out"></i> Keluar
                                                <span class="arrow"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                @if(Request::segment(2) == 'master-data')
                                    @include('Aset.Master.'.ucwords(Request::segment(3)).'.Index')
                                @elseif(Request::segment(2) == 'transaksi')
                                    <?php
                                        $req    = 'Input';
                                        $req    .= str_replace('-','',implode('-', array_map('ucfirst', explode('-', Request::segment(3)))));
                                    ?>
                                    @include('Aset.Transaksi.'.$req)
                                @elseif(Request::segment(2) == 'laporan')

                                @elseif(Request::segment(2) == 'informasi')

                                @elseif(Request::segment(2) == 'pengaturan')
                                    @include('Aset.Pengaturan.Pengguna')
                                @else
                                    @include('Aset.Master.Beranda.Index')
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row">
                <div class="page-wrapper-bottom">
                    <div class="page-footer">
                        <div class="container-fluid">
                            <div class="col-md-10 text-left">
                                {{ $tahunCopyright }} &copy;
                                <a target="_blank" href="http://stae-tl.org">{{ TITLE_ACRONYM }}</a> &nbsp;|&nbsp;
                                <a>SISTEM INFORMASI MANAJEMEN ASET</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @yield('footer')
    </body>
</html>