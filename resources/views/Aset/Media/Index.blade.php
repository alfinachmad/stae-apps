<div class="container-fluid">
    <div class="page-content-inner">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-folder-open"></i> {{ ucfirst($titleDoc) }}
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-advance table-hover" id="sample_1">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>Nama Departemen</th>
                        <th class="text-center">Nama Ruangan</th>
                        <th class="text-center">Lantai</th>
                        <th class="text-center">Nama Gedung</th>
                        <th class="text-center">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="6">Data Belum Tersedia</td>
                        </tr>
                    </tbody>
                </table>
                <a href="#tambahDepartemen" data-toggle="modal" class="btn btn-sm btn-primary sbold">
                    <i class="fa fa-plus-square-o"></i> Tambah Departemen
                </a>
            </div>
        </div>
    </div>
</div>