<div class="container-fluid">
    <div class="page-content-inner">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-folder-open"></i> {{ ucfirst($titleDoc) }}
                </div>
                <div class="actions">
                    <a class="btn btn-sm bg-blue-steel font-white" href="#tambah{{$field}}" data-toggle="modal">
                        <i class="icon-plus"></i> Tambah
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped table-bordered table-advance table-hover" id="sample_1">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Kode Departemen</th>
                                <th class="text-center">Nama Departemen</th>
                                <th class="text-center">Nama Ruangan</th>
                                <th class="text-center">Lantai</th>
                                <th class="text-center">Nama Gedung</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($numrows == 0)
                                <tr>
                                    <td colspan="7">Data Belum Tersedia</td>
                                </tr>
                            @else
                                @foreach($data as $key => $value)
                                    <tr>
                                        <td class="text-center">{{ $key + $data->firstItem() }}</td>
                                        <td class="text-center">{{ strtoupper($value['kode_departemen']) }}</td>
                                        <td>{{ strtoupper($value['nama_departemen']) }}</td>
                                        <td class="text-left">{{ ucwords(strtolower($value['nama_ruangan'])) }}</td>
                                        <td class="text-center">{{ $value['lantai'] }}</td>
                                        <td class="text-center">{{ strtoupper($value['gedung'])  }}</td>
                                        <td class="text-center">
                                            <a href="#ubah{{$field}}" data-toggle="modal" data-idrow="{{ $value['kode_departemen'] }}" class="ubah btn blue btn-xs blue-stripe btn-outline uppercase">
                                                <i class="fa fa-edit"></i> Ubah
                                            </a>
                                            <a href="#hapus{{$field}}" data-toggle="modal" data-idrow="{{ $value['kode_departemen'] }}" data-idcallback="{{ strtoupper($value['nama_departemen']) }}" class="hapus btn red btn-xs red-stripe btn-outline uppercase">
                                                <i class="fa fa-trash"></i> Hapus
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        {{ $data->links('vendor.pagination.default') }}
                    </div>
                    <div class="col-md-6 text-right">
                        <ul class="pagination">
                            <li><a style="cursor: wait !important">Total : <strong>{{ $data->total() }}</strong> Data</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('Modal.'.$field.'.Tambah'.$field)
@include('Modal.'.$field.'.Ubah'.$field)
@include('Modal.'.$field.'.Hapus'.$field)
<?php $sessionData = session('NoticeSession') ?>
@if(Session::has('NoticeSession'))
    @if($sessionData['status'] == 'failed')
        @include('Modal.ModalFailed')
    @else
        @include('Modal.ModalSuccess')
    @endif
@endif