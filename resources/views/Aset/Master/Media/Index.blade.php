<div class="container-fluid">
    <div class="page-content-inner">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-folder-open"></i> {{ ucfirst($titleDoc) }}
                </div>
                <div class="actions">
                    <a class="btn btn-sm bg-blue-steel font-white" href="#tambah{{$field}}Besar" data-toggle="modal">
                        <i class="icon-plus"></i> Tambah
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="portfolio-content portfolio-3">

                    <div id="js-grid-lightbox-gallery" class="cbp cbp-caption-active cbp-caption-zoom cbp-ready" style="height: 646px;">
                        <div class="cbp-wrapper-outer"><div class="cbp-wrapper">
                                <div class="cbp-item web-design graphic print motion" style="width: 318px; left: 0px; top: 0px;">
                                    <div class="cbp-item-wrapper">
                                        <a href="/global/plugins/cubeportfolio/ajax/project3.html" class="cbp-caption cbp-singlePageInline" data-title="World Clock Widget<br>by Paul Flavius Nechita" rel="nofollow">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="/global/img/portfolio/600x600/01.jpg" alt="">
                                            </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignLeft">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title">World Clock Widget</div>
                                                        <div class="cbp-l-caption-desc">by Paul Flavius Nechita</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="cbp-item web-design logos identity graphic" style="width: 318px; left: 328px; top: 0px;"><div class="cbp-item-wrapper">
                                        <a href="/global/plugins/cubeportfolio/ajax/project4.html" class="cbp-caption cbp-singlePageInline" data-title="Bolt UI<br>by Tiberiu Neamu" rel="nofollow">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="/global/img/portfolio/600x600/1.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignLeft">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title">Bolt UI</div>
                                                        <div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div></div>
                                <div class="cbp-item graphic print identity" style="width: 318px; left: 656px; top: 0px;"><div class="cbp-item-wrapper">
                                        <a href="/global/plugins/cubeportfolio/ajax/project3.html" class="cbp-caption cbp-singlePageInline" data-title="WhereTO App<br>by Tiberiu Neamu" rel="nofollow">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="/global/img/portfolio/600x600/02.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignLeft">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title">WhereTO App</div>
                                                        <div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div></div>
                                <div class="cbp-item web-design motion logos" style="width: 318px; left: 984px; top: 0px;"><div class="cbp-item-wrapper">
                                        <a href="/global/plugins/cubeportfolio/ajax/project4.html" class="cbp-caption cbp-singlePageInline" data-title="iDevices<br>by Tiberiu Neamu" rel="nofollow">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="/global/img/portfolio/600x600/2.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignLeft">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title">iDevices</div>
                                                        <div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div></div>
                                <div class="cbp-item identity graphic print" style="width: 318px; left: 1312px; top: 0px;"><div class="cbp-item-wrapper">
                                        <a href="/global/plugins/cubeportfolio/ajax/project3.html" class="cbp-caption cbp-singlePageInline" data-title="Seemple* Music for iPad<br>by Tiberiu Neamu" rel="nofollow">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="/global/img/portfolio/600x600/03.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignLeft">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title">Seemple* Music for iPad</div>
                                                        <div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div></div>
                                <div class="cbp-item motion print logos web-design" style="width: 318px; left: 0px; top: 328px;"><div class="cbp-item-wrapper">
                                        <a href="/global/plugins/cubeportfolio/ajax/project4.html" class="cbp-caption cbp-singlePageInline" data-title="Remind~Me Widget<br>by Tiberiu Neamu" rel="nofollow">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="/global/img/portfolio/600x600/3.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignLeft">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title">Remind~Me Widget</div>
                                                        <div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div></div>
                                <div class="cbp-item graphic logos" style="width: 318px; left: 328px; top: 328px;"><div class="cbp-item-wrapper">
                                        <a href="/global/plugins/cubeportfolio/ajax/project3.html" class="cbp-caption cbp-singlePageInline" data-title="Workout Buddy<br>by Tiberiu Neamu" rel="nofollow">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="/global/img/portfolio/600x600/04.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignLeft">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title">Workout Buddy</div>
                                                        <div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div></div>
                                <div class="cbp-item identity print logos motion" style="width: 318px; left: 656px; top: 328px;"><div class="cbp-item-wrapper">
                                        <a href="/global/plugins/cubeportfolio/ajax/project4.html" class="cbp-caption cbp-singlePageInline" data-title="Digital Menu<br>by Cosmin Capitanu" rel="nofollow">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="/global/img/portfolio/600x600/4.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignLeft">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title">Digital Menu</div>
                                                        <div class="cbp-l-caption-desc">by Cosmin Capitanu</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div></div>
                                <div class="cbp-item identity motion web-design" style="width: 318px; left: 984px; top: 328px;"><div class="cbp-item-wrapper">
                                        <a href="/global/plugins/cubeportfolio/ajax/project3.html" class="cbp-caption cbp-singlePageInline" data-title="Holiday Selector<br>by Cosmin Capitanu" rel="nofollow">
                                            <div class="cbp-caption-defaultWrap">
                                                <img src="/global/img/portfolio/600x600/05.jpg" alt=""> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignLeft">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title">Holiday Selector</div>
                                                        <div class="cbp-l-caption-desc">by Cosmin Capitanu</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div></div>
                            </div>
                        </div>
                    </div>
                    <div id="js-loadMore-lightbox-gallery" class="cbp-l-loadMore-button">
                        <a href="/global/plugins/cubeportfolio/ajax/loadMore3.html" class="cbp-l-loadMore-link btn grey-mint btn-outline" rel="nofollow">
                            <span class="cbp-l-loadMore-defaultText">LOAD MORE</span>
                            <span class="cbp-l-loadMore-loadingText">LOADING...</span>
                            <span class="cbp-l-loadMore-noMoreLoading">NO MORE WORKS</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('Modal.'.$field.'.Tambah'.$field.'Besar')