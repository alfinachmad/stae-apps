<div class="container-fluid">
    <div class="page-content-inner">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-folder-open"></i> {{ ucfirst($titleDoc) }}
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped table-bordered table-advance table-hover" id="sample_1">
                            <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Kode Barang</th>
                                <th class="text-center">Nama Barang</th>
                                <th class="text-center">Nomor Seri Aset</th>
                                <th class="text-center">Nomor Seri RDTL</th>
                                <th class="text-center">Nomor Seri Alat</th>
                                <th class="text-center">Gambar</th>
                                <th class="text-center">Aksi</th>
                                <th class="text-center">Cetak</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $key => $value)
                                    <?php $jsonnoseri   = json_encode([$value->no_seri_aset,$value->no_seri_rdtl,$value->no_seri_alat]); ?>
                                    <tr>
                                        <td class="text-center">{{ $key + $data->firstItem() }}</td>
                                        <td class="text-center">{{ $value->kode_barang }}</td>
                                        <td class="text-center">{{ $value->nama_barang }}</td>
                                        <td class="text-center">{{ $value->no_seri_aset }}</td>
                                        <td class="text-center">{{ $value->no_seri_rdtl }}</td>
                                        <td class="text-center">{{ $value->no_seri_alat }}</td>
                                        <td class="text-center">{{ $value->gambar }}</td>
                                        <td class="text-center">
                                            @if($value->statusprint == 3)
                                                <a class="btn blue btn-xs blue-stripe btn-outline uppercase">
                                                    <i class="fa fa-check"></i>
                                                </a>
                                            @else
                                            <a href="#tambahNoSeri" data-toggle="modal" data-idrow="{{ $value->kode_barang }}" data-idcallback="{{ strtoupper($value->no_seri_aset) }}" class="isi btn red btn-xs red-stripe btn-outline uppercase">
                                                <i class="fa fa-times"></i>
                                            </a>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if($value->statusprint == 3)
                                            <a href="{{ URL::route('aset-cetak',['modul' => 'cetaknoseri', 'id' => Crypt::encrypt($jsonnoseri)]) }}" target="_blank" class="btn blue btn-xs blue-stripe btn-outline uppercase">
                                                <i class="fa fa-print"></i>
                                            </a>
                                            @else
                                            -
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        {{ $data->links('vendor.pagination.default') }}
                    </div>
                    <div class="col-md-6 text-right">
                        <ul class="pagination">
                            <li><a style="cursor: wait !important">Total : <strong>{{ $data->total() }}</strong> Data</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('Modal.Barang.TambahNoSeriBarang')
<?php $sessionData = session('NoticeSession') ?>
@if(Session::has('NoticeSession'))
    @if($sessionData['status'] == 'failed')
        @include('Modal.ModalFailed')
    @else
        @include('Modal.ModalSuccess')
    @endif
@endif