<!DOCTYPE html>
<html lang="en">
    <head>
        <title>{{ TITLE_ACRONYM }} | {{ $title_page }}</title>
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
        <link href="/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="/apps/_start/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="/layouts/layout3/css/themes/blue-steel.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
        <link href="/apps/_start/style.css" rel="stylesheet" type="text/css"/>
        <style>
            .widget-body {
                box-shadow: 0 1px 3px rgba(0,0,0,0.10), 0 1px 2px rgba(0,0,0,0.15);
            }
            figure {
                font-size: 1.6em !important;
                font-family: 'Roboto Slab', serif !important;
            }

            figure img {
                width: 115px;
                margin-bottom: 15px;
            }
        </style>
    </head>

    <body>
        <header>
            <div class="container">
                <div class="row" style="margin-top: 10px; margin-bottom: 25px;">
                    <div id="header" class="col-md-12 text-center">
                        <figure>
                            <img src="/apps/img/logo-stae-upscale-2.png" width="115" />
                            <figcaption>{{ TITLE_APP }}</figcaption>
                            <figcaption>{{ $subtitle }}</figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        </header>
        <div class="widget-banner">
            <div class="container">
                <ul class="nav nav-tabs">
                    <li>
                        <a href="#tabpres" data-toggle="tab"> PEMILU PRESIDEN </a>
                    </li>
                    <li class="active">
                        <a href="#tabpar" data-toggle="tab"> PEMILU PARLEMEN </a>
                    </li>
                    <li>
                        <a href="#tabAPI" data-toggle="tab"> AKSES API </a>
                    </li>
                    <li>
                        <a href="{{ route('stae-sso', ['tipe' => 'keluar']) }}"> KELUAR </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade" id="tabPDF">
                        <h3 style="margin-top: 40px;">Download Hasil Pemilu Dalam Formnat PDF</h3>
                        <div class="row" style="margin-top: 25px;">

                        </div>
                    </div>
                    <div class="tab-pane fade" id="tabpres">
                        <h3 style="margin-top: 40px;">Hasil Pemilu Presiden Timor Leste Tahun 2017</h3>
                        <div class="row" style="margin-top: 25px;">
                            <div class="col-md-6 margin-bottom-30">
                                <div class="widget-body" style="padding-bottom: 10px !important;">
                                    <div class="widget-layout">
                                        <h3 class="widget-title">Hasil - <span class="widget-subtitle">Pemilu Presiden 2017</span></h3>
                                        <div class="btn-group widget-btn-list" role="group" aria-label="...">
                                            <a class="btn btn-default widget-btn" target="_blank" href="{{ route('pr2017') }}" data-hint="tooltip" data-original-title="Default Version">Tampilan Tabular</a>
                                        </div>
                                    </div>
                                    <a target="_blank" href="{{ route('pr2017') }}"><img src="/apps/_start/img/NASIONAL_PRES.png" alt="" width="90%"></a>
                                </div>
                            </div>
                            <div class="col-md-6 margin-bottom-30">
                                <div class="widget-body" style="padding-bottom: 10px !important;">
                                    <div class="widget-layout">
                                        <h3 class="widget-title">Hasil - <span class="widget-subtitle">Pemilu Parlemen 2017</span></h3>
                                        <div class="btn-group widget-btn-list" role="group" aria-label="...">
                                            <a class="btn btn-default widget-btn" target="_blank" href="{{ route('pr2017bar') }}" data-hint="tooltip" data-original-title="Default Version">Tampilan TV</a>
                                        </div>
                                    </div>
                                    <a target="_blank" href="{{ route('pr2017bar') }}"><img src="/apps/_start/img/NASIONAL01.png" alt="" width="90%" style="height: 281px !important;"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tabAPI">
                        <h3 style="margin-top: 40px;">Akses API Hasil Pemilu Timor Leste 2017</h3>
                        <div class="row" style="margin-top: 25px;">
                            <div class="col-md-6">
                                <div class="widget-body" style="padding-bottom: 10px !important;">
                                    <div class="widget-layout" align="left">
                                        <div class="mt-clipboard-container" style="padding-top: 0px !important;">
                                            <label>API URL</label>
                                            <input type="text" id="mt-target-1" class="form-control" readonly value="http://stae-info.app/api/tabulasi/parlemen2017/{(?:nasional|all|party|data)}/hasil">
                                            <a href="javascript:;" class="btn green mt-clipboard" data-clipboard-action="copy" data-clipboard-target="#mt-target-1">
                                                <i class="icon-note"></i> Copy URL
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade active in" id="tabpar">
                        <h3 style="margin-top: 40px;">Hasil Pemilu Parlemen Timor Leste Tahun 2017</h3>
                        <div class="row" style="margin-top: 25px;">
                            <div class="col-md-6 margin-bottom-30">
                                <div class="widget-body" style="padding-bottom: 10px !important;">
                                    <div class="widget-layout">
                                        <h3 class="widget-title">Hasil - <span class="widget-subtitle">Pemilu Parlemen 2017</span></h3>
                                        <div class="btn-group widget-btn-list" role="group" aria-label="...">
                                            <a class="btn btn-default widget-btn" target="_blank" href="{{ route('pr2017') }}" data-hint="tooltip" data-original-title="Default Version">Tampilan Tabular</a>
                                        </div>
                                    </div>
                                    <a target="_blank" href="{{ route('pr2017') }}"><img src="/apps/_start/img/NASIONAL.png" alt="" width="90%"></a>
                                </div>
                            </div>
                            <div class="col-md-6 margin-bottom-30">
                                <div class="widget-body" style="padding-bottom: 10px !important;">
                                    <div class="widget-layout">
                                        <h3 class="widget-title">Hasil - <span class="widget-subtitle">Pemilu Parlemen 2017</span></h3>
                                        <div class="btn-group widget-btn-list" role="group" aria-label="...">
                                            <a class="btn btn-default widget-btn" target="_blank" href="{{ route('pr2017bar') }}" data-hint="tooltip" data-original-title="Default Version">Tampilan Grafik Bar</a>
                                        </div>
                                    </div>
                                    <a target="_blank" href="{{ route('pr2017bar') }}"><img src="/apps/_start/img/NASIONAL_BAR.png" alt="" width="90%"></a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 margin-bottom-30">
                                <div class="widget-body" style="padding-bottom: 10px !important;">
                                    <div class="widget-layout">
                                        <h3 class="widget-title">Hasil - <span class="widget-subtitle">Pemilu Parlemen 2017</span></h3>
                                        <div class="btn-group widget-btn-list" role="group" aria-label="...">
                                            <a class="btn btn-default widget-btn" target="_blank" href="{{ route('pr2017') }}" data-hint="tooltip" data-original-title="Default Version">Tampilan Grafik Pie</a>
                                        </div>
                                    </div>
                                    <a target="_blank" href="{{ route('pr2017') }}"><img src="/apps/_start/img/NASIONAL_PIE.png" alt="" width="90%"></a>
                                </div>
                            </div>
                            <div class="col-md-6 margin-bottom-30">
                                <div class="widget-body" style="padding-bottom: 10px !important;">
                                    <div class="widget-layout">
                                        <h3 class="widget-title">Hasil - <span class="widget-subtitle">Pemilu Parlemen 2017</span></h3>
                                        <div class="btn-group widget-btn-list" role="group" aria-label="...">
                                            <a class="btn btn-default widget-btn" target="_blank" href="{{ route('pr2017p') }}" data-hint="tooltip" data-original-title="Default Version">Tampilan Partai</a>
                                        </div>
                                    </div>
                                    <a target="_blank" href="{{ route('pr2017p') }}"><img src="/apps/_start/img/21.png" alt="" width="90%"></a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 margin-bottom-30">
                                <div class="widget-body" style="padding-bottom: 10px !important;">
                                    <div class="widget-layout">
                                        <h3 class="widget-title">Hasil - <span class="widget-subtitle">Pemilu Parlemen 2017</span></h3>
                                        <div class="btn-group widget-btn-list" role="group" aria-label="...">
                                            <a class="btn btn-default widget-btn" target="_blank" href="{{ route('pr2017tv') }}" data-hint="tooltip" data-original-title="Default Version">Tampilan TV</a>
                                        </div>
                                    </div>
                                    <a target="_blank" href="{{ route('pr2017tv') }}"><img src="/apps/_start/img/NASIONAL01.png" alt="" width="90%" style="height: 281px !important;"></a>
                                </div>
                            </div>
                            <div class="col-md-6 margin-bottom-30">
                                <div class="widget-body" style="padding-bottom: 10px !important;">
                                    <div class="widget-layout">
                                        <h3 class="widget-title">Hasil - <span class="widget-subtitle">Pemilu Parlemen 2017</span></h3>
                                        <div class="btn-group widget-btn-list" role="group" aria-label="...">
                                            <a class="btn btn-default widget-btn" target="_blank" href="{{ route('pr2017ltable') }}" data-hint="tooltip" data-original-title="Default Version">LiveTable Version</a>
                                        </div>
                                    </div>
                                    <a target="_blank" href="{{ route('pr2017ltable') }}"><img src="/apps/_start/img/LIVETABLE.png" alt="" width="90%"></a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer" style="padding: 5px 0px 15px 0px !important;">
            <p class="copyright">Copyright @ <a href="http://keenthemes.com" target="_blank">STAE.</a></p>
        </footer>
        <script src="/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="/global/plugins/clipboardjs/clipboard.min.js"></script>
        <script src="/pages/scripts/components-clipboard.min.js" type="text/javascript"></script>
    </body>
</html>