<div class="row">
    <div id="header" class="col-md-6 col-md-push-3 text-center">
        <figure>
            <img src="/apps/img/logo-stae-upscale-2.png" width="115" />
            <figcaption>{{ TITLE_APP }}</figcaption>
            <figcaption>{{ $subtitle }}</figcaption>
        </figure>
    </div>
    <div class="col-md-12">
        <div class="sexy_line"></div>
    </div>
</div>
<div id="informasi" class="portlet-body" {!! (Request::segment(1) == 'tabulasi' ? 'data-route="'.$dtroute.'" data-api="'.$dtapi.'"' : '') !!}>

    @if(Request::segment(1) == 'tabulasi')
        @if($dtstatusdev)
            <div align="center" style="font-size: 35px; opacity: 0.2; position: fixed; z-index:99; color: #000; top: 30px;">
                TRIAL / KOKO
            </div>
        @else
            <div align="center" style="position: fixed; z-index:99; color: #000; top: 30px;">
            <span style="margin-left: 0px;">
                <a class="btn btn-sm default red-stripe"> ATUALIZA IKUS : {{ '2017-07-24 03:27:15' }} </a>
            </span>
            </div>
        @endif
    @elseif(Request::segment(1) == 'info' AND Request::segment(2) != 'media')
        <div align="center" style="position: fixed; z-index:99; color: #000; top: 30px;">
            <span style="margin-left: 0px;">
                <a class="btn btn-sm default red-stripe"> ATUALIZA IKUS : {{ $lastupd }} </a>
            </span>
        </div>
    @endif

    @if(Request::segment(2) == 'parlemen')
        @if(Request::segment(4) == 'SBar')
            @include('Informasi.Parlemen.SBar')
        @elseif(Request::segment(4) == 'SPie')
            @include('Informasi.Parlemen.SPie')
        @elseif(Request::segment(4) == 'bar')
            @include('Informasi.Parlemen.Bar')
        @elseif(Request::segment(4) == 'livetable')
            @include('Informasi.Parlemen.LTable')
        @elseif(Request::segment(4) == 'party')
            @include('Informasi.Parlemen.Partai')
        @elseif(Request::segment(4) == 'maps')
            @include('Informasi.Parlemen.Maps')
        @else
            @include('Informasi.Parlemen.Index')
        @endif
    @elseif(Request::segment(2) == 'presiden')
        @include('Informasi.Presiden.Index')
    @elseif(Request::segment(2) == 'total-registrasi')
        @include('Informasi.Registrasi.Total')
    @elseif(Request::segment(2) == 'registrasi-luar-negeri')
        @include('Informasi.Registrasi.LuarNegeri')
    @elseif(Request::segment(2) == 'registrasi-dalam-negeri')
        @include('Informasi.Registrasi.DalamNegeri')
    @elseif(Request::segment(2) == 'media')
        @include('Informasi.Media.Index')
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="breakingNews" id="news" style="background-color: #eff3f8 !important">
                <div class="bn-title"><h2 id="rsc" style="min-width: 240px !important; text-transform: uppercase !important"></h2><span></span></div>
                <ul>
                    <li><a href="javascript;">REZULTADU PROVIZÓRIU APURAMENTU MUNISIPAL / RAEOA / DIASPORA</a></li>
                </ul>
            </div>
        </div>

        <div class="btn-group dropup select-id" style="position: absolute; right: 30px;">
            @if(Request::segment(2) != 'media')
                <button type="button" class="btn bg-yellow-lemon bg-font-dark dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="height: 40px;">
                    <i class="fa fa-angle-up"></i>
                </button>
            @endif
            <ul id="select-slide-index" class="dropdown-menu pull-right" role="menu">
                @if(Request::segment(1) == 'tabulasi')
                    @if(Request::segment(4) == 'party')
                        @foreach($dthasil['origin']['ALL']['suara'] as $key => $value)
                            <li><a class="select-slide" href="javascript:void(0)"> {{ $value['NAMA_ACRY'] }} </a></li>
                        @endforeach
                    @else
                        @foreach($dtdistrik['origin'] as $key => $value)
                            @if($value['KD'] == '2101' OR $value['KD'] == 'ALL')
                                <li class="divider"></li>
                            @endif
                            <li><a class="select-slide" href="javascript:void(0)"> {{ ($value['ND'] == 'ALL' ? 'NASIONAL' : $value['ND']) }} </a></li>
                        @endforeach
                        @if(Request::segment(4) == 'tabular')
                            <li><a class="select-slide" href="javascript:void(0)"> GRAFIKA BAR </a></li>
                            <li><a class="select-slide" href="javascript:void(0)"> GRAFIKA PIE </a></li>
                        @endif
                    @endif
                @elseif(Request::segment(1) == 'info')
                    <li><a class="select-slide" href="{{ URL::route('media-informasi', ['tipe' => 'registrasi-dalam-negeri']) }}">NACIONAL</a></li>
                    <li><a class="select-slide" href="{{ URL::route('media-informasi', ['tipe' => 'registrasi-luar-negeri']) }}">ESTRANJEIRU</a></li>
                    <li><a class="select-slide" href="{{ URL::route('media-informasi', ['tipe' => 'total-registrasi']) }}">TOTAL</a></li>
                @endif
            </ul>
        </div>
    </div>
</div>