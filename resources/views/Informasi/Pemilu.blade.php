<div class="row">
    <div id="header" class="col-md-6 col-md-push-3 text-center">
        <figure>
            <img src="/apps/img/logo-stae-upscale-2.png" width="115" />
            <figcaption>SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL</figcaption>
            <figcaption>{{ $subtitle }}</figcaption>
        </figure>
    </div>
    <div class="col-md-12">
        <div class="sexy_line"></div>
    </div>
</div>
<div class="portlet-body">

    <div align="center" style="font-size: 35px; opacity: 0.2; position: fixed; z-index:99; color: #000; top: 30px;">
        CONTOH / EZEMPLUl
    </div>

    @if(Request::segment(2) == 'parlemen')
        @include('Informasi.Parlemen.Index')
    @elseif(Request::segment(2) == 'presiden')
        @include('Informasi.Presiden.Index')
    @elseif(Request::segment(2) == 'total-registrasi' || Request::segment(2) == 'registrasi-luar-negeri' || Request::segment(2) == 'registrasi-dalam-negeri')
        @include('Informasi.Registrasi.Registrasi')
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="breakingNews" id="news" style="background-color: #eff3f8 !important">
                <div class="bn-title"><h2 id="timer" style="min-width: 240px !important; text-transform: uppercase !important"></h2><span></span></div>
                <ul>
                    <li><a href="javascript;">Resenseamentu Eleitoral no Atualizasaun Baze Dadus 2017</a></li>
                    <li><a href="javascript;">Nullam sit amet nisl ex Curabitur tortor libero, vehicula sagittis luctus sed, lobortis sed arcu</a></li>
                    <li><a href="javascript;">Cras lorem augue, facilisis a commodo in, facilisis finibus libero vel ultrices. Curabitur tortor libero, vehicula sagittis luctus sed, lobortis sed arcu</a></li>
                    <li><a href="javascript;">Maecenas imperdiet ante vitae neque facilisis cursus Curabitur tortor libero, vehicula sagittis luctus sed, lobortis sed arcu</a></li>
                    <li><a href="javascript;">Maecenas libero ipsum, placerat in mattis vel, tincidunt quis est. Curabitur tortor libero, vehicula sagittis luctus sed, lobortis sed arcu</a></li>
                    <li><a href="javascript;">Curabitur tortor libero, vehicula sagittis luctus sed, lobortis sed arcu Curabitur tortor libero, vehicula sagittis luctus sed, lobortis sed arcu</a></li>
                    <li><a href="javascript; ">Curabitur tortor libero, vehicula sagittis luctus sed, lobortis sed arcu Curabitur tortor libero, vehicula sagittis luctus sed, lobortis sed arcu</a></li>
                </ul>
            </div>
        </div>
        <div class="btn-group dropup select-id" style="position: absolute; right: 30px;">
            <button type="button" class="btn bg-red-flamingo bg-font-dark dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="height: 40px;">
                <i class="fa fa-angle-up"></i>
            </button>
            <ul id="select-slide-index" class="dropdown-menu pull-right" role="menu">
                @foreach($municipiu as $key => $value)
                    @if($value['KD'] == '2101')
                        <li class="divider"></li>
                    @endif
                    <li><a class="select-slide" href="javascript:void(0)"> {{ $value['ND'] }} </a></li>
                @endforeach
                    <li class="divider"></li>
                    <li><a class="select-slide" href="javascript:void(0)"> NACIONAL </a></li>
            </ul>
        </div>
    </div>
</div>