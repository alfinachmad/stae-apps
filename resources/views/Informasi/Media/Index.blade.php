<div class="row">
    <div id="header" class="col-md-8 col-md-push-2 text-center">
        <figure>
            <img src="/apps/img/logo-stae-upscale-2.png" width="115" />
            <figcaption>SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL</figcaption>
            <figcaption>MEDIA INFORMASAUN</figcaption>
        </figure>
    </div>
    <div class="col-md-12">
        <div class="sexy_line"></div>
    </div>
    <div class="col-md-12">
        <div class="todo-container">
            <div class="row">
                <div class="col-md-8">
                    <ul class="todo-projects-container" style="border: none !important">
                        <div class="embed-responsive embed-responsive-16by9">
                            <video src="http://stae-info.app//api/video/1" id="myvideo" width="400" controls autoplay muted></video>
                        </div>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="todo-projects-container">
                        <li class="todo-padding-b-0 bg-blue" style="padding: 0px 20px !important;">
                            <div class="todo-head text-center" style="border: none !important; padding: 10px 0px 5px 0px;">
                                <h3 class="bg-font-dark">NEWS INFORMASAUN</h3>
                            </div>
                        </li>
                        <div class="vticker">
                            <div class="list-group">

                            </div>
                        </div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="content-body-custom" class="portlet-body" data-attr-server="{{ time() * 1000 }}">
    <div class="row">
        <div class="col-md-12">
            <div class="breakingNews" id="news" style="background-color: #eff3f8 !important">
                <div class="bn-title"><h2 id="timer" style="min-width: 250px !important; text-transform: uppercase !important"></h2><span></span></div>
                <ul>
                    <li><a href="#">Director Jeral STAE ho ekipa halao enkontru ho funsionariu consul jeral Timor-Leste iha Sydney (19/05/2017).</a></li>
                    <li><a href="#">Estabelese centro atendimentu kartaun eleitor iha Consul Honorary Melbourne no Resendea, atualiza dadus  ba eleitor nebe hela iha estado NSW, Vitoria (15/05/2017)</a></li>
                    <li><a href="#">STAE halo mudansa ba dadus Consileiru Crisogno Leandro nebe muda endereco husi Timor-Leste ba Melbourne. Iha Consul Honorary Timor-Leste (15/05/2017)</a></li>
                    <li><a href="#">Diretor Jeral STAE  Acilino Manuel Branco, halo diskusaun ho Consul Timor-Leste Sr. Francisco Filipe  iha consulado jeral Darwin. (13/05/2017)</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>