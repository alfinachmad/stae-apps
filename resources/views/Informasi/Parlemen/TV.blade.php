<div class="row">
    <div class="col-md-1">
        <img src="/apps/img/stae_logo_new.png" width="110" />
    </div>
    <div class="col-md-10 text-center" style="font-size: 22px; font-weight: bold !important;margin-top: 0px;">
        <figure>
            <figcaption>SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL</figcaption>
            <figcaption>{{ $subtitle }}</figcaption>
        </figure>
    </div>
    <div class="col-md-1">
        
    </div>
    <div class="col-md-12" style="margin-top: 0px; margin-bottom: 0px !important;">
        <div id="content-hasil-pemilu" class="div-hasil sliderdiv national-stats">
            <ul>
                <?php $index = 0; ?>
                @foreach($dthasil['origin'] as $key => $value)
                    <?php
                    $title      = '';
                    $nasional   = 'REZULTADU PROVIZ&Oacute;RIU APURAMENTU MUNIS&Iacute;PIU 12 / RAEOA / DIASPORA';
                    if($key >= 2101 AND $key <= 2601):
                        $title  = 'REZULTADU PROVIZ&Oacute;RIU APURAMENTU DIASPORA '.$value['negara'].' ~ '.$value['distrik'];
                    elseif($key == '09'):
                        $title  = 'REZULTADU PROVIZ&Oacute;RIU APURAMENTU RAEOA';
                    elseif($key == 'ALL'):
                        $title  = $nasional;
                    else:
                        $title  = 'REZULTADU PROVIZ&Oacute;RIU APURAMENTU MUNIS&Iacute;PIU '.$value['distrik'];
                    endif;

                    $suara      = $value['suara'];
                    $stats      = $value['stats'];
                    $pakta      = $stats['TAKTAIN'] / $stats['TAKTA'] * 100;
                    $ppartisi   = $stats['TVOTUS'] > 0 ? ($stats['TVOTUS'] / $stats['TELEITOR'] * 100) : 0;
                    $stprosesu  = 'PROSESU';
                    $sthotu     = 'HOTU ONA';

                    $votadetail = [
                        'feto'  => ['total' => 0, 'persen' => $stprosesu],
                        'mane'  => ['total' => 0, 'persen' => $stprosesu],
                        'lvota' => ['total' => 0, 'persen' => $stprosesu],
                        'parti' => ['total' => 0, 'persen' => $stprosesu]
                    ];
                    if($stats['TAKTAST'] == $sthotu):
                        $votadetail = [
                            'feto'  => ['total' => number_format($stats['FETO']), 'persen' => number_format($stats['PFETO'],2) . ' %'],
                            'mane'  => ['total' => number_format($stats['MANE']), 'persen' => number_format($stats['PMANE'],2) . ' %'],
                            'lvota' => ['total' => number_format($stats['LAVOTA']), 'persen' => number_format($stats['PLAVOTA'],2) . ' %'],
                            'parti' => ['total' => number_format($stats['TVOTUS']), 'persen' => number_format($stats['PPARTISIPAS'],2) . ' %']
                        ];
                    endif;
                    ?>
                    <li id="subcontent-hasil-pemilu">
                        <h4 id="judul-municipiu" class="text-center" style="font-weight: bold; font-size: 20px !important; text-shadow: 1px 0 #000;">{{ $title }}</h4>
                        <div id="statistik-pemilihan-tv" style="float: right !important; width: 42% !important; padding-right: 15px;">
                            <table id="tabel-akta" class="table" style="margin-top: 0px; margin-bottom: 5px !important;">
                                <thead class="bg-primary font-white">
                                <tr>
                                    <th colspan="3" class="text-center">AKTA OPERASAUN</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="text-left" style="width: 57% !important">TOTAL AKTA</td>
                                    <td class="text-right">{{ $stats['TAKTA'] }}</td>
                                </tr>
                                <tr>
                                    <td class="text-left">TOTAL DIZITA ONA</td>
                                    <td class="text-right">{{ $stats['TAKTAIN'] }}</td>
                                </tr>
                                <tr>
                                    <td class="text-left">SEIDAUK DIZITA</td>
                                    <td class="text-right">{{ ($stats['TAKTA'] - $stats['TAKTAIN']) }}</td>
                                </tr>
                                <tr>
                                    <td class="text-left">ESTADO AKTA</td>
                                    <td class="text-right">{{ $stats['TAKTAST'] }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right" colspan="2">
                                        <div class="progress progress-striped active" style="margin-bottom: 0px;">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="max-width: 80%; width: {{ $pakta }}%">
                                            </div>
                                            {{ number_format($pakta,2) }} %
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <table id="tabel-elektor" class="table" style="margin-bottom: 5px !important;">
                                <thead class="bg-primary font-white">
                                <tr>
                                    <th class="text-center" colspan="3">KLASIFIKASAUN VOTU</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="text-left">VOTU VALIDU</td>
                                    <td class="text-right">{{ number_format($stats['VALIDU']) }}</td>
                                    <td class="text-right">{{ number_format($stats['PVALIDU'],2) }} %</td>
                                </tr>
                                <tr>
                                    <td class="text-left">VOTU BRANKU</td>
                                    <td class="text-right">{{ number_format($stats['BRANKU']) }}</td>
                                    <td class="text-right">{{ number_format($stats['PBRANKU'],2) }} %</td>
                                </tr>
                                <tr>
                                    <td class="text-left">VOTU NULU</td>
                                    <td class="text-right">{{ number_format($stats['NULU']) }}</td>
                                    <td class="text-right">{{ number_format($stats['PNULU'],2) }} %</td>
                                </tr>
                                <tr>
                                    <td class="text-left">VOTU REKLAMADU</td>
                                    <td class="text-right">{{ number_format($stats['REKLAMA']) }}</td>
                                    <td class="text-right">{{ number_format($stats['PREKLAMA'],2) }} %</td>
                                </tr>
                                <tr>
                                    <td class="text-left">VOTU REZEITADU</td>
                                    <td class="text-right">{{ number_format($stats['REZEITADU']) }}</td>
                                    <td class="text-right">{{ number_format($stats['PREZEI'],2) }} %</td>
                                </tr>
                                </tbody>
                            </table>
                            <table id="tabel-elektor" class="table">
                                <tbody>
                                <tr>
                                    <td class="text-left">TOTAL ELEITOR</td>
                                    <td class="text-right">{{ number_format($stats['TELEITOR']) }}</td>
                                    <td class="text-right">{{ number_format(($stats['TELEITOR'] / $dttotnasional) * 100,2) }} %</td>
                                </tr>
                                <tr>
                                    <td class="text-left">PARTISIPASAUN</td>
                                    <td class="text-right">{{ $votadetail['parti']['total'] }}</td>
                                    <td class="text-right">{{ $votadetail['parti']['persen'] }}</td>
                                </tr>
                                <tr>
                                    <td class="text-left">- MANE</td>
                                    <td class="text-right">{{ $votadetail['mane']['total'] }}</td>
                                    <td class="text-right">{{ $votadetail['mane']['persen'] }}</td>
                                </tr>
                                <tr>
                                    <td class="text-left">- FETO</td>
                                    <td class="text-right">{{ $votadetail['feto']['total'] }}</td>
                                    <td class="text-right">{{ $votadetail['feto']['persen'] }}</td>
                                </tr>
                                <tr>
                                    <td class="text-left">LA TUIR VOTA</td>
                                    <td class="text-right">{{ $votadetail['lvota']['total'] }}</td>
                                    <td class="text-right">{{ $votadetail['lvota']['persen'] }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="rowcontent-hasil-pemilu" style="padding-left: 15px;">
                            <ul>
                                <li>
                                    <table id="tv" class="table table-bordered" style="width: 98% !important; float: left !important;">
                                        <thead class="bg-primary">
                                        <tr>
                                            <th class="text-center">NO.</th>
                                            <th class="text-center" colspan="2">PARTIDU / COLIGASAUN</th>
                                            <th class="text-center">TOTAL</th>
                                            <th class="text-center">%</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $i      = 0;
                                        $j      = 0;
                                        ?>
                                        @for($i=0;$i<11;$i++)
                                            <tr>
                                                <td class="text-center">{{ $i+1 }}</td>
                                                <td class="text-center"><img src="/apps/img/kandidat-partai/i{{ $i+1 }}.jpg" width="95" style="border: 1px solid #000;" /></td>
                                                <td>{{ $suara[$i]['NAMA_ACRY'] }}</td>
                                                <td class="text-right">{{ number_format($suara[$i]['TOTAL']) }}</td>
                                                <td class="text-right">{{ number_format($suara[$i]['PERSEN'],2) }} %</td>
                                            </tr>
                                            <?php $j = $i?>
                                        @endfor
                                        </tbody>
                                    </table>
                                </li>
                                <li>
                                    <table id="tv" class="table table-bordered" style="width: 95% !important; float: left !important;">
                                        <thead class="bg-primary">
                                        <tr>
                                            <th class="text-center">NO.</th>
                                            <th class="text-center" colspan="2">PARTIDU / COLIGASAUN</th>
                                            <th class="text-center">TOTAL</th>
                                            <th class="text-center">%</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i = 12;?>
                                        @for($k=($j+1);$k<count($suara);$k++)
                                            <tr>
                                                <td class="text-center">{{ $k+1 }}</td>
                                                <td class="text-center"><img src="/apps/img/kandidat-partai/i{{ $k+1 }}.jpg" width="95" style="border: 1px solid #000;" /></td>
                                                <td>{{ $suara[$k]['NAMA_ACRY'] }}</td>
                                                <td class="text-right">{{ number_format($suara[$k]['TOTAL']) }}</td>
                                                <td class="text-right">{{ number_format($suara[$k]['PERSEN'],2) }} %</td>
                                            </tr>
                                        @endfor
                                        </tbody>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <?php $index++; ?>
                @endforeach
            </ul>
        </div>
    </div>
</div>