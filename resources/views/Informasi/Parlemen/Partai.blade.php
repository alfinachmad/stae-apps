<div id="content-hasil-pemilu" class="div-hasil sliderdiv national-stats">
    <ul>
        @foreach($dthasil['origin']['ALL']['suara'] as $key => $value)
            <li id="subcontent-hasil-pemilu">
                <h4 id="judul-municipiu" class="text-center text-uppercase" style="margin-top: 20px">
                    <img src="/apps/img/kandidat-partai/i{{ $key+1 }}.jpg" width="100" style="border: 1px solid #CCC;" />
                    {{ '&nbsp;&nbsp;'.($key+1) . '. '. $value['NAMA_ACRY'] . ' - ' . $value['NAMA'] }}
                </h4>
            </li>
        @endforeach
    </ul>
    <div id="partai-bar" class="chart" style="height: 645px !important;"></div>
    {{--<div id="bar-img">
        @foreach($dthasil['origin']['ALL']['suara'] as $k => $v)
            <img src="/apps/img/kandidat-partai/i{{ $k+1 }}.jpg" width="70" style="border: 1px solid #CCC;" />
        @endforeach
    </div>--}}
</div>