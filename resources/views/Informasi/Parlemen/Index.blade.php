<div id="content-hasil-pemilu" class="div-hasil sliderdiv national-stats">
    <h4 id="judul-municipiu" class="text-center">MUNISIPIU</h4>
    <ul>
        @foreach($dthasil['origin'] as $key => $value)
            <?php
            $title      = '';
            $nasional   = 'REZULTADU PROVIZ&Oacute;RIU APURAMENTU MUNIS&Iacute;PIU 12 / RAEOA / DIASPORA';
            if($key >= 2101 AND $key <= 2601):
                $title  = 'REZULTADU PROVIZ&Oacute;RIU APURAMENTU DIASPORA '.$value['negara'].' ~ '.$value['distrik'];
            elseif($key == '09'):
                $title  = 'REZULTADU PROVIZ&Oacute;RIU APURAMENTU RAEOA';
            elseif($key == 'ALL'):
                $title  = $nasional;
            else:
                $title  = 'REZULTADU PROVIZ&Oacute;RIU APURAMENTU MUNIS&Iacute;PIU '.$value['distrik'];
            endif;

            $suara      = $value['suara'];
            $stats      = $value['stats'];
            $pakta      = $stats['TAKTAIN'] / $stats['TAKTA'] * 100;
            $ppartisi   = $stats['TVOTUS'] > 0 ? ($stats['TVOTUS'] / $stats['TELEITOR'] * 100) : 0;
            $stprosesu  = 'PROSESU';
            $sthotu     = 'HOTU ONA';

            $votadetail = [
                'feto'  => ['total' => 0, 'persen' => $stprosesu],
                'mane'  => ['total' => 0, 'persen' => $stprosesu],
                'lvota' => ['total' => 0, 'persen' => $stprosesu],
                'parti' => ['total' => 0, 'persen' => $stprosesu]
            ];
            if($stats['TAKTAST'] == $sthotu):
                $votadetail = [
                    'feto'  => ['total' => number_format($stats['FETO']), 'persen' => number_format($stats['PFETO'],2) . ' %'],
                    'mane'  => ['total' => number_format($stats['MANE']), 'persen' => number_format($stats['PMANE'],2) . ' %'],
                    'lvota' => ['total' => number_format($stats['LAVOTA']), 'persen' => number_format($stats['PLAVOTA'],2) . ' %'],
                    'parti' => ['total' => number_format($stats['TVOTUS']), 'persen' => number_format($stats['PPARTISIPAS'],2) . ' %']
                ];
            endif;
            ?>
            <li id="subcontent-hasil-pemilu" data-title="{{ $title }}">
                <div id="hasil-pemilihan" class="col-md-9">
                    @foreach($suara as $k => $v)
                        <div class="col-md-4" style="padding-right: 0px; margin-bottom: 6px !important;">
                            <div id="grid-partai" class="dashboard-stat2 mt-element-ribbon" style="border: 1px solid #CCC; padding-top: 0px; padding-bottom: 5px; padding-left: 5px; padding-right: 5px; margin-bottom: 10px; height: 85px !important;">
                                <div class="display" style="margin-bottom: 0px;">
                                    <div class="col-md-1 text-center" style="padding-left: 0px; font-size: 24px; padding-top: 20px;">
                                        <span class="font-grey-mint">{{ $k+1 }}</span>
                                    </div>
                                    <div class="col-md-3" style="padding-left: 0px !important;">
                                        <div class="icon" style="float: left !important;">
                                            <img src="/apps/img/kandidat-partai/i{{ $k+1 }}.jpg" width="140" style="border: 1px solid #CCC;" />
                                        </div>
                                    </div>
                                    <div class="col-md-8" style="padding-right: 0px !important;">
                                        <div class="number text-right" style="float: right !important;">
                                            <h3 style="font-weight: normal !important; padding-bottom: 20px; padding-top: 5px; padding-right: 5px;">
                                                <span id="total-vote" class="font-blue-madison">{{ number_format($v['TOTAL']) }}</span> &nbsp;
                                                <span class="font-blue-dark">{{ number_format($v['PERSEN'],2) }} %</span>
                                            </h3>
                                            <span style="font-size: 9px; text-transform: capitalize; width: 250px !important; display: block; border-bottom: 1px solid #CCC; ">
                                                {{ $v['NAMA'] }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div id="statistik-pemilihan" class="col-md-3">
                    <table id="tabel-akta" class="table" style="margin-top: 20px; margin-bottom: 16px !important;">
                        <thead class="bg-blue font-white">
                        <tr>
                            <th colspan="3" class="text-center">AKTA OPERASAUN</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-left" style="width: 57% !important">TOTAL AKTA</td>
                            <td class="text-right">{{ $stats['TAKTA'] }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">TOTAL DIZITA ONA</td>
                            <td class="text-right">{{ $stats['TAKTAIN'] }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">SEIDAUK DIZITA</td>
                            <td class="text-right">{{ ($stats['TAKTA'] - $stats['TAKTAIN']) }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">ESTADO AKTA</td>
                            <td class="text-right">{{ $stats['TAKTAST'] }}</td>
                        </tr>
                        <tr>
                            <td class="text-right" colspan="2">
                                <div class="progress progress-striped active" style="margin-bottom: 0px;">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="max-width: 80%; width: {{ $pakta }}%">
                                    </div>
                                    {{ number_format($pakta,2) }} %
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <table id="tabel-elektor" class="table">
                        <thead class="bg-blue font-white">
                        <tr>
                            <th class="text-center" colspan="3">KLASIFIKASAUN VOTU</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-left">VOTU VALIDU</td>
                            <td class="text-right">{{ number_format($stats['VALIDU']) }}</td>
                            <td class="text-right">{{ number_format($stats['PVALIDU'],2) }} %</td>
                        </tr>
                        <tr>
                            <td class="text-left">VOTU BRANKU</td>
                            <td class="text-right">{{ number_format($stats['BRANKU']) }}</td>
                            <td class="text-right">{{ number_format($stats['PBRANKU'],2) }} %</td>
                        </tr>
                        <tr>
                            <td class="text-left">VOTU NULU</td>
                            <td class="text-right">{{ number_format($stats['NULU']) }}</td>
                            <td class="text-right">{{ number_format($stats['PNULU'],2) }} %</td>
                        </tr>
                        <tr>
                            <td class="text-left">VOTU REKLAMADU</td>
                            <td class="text-right">{{ number_format($stats['REKLAMA']) }}</td>
                            <td class="text-right">{{ number_format($stats['PREKLAMA'],2) }} %</td>
                        </tr>
                        <tr>
                            <td class="text-left">VOTU REZEITADU</td>
                            <td class="text-right">{{ number_format($stats['REZEITADU']) }}</td>
                            <td class="text-right">{{ number_format($stats['PREZEI'],2) }} %</td>
                        </tr>
                        <tr>
                            <td class="text-left">VOTU ABANDONADO</td>
                            <td class="text-right">{{ number_format($stats['ABANDONADO']) }}</td>
                            <td class="text-right">{{ number_format($stats['PABANDO'],2) }} %</td>
                        </tr>
                        </tbody>
                    </table>
                    <table id="tabel-elektor" class="table">
                        <tbody>
                        <tr>
                            <td class="text-left">TOTAL ELEITOR</td>
                            <td class="text-right">{{ number_format($stats['TELEITOR']) }}</td>
                            <td class="text-right">{{ number_format(($stats['TELEITOR'] / $dttotnasional) * 100,2) }} %</td>
                        </tr>
                        <tr>
                            <td class="text-left">PARTISIPASAUN</td>
                            <td class="text-right">{{ $votadetail['parti']['total'] }}</td>
                            <td class="text-right">{{ $votadetail['parti']['persen'] }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">- MANE</td>
                            <td class="text-right">{{ $votadetail['mane']['total'] }}</td>
                            <td class="text-right">{{ $votadetail['mane']['persen'] }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">- FETO</td>
                            <td class="text-right">{{ $votadetail['feto']['total'] }}</td>
                            <td class="text-right">{{ $votadetail['feto']['persen'] }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">LA TUIR VOTA</td>
                            <td class="text-right">{{ $votadetail['lvota']['total'] }}</td>
                            <td class="text-right">{{ $votadetail['lvota']['persen'] }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </li>
        @endforeach
        <li id="subcontent-hasil-pemilu" data-title="{{ $nasional }}">
            <div id="nasional-bar" style="height: 675px !important;"></div>
            <div id="bar-img">
                @foreach($dthasil['origin']['ALL']['suara'] as $k => $v)
                    <img src="/apps/img/kandidat-partai/i{{ $k+1 }}.jpg" width="70" style="border: 1px solid #CCC;" />
                @endforeach
            </div>
        </li>
        <li id="subcontent-hasil-pemilu" data-title="{{ $nasional }}">
            <div id="nasional-pie" style="height: 725px !important; width: 70% !important; float: left;"></div>
            <div id="nasional-pie-legend" style="height: 725px !important; width: 30% !important; float: right;padding-left: 0px !important;"></div>
        </li>
    </ul>
</div>