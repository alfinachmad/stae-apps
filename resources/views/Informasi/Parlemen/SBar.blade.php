<div id="hasil-pemilu-parlemen-2017" class="div-hasil sliderdiv national-stats">
    <ul>
        @foreach($dthasil['origin'] as $key => $value)
        <?php
            $title      = '';
            if($key >= 2101 AND $key <= 2601):
                $title  = 'REZULTADU PROVIZ&Oacute;RIU APURAMENTU DIASPORA '.$value['negara'].' ~ '.$value['distrik'];
            elseif($key == '09'):
                $title  = 'REZULTADU PROVIZ&Oacute;RIU APURAMENTU RAEOA';
            elseif($key == 'ALL'):
                $title  = 'REZULTADU PROVIZ&Oacute;RIU APURAMENTU MUNIS&Iacute;PIU 12 / RAEOA / DIASPORA';
            else:
                $title  = 'REZULTADU PROVIZ&Oacute;RIU APURAMENTU MUNIS&Iacute;PIU '.$value['distrik'];
            endif;

            $suara      = $value['suara'];
            $stats      = $value['stats'];
            $pakta      = $stats['TAKTAIN'] / $stats['TAKTA'] * 100;
            $ppartisi   = $stats['TVOTUS'] > 0 ? ($stats['TVOTUS'] / $stats['TELEITOR'] * 100) : 0;
        ?>
        <li id="list-hasil">
            <h4 id="judul-municipiu" class="text-center">{{ $title }}</h4>
        </li>
        @endforeach
    </ul>
    {{--<h4 id="judul-municipiu" class="text-center">REZULTADU PROVIZORIU APURAMENTU NASIONAL</h4>--}}
    {{--<div id="nasional-bar" class="chart" style="height: 695px !important;"></div>--}}
    <div id="nasional-bar" class="chart" style="height: 675px !important;"></div>
    <div id="nasional-bar-img">
        @foreach($dthasil['origin']['ALL']['suara'] as $k => $v)
            <img src="/apps/img/kandidat-partai/i{{ $k+1 }}.jpg" width="70" style="border: 1px solid #CCC;" />
        @endforeach
    </div>
</div>