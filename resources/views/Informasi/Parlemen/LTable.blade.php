<?php
    header('Refresh: 8;url=' . route('pr2017ltable'));
    $distrik    = $dt['distrik'];
    $hasil      = $dt['hasil'];
?>

<h4 id="judul-municipiu" class="text-center">LIVE TABLE PEROLEHAN SUARA</h4>
<div class="table-scrollable" style="margin-bottom: 5px !important; margin-top: -5px !important">
    <table id="livetable" class="table table-bordered table-striped">
        <thead class="bg-blue font-white">
        <tr>
            <th class="text-center" valign="middle" rowspan="2" style="vertical-align: middle !important;">Distrik</th>
            <th class="text-center" colspan="21">PARTIDO</th>
        </tr>
        <tr>
            @foreach($hasil['ALL']['suara'] as $key => $value)
                <th class="text-center">{{ $value['NAMA_ACRY'] }}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($distrik as $key => $value)
            <?php
            $color  = '';
            $text   = '';
            $status = $dthasil['origin'][$value['KD']]['stats']['TAKTAST'];
            if($status == '-'):
                $color  = 'bg-red-flamingo font-white';
                $text   = '';
            elseif($status == 'HOTU ONA'):
                $color  = 'bg-yellow font-white';
            else:
                $color  = '';
                $text   = '<small class="blink_me font-red"><sup> Proses </sup></small>';
            endif;
            ?>
            <tr>
                <td>{{ ($value['ND'] == 'ALL' ? 'NASIONAL' : $value['ND']) }} {!! $text !!}</td>
                @foreach($hasil[$value['KD']]['suara'] as $k => $v)
                    <td class="text-right {{ $color }}">{{ number_format($v['TOTAL']) }}</td>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
</div>