<?php header('Refresh: 160;url=' . route('cd2017')); ?>
<div class="row">
    <div id="header" class="col-md-6 col-md-push-3 text-center">
        <figure>
            <img src="/apps/img/logo-stae-upscale-2.png" width="115" />
            <figcaption>{{ TITLE_APP }}</figcaption>
            <figcaption>{{ $subtitle }}</figcaption>
        </figure>
    </div>
    <div class="col-md-12">
        <div class="sexy_line"></div>
    </div>
</div>
<div class="portlet-body">
    <div class="row">
        <div class="col-md-12">
            <div class="todo-container">
                <div class="row">
                    <div class="col-md-8 col-md-push-2">
                        <ul class="todo-projects-container" style="border: none !important">
                            <div class="embed-responsive embed-responsive-16by9">
                                <video src="/apps/video/elpar2017.mp4" id="myvideo" width="100" controls loop autoplay muted></video>
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-1 col-lg-4 text-center"></div>
            <div class="col-md-1 col-lg-4 p-l-0 text-center">
                <div class="jam"></div>
            </div>
            <div class="col-md-1 col-lg-4 text-center"></div>
        </div>
        <div class="col-md-12">
            <div class="breakingNews" id="news" style="background-color: #eff3f8 !important">
                <div class="bn-title"><h2 id="timer" style="min-width: 240px !important; text-transform: uppercase !important"></h2><span></span></div>
                <ul>
                    <li><a href="javascript;" class="text-uppercase">eleisaun parlamentar 22 jullu 2017</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
