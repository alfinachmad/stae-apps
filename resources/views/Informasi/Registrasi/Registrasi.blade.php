<?php header('refresh:'.$refreshSec.';url='.Request::root().'/frontend/register/'.$refreshNxt); ?>
<div id="national-elector" class="sliderdiv national-stats" style="height: 750px !important; ">
    <h4 class="text-center">TOTAL {{ $subMainTitle }} - {{ $periode }}</h4>
    <div class="table-scrollable" style="overflow: hidden !important;">
        <table id="result-regis-foreign" class="table table-striped table-bordered table-hover">
            <thead class="bg-blue font-white">
            <tr>
                <th scope="col" class="text-center" rowspan="2" style="vertical-align: middle !important;"> MUNICIPIU </th>
                <th scope="col" class="text-center" colspan="3"> ELEITOR FOUN </th>
                <th scope="col" class="text-center" colspan="3"> ATUALIZA </th>
                <th scope="col" class="text-center" colspan="3"> MUDA HELA FATIN </th>
                <th scope="col" class="text-center" colspan="3"> HAMO'OS </th>
            </tr>
            <tr>
                <th scope="col" class="text-center"> MANE </th>
                <th scope="col" class="text-center"> FETO </th>
                <th scope="col" class="text-center"> TOTAL </th>
                <th scope="col" class="text-center"> MANE </th>
                <th scope="col" class="text-center"> FETO </th>
                <th scope="col" class="text-center"> TOTAL </th>
                <th scope="col" class="text-center"> MANE </th>
                <th scope="col" class="text-center"> FETO </th>
                <th scope="col" class="text-center"> TOTAL </th>
                <th scope="col" class="text-center"> MANE </th>
                <th scope="col" class="text-center"> FETO </th>
                <th scope="col" class="text-center"> TOTAL </th>
            </tr>
            </thead>
            <tbody>
            <?php
            $tBaru      = 0;
            $tKoreksi   = 0;
            $tGanti     = 0;
            $tHapus     = 0;
            ?>
            @foreach($regNew as $key => $value)
                <tr>
                    <td>{{ $value['ND'] }}</td>
                    <td>{{ number_format($value['MN']) }}</td>
                    <td>{{ number_format($value['FN']) }}</td>
                    <td>{{ number_format($value['TMFN']) }}</td>
                    <td>{{ number_format($value['MC']) }}</td>
                    <td>{{ number_format($value['FC']) }}</td>
                    <td>{{ number_format($value['TMFC']) }}</td>
                    <td>{{ number_format($value['MM']) }}</td>
                    <td>{{ number_format($value['FM']) }}</td>
                    <td>{{ number_format($value['TMFM']) }}</td>
                    <td>{{ number_format($value['MD']) }}</td>
                    <td>{{ number_format($value['FD']) }}</td>
                    <td>{{ number_format($value['TMFD']) }}</td>
                </tr>
                <?php
                $tBaru      += $value['TMFN'];
                $tKoreksi   += $value['TMFC'];
                $tGanti     += $value['TMFM'];
                $tHapus     += $value['TMFD'];
                ?>
            @endforeach
            </tbody>
            <tfoot class="bg-blue font-white">
            <tr>
                <td class="text-center">TOTAL</td>
                <td colspan="3" class="text-center">{{ number_format($tBaru) }}</td>
                <td colspan="3" class="text-center">{{ number_format($tKoreksi) }}</td>
                <td colspan="3" class="text-center">{{ number_format($tGanti) }}</td>
                <td colspan="3" class="text-center">{{ number_format($tHapus) }}</td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>