<div id="election-result" class="sliderdiv">
    <ul>
        @foreach($distrik as $key => $value)
            <li>
                <h4 class="text-center">
                    {{
                        strtoupper(trans($value['ND'])) == 'RAEOA' ? 'R.A.E.O.A' :
                            ((strtoupper(trans($value['NG'])) == 'TIMOR LESTE' ? 'MUNICÍPIO ' . strtoupper(trans($value['ND'])) : strtoupper(trans($value['NG'])) .' ~ '. strtoupper(trans($value['ND']))))
                    }}
                </h4>
            </li>
        @endforeach
    </ul>
</div>
<div id="chartdivrekapitulasaun" class="chart" style="height: 365px; !important;"></div>