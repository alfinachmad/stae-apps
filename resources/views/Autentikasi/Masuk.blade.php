<!DOCTYPE html>
<html>
    <head>
        <title>{{ TITLE_ACRONYM .' | SISTEM INFORMASI MANAJEMEN ASET '}}</title>
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
        <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="/pages/css/login.min.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <body class=" login">
        <div class="logo">
            <a href="index.html">
                <img src="/apps/img/logo-stae-upscale.png" alt="" width="125" />
            </a>
        </div>
        <div class="content" style="margin-top: 15px !important">
            <form class="login-form" action="{{ route('stae-sso-masuk-proses') }}" method="post">
                {{ csrf_field() }}
                <h3 class="form-title font-green">Masuk</h3>
                <?php $sessionData = session('NoticeSessionLogin') ?>
                @if($sessionData['status'] == 'failed')
                    <div class="alert alert-danger">
                        <button class="close" data-close="alert"></button>
                        <span>{{ $sessionData['message'] }}</span>
                    </div>
                @endif
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any username and password. </span>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Nama Pengguna / E-Mail</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" name="username" autofocus required value="*******" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Kata Kunci</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" value="WELCOME" name="password" required /> </div>
                    <input type="hidden" name="ufield" value="{{ \Illuminate\Support\Facades\Crypt::encrypt(session('url')) }}" />
                <div class="form-actions">
                    <button type="submit" class="btn green uppercase">Masuk</button>
                    <a href="javascript:;" id="forget-password" class="forget-password">Lupa Kata Kunci</a>
                </div>
                <div class="create-account">
                    <p>
                        <a href="javascript:;" id="register-btn" class="uppercase">Buat Akun Pengguna</a>
                    </p>
                </div>
            </form>
            <form class="forget-form" action="index.html" method="post">
                <h3 class="font-green">Lupa Kata Kunci ?</h3>
                <p> Isi alamat E-Mail Anda untuk mengatur ulang. </p>
                <div class="form-group">
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="E-Mail" name="email" /> </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn green btn-outline">Kembali</button>
                    <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
                </div>
            </form>
        </div>
        <div class="copyright"> {{ $tahunCopyright }} © {{ TITLE_ACRONYM .' | SISTEM INFORMASI MANAJEMEN '}} </div>
        <script src="/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <script src="/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
        <script src="/global/scripts/app.min.js" type="text/javascript"></script>
        <script src="/pages/scripts/login-5.js" type="text/javascript"></script>

    </body>
</html>