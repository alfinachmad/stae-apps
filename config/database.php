<?php

return [

    /*
    |--------------------------------------------------------------------------
    | PDO Fetch Style
    |--------------------------------------------------------------------------
    |
    | By default, database results will be returned as instances of the PHP
    | stdClass object; however, you may desire to retrieve records in an
    | array format for simplicity. Here you can tweak the fetch style.
    |
    */

    'fetch' => PDO::FETCH_OBJ,

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
        ],

        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],

        'pgsql' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'oracle' => [
            'driver'        => 'oracle',
            'tns'           => env('ODB_TNS', ''),
            'host'          => env('ODB_HOST', '192.168.45.58'),
            'port'          => env('ODB_PORT', '1521'),
            'database'      => env('ODB_DATABASE', 'STAE'),
            'username'      => env('ODB_USERNAME', 'umum'),
            'password'      => env('ODB_PASSWORD', 'umum'),
            'charset'       => env('ODB_CHARSET', 'AL32UTF8'),
            'prefix'        => env('ODB_PREFIX', ''),
            'prefix_schema' => env('ODB_SCHEMA_PREFIX', ''),
        ],

        'oracleTab' => [
            'driver'        => 'oracle',
            'tns'           => env('TAB_DB_TNS', ''),
            'host'          => env('TAB_DB_HOST', '192.168.45.56'),
            'port'          => env('TAB_DB_PORT', '1521'),
            'database'      => env('TAB_DB_DATABASE', 'stae'),
            'username'      => env('TAB_DB_USERNAME', 'tabulasi'),
            'password'      => env('TAB_DB_PASSWORD', 'tabulasi'),
            'charset'       => env('TAB_DB_CHARSET', 'AL32UTF8'),
            'prefix'        => env('TAB_DB_PREFIX', ''),
            'prefix_schema' => env('TAB_DB_SCHEMA_PREFIX', ''),
        ],

        'oraclePDidik' => [
            'driver'        => 'oracle',
            'tns'           => env('TAB_DB_TNS', ''),
            'host'          => env('TAB_DB_HOST', '192.168.25.193'),
            'port'          => env('TAB_DB_PORT', '1521'),
            'database'      => env('TAB_DB_DATABASE', 'orcl'),
            'username'      => env('TAB_DB_USERNAME', 'umum'),
            'password'      => env('TAB_DB_PASSWORD', 'umum'),
            'charset'       => env('TAB_DB_CHARSET', 'AL32UTF8'),
            'prefix'        => env('TAB_DB_PREFIX', ''),
            'prefix_schema' => env('TAB_DB_SCHEMA_PREFIX', ''),
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'cluster' => false,

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],

];
