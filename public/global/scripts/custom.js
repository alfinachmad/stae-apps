(function($) {
    $.fn.extend({
        SemuaAwalKataHurufBesar: function() {
            $(this).keyup(function(event) {
                var box         = event.target;
                var txt         = $(this).val();
                var start       = box.selectionStart;
                var end         = box.selectionEnd;
                $(this).val(txt.toLowerCase().replace(/^(.)|(\s|\-)(.)/g,
                    function(c) {
                        return c.toUpperCase();
                    }));
                box.setSelectionRange(start, end);
            });
            return this;
        },

        HurufBesarDiAwal: function() {
            $(this).keyup(function(event) {
                var box = event.target;
                var txt = $(this).val();
                var start = box.selectionStart;
                var end = box.selectionEnd;

                $(this).val(txt.toLowerCase().replace(/^(.)/g,
                    function(c) {
                        return c.toUpperCase();
                    }));
                box.setSelectionRange(start, end);
            });
            return this;
        },

        SemuaHurufKecil: function() {
            $(this).keyup(function(event) {
                var box = event.target;
                var txt = $(this).val();
                var start = box.selectionStart;
                var end = box.selectionEnd;

                $(this).val(txt.toLowerCase());
                box.setSelectionRange(start, end);
            });
            return this;
        },

        SemuaHurufBesar: function() {
            $(this).keyup(function(event) {
                var box = event.target;
                var txt = $(this).val();
                var start = box.selectionStart;
                var end = box.selectionEnd;

                $(this).val(txt.toUpperCase());
                box.setSelectionRange(start, end);
            });
            return this;
        },

        HanyaBolehAngka: function(){
            $(this).keypress(function(event){
                var charCode = (event.which) ? event.which : event.keyCode;
                return !(charCode > 31 && (charCode < 48 || charCode > 57));
            });
        },

        HanyaBolehHuruf: function(){
            $(this).keypress(function(event){
                var keyCode = (event.keyCode ? event.keyCode : event.which);
                if (keyCode > 47 && keyCode < 58) {
                    event.preventDefault();
                }
            });
        },

        HanyaEmail: function(){
            $(this).keypress(function(event){
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                return regex.test(email);
            });
        }

    });
})(jQuery);