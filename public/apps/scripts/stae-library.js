var URL     = window.location.pathname,
    HOSTNM  = window.location.hostname,
    ROOT    = window.location.protocol + '//' + HOSTNM + '/',
    xURL    = URL.split('/'),
    lxURL   = xURL.length,
    lastURL = xURL[lxURL - 1],
    dtRoute = '',
    arDtR   = '',
    APIURL  = '',
    request = '',
    clock   = '',
    txtWrt  = '',
    chrtP   = '',
    chrtPst = '';

    // Cek apakah tabulasi atau bukan.
    // Sesuai URL, /tabulasi/parlemen/2017/{tipe}, total array setelah dipisahkan oleh '/' berjumlah 5

    $(document).ready(function(){
        if(lxURL == 5){
            dtRoute = document.getElementById('informasi').dataset.route;
            APIURL  = document.getElementById('informasi').dataset.api;
            arDtR   = dtRoute.split(',');

            document.body.onkeydown = function(event){
                if((event.ctrlKey && (event.keyCode == 82)) || (event.keyCode == 116)){
                    $("div#content-hasil-pemilu ul").hide();

                    App.blockUI({
                        target: '#page-render',
                        boxed: true,
                        message: 'Processing...',
                    });

                    window.setTimeout(function() {
                        App.unblockUI('#page-render');
                        window.location.reload();
                    });
                    return false;
                }
            };

            if(arDtR.includes(lastURL)){
                var divHasil    = $('div#content-hasil-pemilu'),
                    tbHasil     = $('div#rowcontent-hasil-pemilu'),
                    liHasil     = $('li#subcontent-hasil-pemilu'),
                    arrDiv      = [],
                    chart       = new AmCharts.AmSerialChart(),
                    vAxis       = new AmCharts.ValueAxis(),
                    graph       = new AmCharts.AmGraph(),
                    chartData   = '',
                    txtFl       = '',
                    ctgry       = '',
                    chrDtTxt    = '',
                    totalLi     = liHasil.length;

                $.each(liHasil, function(i,v){
                    arrDiv.push(v.dataset.title);
                });
                $('h4#judul-municipiu').text(arrDiv[0]);

                $('a.select-slide').click(function(e) {
                    e.preventDefault();
                    var index = $('a.select-slide').index(this);
                    divHasil.data('unslider').animate(index);
                    chart.animateAgain();
                });

                if(lastURL == 'tv'){
                    tbHasil.unslider({
                        autoplay: true,
                        animation: 'vertical',
                        infinite: false,
                        arrows: false,
                        delay: 10000
                    });
                }else{
                    divHasil.unslider({
                        autoplay: true,
                        infinite: true,
                        arrows: false,
                        delay: 20000
                    });
                }

                AmCharts.loadJSON = function(url){
                    if(window.XMLHttpRequest){
                        request = new XMLHttpRequest();
                    }else{
                        request = new ActiveXObject('Microsoft.XMLHTTP');
                    }
                    request.open('GET', url, false);
                    request.send();
                    return eval(request.responseText);
                };

                AmCharts.ready(function(){
                    if(lastURL == 'party'){
                        chartData           = AmCharts.loadJSON(ROOT + APIURL + 'party/hasil');
                        chrDtTxt            = chartData[0][0];
                        txtFl               = "[[NAMA]] : <b> [[TOTAL]] </b> ";
                        ctgry               = "NAMA";
                        txtWrt              = 'partai-bar';
                    }else if(lastURL == 'bar'){
                        chartData           = AmCharts.loadJSON(ROOT + APIURL + 'all/hasil');
                        chrDtTxt            = chartData[0][0]['suara'];
                        txtWrt              = 'municipiu-bar';
                    }else{
                        chartData           = AmCharts.loadJSON(ROOT + APIURL + 'nasional/hasil');
                        chrDtTxt            = chartData[0];
                        txtFl               = "[[NAMA_ACRY]] : <b> [[TOTAL]] </b> ";
                        ctgry               = "NAMA_ACRY";
                        txtWrt              = 'nasional-bar';
                    }

                    chart.dataProvider      = chrDtTxt;

                    vAxis.gridAlpha         = 0.2;
                    vAxis.dashLength        = 0;

                    graph.type              = "column";
                    graph.valueField        = "TOTAL";
                    graph.labelText         = '[[TOTAL]] : [[PERSEN]]';
                    graph.labelPosition     = 'top';
                    graph.showAllValueLabels= true;
                    graph.labelFunction     = function(item){
                        var perse   = Math.abs(item.dataContext.PERSEN);
                        return to2(item.values.value);
                    };
                    graph.fillAlphas        = 1;
                    graph.fontSize          = 18;
                    graph.colorField        = 'COLOR';
                    graph.balloonText       = txtFl;
                    graph.lineColor         = "#BBBBBB";
                    graph.lineAlpha         = 0.2;

                    chart.startDuration         = 1;
                    chart.sequencedAnimation    = false;
                    chart.categoryField         = ctgry;
                    chart.angle             = 30;
                    chart.depth3D           = 15;
                    chart.effect            = "easeOutSine";

                    chart.addValueAxis(vAxis);
                    chart.addGraph(graph);
                    chart.write(txtWrt);
                });

                divHasil.on('unslider.change', function (event, index){
                    $('h4#judul-municipiu').text(arrDiv[index]);
                    chart.dataProvider = chrDtTxt;
                    chart.validateData();
                    chart.animateAgain();

                    if(lastURL == 'party' || lastURL == 'bar'){
                        if(index == (totalLi - 1)){
                            divHasil.data('unslider').stop();
                            setTimeout(function () {
                                App.blockUI({
                                    target: '#page-render',
                                    boxed: true,
                                    message: 'Processing...',
                                });

                                window.setTimeout(function () {
                                    App.unblockUI('#page-render');
                                    window.location.reload();
                                }, 3000);
                            }, 20000);
                        }
                    }else if(lastURL == 'tv'){
                        if (index == (totalLi - 1)) {
                            divHasil.data('unslider').stop();
                            setTimeout(function () {
                                App.blockUI({
                                    target: '#page-render',
                                    boxed: true,
                                    message: 'Processing...',
                                });

                                window.setTimeout(function () {
                                    App.unblockUI('#page-render');
                                    window.location.reload();
                                }, 3000);
                            }, 20000);
                        }
                    }else{
                        if(index == (totalLi - 1)){
                            divHasil.data('unslider').stop();
                            setTimeout(function () {
                                App.blockUI({
                                    target: '#page-render',
                                    boxed: true,
                                    message: 'Processing...',
                                });

                                window.setTimeout(function () {
                                    App.unblockUI('#page-render');
                                    divHasil.data('unslider').animate(0);
                                }, 3000);
                            }, 60000);
                        }else if(index == 19){
                            divHasil.data('unslider').stop();
                            setTimeout(function () {
                                divHasil.data('unslider').next();
                            }, 60000);
                        }else if (index == 20){
                            divHasil.data('unslider').stop();
                            setTimeout(function () {
                                divHasil.data('unslider').next();
                            }, 60000);
                        }
                    }
                });

                chrtPst = {
                    dataProvider: chart.dataProvider = chartData[0],
                    angle: 30,
                    balloonText: "[[title]]: [[valueTotal]]",
                    depth3D: 20,
                    labelFunction: function(item){
                        var value = item.value;
                        var percent = item.percents.toFixed(2);
                        return item.title + ": " + to2(value) + " (" + percent + "%)";
                    },
                    maxAngle: 60,
                    maxDepth: 30,
                    minAngle: 0,
                    minDepth: 1,
                    outlineAlpha: 0.4,
                    startDuration: 0,
                    theme: "light",
                    titleField: "NAMA_ACRY",
                    type: "pie",
                    valueField: "TOTAL",
                    colorField: 'COLOR',
                    pulledField: 'PULLEDOUT',
                    pullOutOnlyOne: true
                },
                chrtP = AmCharts.makeChart("nasional-pie",chrtPst);
                chrtP.addListener('rendered', function(event){
                    var legend          = document.getElementById('nasional-pie-legend'),
                        text            = '<table id="legend-pie"><tbody>';

                    chart.customLegend  = legend;
                    for(var i in chart.chartData){
                        var row     = chart.chartData[i],
                            color   = chart.colors[i],
                            dataCtx = row.dataContext,
                            percent = Math.round(dataCtx.PERSEN * 100) / 100,
                            value   = dataCtx.TOTAL;

                        text    += '' +
                            '<tr>' +
                            '<td align="center"><img src="/apps/img/kandidat-partai/i'+ (parseInt(i) + 1) +'.jpg" width="60" style="border: 1px solid #CCC;" /></td>'+
                            '<td>'+(parseInt(i) + 1) +'. '+ dataCtx.NAMA_ACRY+'</td>'+
                            '<td align="right">'+to2(value)+'</td>'+
                            '<td align="right">'+percent+' %</td>'+
                            '</tr>';
                    }
                    text                += '</tbody></table>';
                    legend.innerHTML    = text;
                });
            }
        }else{
            clock   = $('.jam').FlipClock(0, {
                countdown: true,
                clockFace: 'DailyCounter',
            });

            $("#news").breakingNews({
                effect		:"slide-h",
                autoplay	:true,
                timer		: 10000,
                color		:'purple'
            });
        }
    });

