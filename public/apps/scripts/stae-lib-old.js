@if(Request::segment(1) == 'counting-down')

    $(document).ready(function() {
        var clock   = $('.jam').FlipClock(0, {
            countdown: true,
            clockFace: 'DailyCounter',
        });

        $("#news").breakingNews({
            effect		:"slide-h",
            autoplay	:true,
            timer		: 10000,
            color		:'purple'
        });
        setLiveDateTime();
    });

@else

$(document).ready(function(){

    // AVOID CTRL + R AND F5 BUTTON FOR REFRESH MANUALLY
    document.body.onkeydown = function(evt){
        if((evt.ctrlKey && (evt.keyCode == 82)) || (evt.keyCode == 116) ){
            $("div#content-hasil-pemilu ul").hide();

            $(window).load(function(){
                $("div#content-hasil-pemilu ul").hide();
            });

            App.blockUI({
                target: '#page-render',
                boxed: true,
                message: 'Processing...',
            });

            window.setTimeout(function() {
                App.unblockUI('#page-render');
                window.location.reload();
            });
            return false;
        }
    };


            @if(Request::segment(4) != 'tv')

    // SHOW REAL DATE & TIME
    //setLiveDateTime();

    // SHOW FOOTER STICKER NEWS
        $("#news").breakingNews({
            effect		:"slide-h",
            autoplay	:true,
            timer		: 10000,
            color		:'purple'
        });
            @endif

            @if(Request::segment(2) == 'media')
    // SHOW SIDE NEWS
        var dd = $('.vticker').easyTicker({
            direction: 'left',
            easing: 'easeInOutBack',
            speed: 'slow',
            interval: 15000,
            height: 'auto',
            visible: 4,
            mousePause: 0,
            controls: {
                up: '.up',
                down: '.down',
                toggle: '.toggle',
                stopText: 'Stop !!!'
            }
        }).data('easyTicker');
            @endif

            @if(Request::segment(1) == 'tabulasi')

    // Ambil Data Secara External
        AmCharts.loadJSON   = function(url){
            if(window.XMLHttpRequest){
                var request = new XMLHttpRequest();
            }else{
                var request = new ActiveXObject('Microsoft.XMLHTTP');
            }
            request.open('GET', url, false);
            request.send();
            return eval(request.responseText);
        };

    // Tabulasi Parlemen
    @if(Request::segment(2) == 'parlemen')

        var divHasil    = $('div#content-hasil-pemilu'),
            tbHasil     = $('div#rowcontent-hasil-pemilu'),
            liTbHasil   = $('div#rowcontent-hasil-pemilu li'),
            liHasil     = $('li#subcontent-hasil-pemilu'),
            arrDiv      = [],
            chartData   = '';

    divHasil.unslider({
        autoplay: true,
        infinite: true,
        arrows: false,
        delay: 20000
    });

    var chart       = '',
        @if(Request::segment(4) == 'party')

        chartData   = AmCharts.loadJSON('{{ route('api-tabulasi-parlemen2017', ['tipe' => 'party']) }}');
            @elseif(Request::segment(4) == 'bar')

    chartData   = AmCharts.loadJSON('{{ route('api-tabulasi-parlemen2017', ['tipe' => 'all']) }}');
            @else
    @if(Request::segment(4) == 'tv')

        chartData   = '';
            @else

    chartData   = AmCharts.loadJSON('{{ route('api-tabulasi-parlemen2017', ['tipe' => 'nasional']) }}');
            @endif
            @endif

            @if(Request::segment(4) == 'tabular' OR Request::segment(4) == 'party' OR Request::segment(4) == 'bar')

    // Cetak Chart Bar Nasional

    AmCharts.ready(function() {
        chart = new AmCharts.AmSerialChart();
                @if(Request::segment(4) == 'party')

            chart.dataProvider = chartData[0][0];
                @elseif(Request::segment(4) == 'bar')

        chart.dataProvider = chartData[0][0]['suara'];
                @else

        chart.dataProvider = chartData[0];
                @endif

        chart.startDuration = 1;
        chart.sequencedAnimation = false;

        // VALUE AXIS
        var valueAxis           = new AmCharts.ValueAxis();
        valueAxis.gridAlpha     = 0.2;
        valueAxis.dashLength    = 0;
        chart.addValueAxis(valueAxis);

        // GRAPH
        var graph               = new AmCharts.AmGraph();
        graph.type              = "column";
        graph.valueField        = "TOTAL";
        graph.labelText         = '[[TOTAL]] : [[PERSEN]]';
        graph.labelPosition     = 'top';
        graph.showAllValueLabels= true;
        graph.labelFunction     = function(item){
            var perse   = Math.abs(item.dataContext.PERSEN);
            return to2(item.values.value);
        };
        graph.fillAlphas        = 1;
        graph.fontSize          = 18;
        graph.colorField        = 'COLOR';
                @if(Request::segment(4) == 'party')

            graph.balloonText       = "[[NAMA]] : <b> [[TOTAL]] </b> ";
        chart.categoryField     = "NAMA";

                @else

        graph.balloonText       = "[[NAMA_ACRY]] : <b> [[TOTAL]] </b> ";
        chart.categoryField     = "NAMA_ACRY";
                @endif

        graph.lineColor         = "#BBBBBB";
        graph.lineAlpha         = 0.2;

        chart.angle             = 30;
        chart.depth3D           = 15;
        chart.effect            = "easeOutSine";
        chart.startDuration     = 2;
        chart.sequencedAnimation    = 1;
        chart.addGraph(graph);
                @if(Request::segment(4) == 'party')

            chart.write('partai-bar');

                @elseif(Request::segment(4) == 'bar')

        chart.write('municipiu-bar');

                @else

        chart.write('nasional-bar');

                @endif

    });

            @if(Request::segment(4) == 'tabular')

    // Cetak Chart Pie Nasional
        var chartSettings = {
                dataProvider: chart.dataProvider = chartData[0],
                angle: 30,
                balloonText: "[[title]]: [[valueTotal]]",
                depth3D: 20,
                labelFunction: function(item){
                    var value = item.value;
                    var percent = item.percents.toFixed(2);
                    return item.title + ": " + to2(value) + " (" + percent + "%)";
                },
                maxAngle: 60,
                maxDepth: 30,
                minAngle: 0,
                minDepth: 1,
                outlineAlpha: 0.4,
                startDuration: 0,
                theme: "light",
                titleField: "NAMA_ACRY",
                type: "pie",
                valueField: "TOTAL",
                colorField: 'COLOR',
                pulledField: 'PULLEDOUT',
                pullOutOnlyOne: true
            },
            chart   = AmCharts.makeChart("nasional-pie",chartSettings);
    chart.addListener('rendered', function(event){
        var legend          = document.getElementById('nasional-pie-legend'),
            text            = '<table id="legend-pie"><tbody>';

        chart.customLegend  = legend;
        for(var i in chart.chartData){
            var row     = chart.chartData[i],
                color   = chart.colors[i],
                dataCtx = row.dataContext,
                percent = Math.round(dataCtx.PERSEN * 100) / 100,
                value   = dataCtx.TOTAL;

            text    += '' +
                '<tr>' +
                '<td align="center"><img src="/apps/img/kandidat-partai/i'+ (parseInt(i) + 1) +'.jpg" width="60" style="border: 1px solid #CCC;" /></td>'+
                '<td>'+(parseInt(i) + 1) +'. '+ dataCtx.NAMA_ACRY+'</td>'+
                '<td align="right">'+to2(value)+'</td>'+
                '<td align="right">'+percent+' %</td>'+
                '</tr>';
        }
        text                += '</tbody></table>';
        legend.innerHTML    = text;
    });
            @endif


    $.each(liHasil, function(i,v){
        arrDiv.push(v.dataset.title);
    });

    // Cetak Data Tabular Div
    $('h4#judul-municipiu').text(arrDiv[0]);
    divHasil.on('unslider.change', function(event,index){
        $('h4#judul-municipiu').text(arrDiv[index]);

                @if(Request::segment(4) == 'party' OR Request::segment(4) == 'bar')
        @if(Request::segment(4) == 'bar')

            chart.dataProvider  = chartData[0][index]['suara'];
                    @else

        chart.dataProvider  = chartData[0][index];
                    @endif

        chart.validateData();
        chart.animateAgain();

        var total   = liHasil.length;
        if(index == (total - 1)){
            divHasil.data('unslider').stop();
            setTimeout(function(){
                App.blockUI({
                    target: '#page-render',
                    boxed: true,
                    message: 'Processing...',
                });

                window.setTimeout(function() {
                    App.unblockUI('#page-render');
                    window.location.reload();
                }, 3000);
            }, 20000);
        }
                @else

        var total   = liHasil.length;
        if(index == (total - 1)){
            divHasil.data('unslider').stop();
            setTimeout(function(){
                App.blockUI({
                    target: '#page-render',
                    boxed: true,
                    message: 'Processing...',
                });

                window.setTimeout(function() {
                    App.unblockUI('#page-render');
                    divHasil.data('unslider').animate(0);
                }, 3000);
            }, 60000);
        } else if(index == 19){
            divHasil.data('unslider').stop();
            setTimeout(function(){
                divHasil.data('unslider').next();
            },60000);
        } else if(index == 20){
            divHasil.data('unslider').stop();
            setTimeout(function(){
                divHasil.data('unslider').next();
            },60000);
        }
                @endif

    });

    // Cetak Daftar Pemanggil Slide
    $('a.select-slide').click(function(e) {
        e.preventDefault();
        var index = $('a.select-slide').index(this);
        divHasil.data('unslider').animate(index);
        chart.animateAgain();
    });


            @elseif(Request::segment(4) == 'bar')
            @elseif(Request::segment(4) == 'party')
            @elseif(Request::segment(4) == 'livetable')
            @elseif(Request::segment(4) == 'tv')

    tbHasil.unslider({
        autoplay: true,
        animation: 'vertical',
        infinite: false,
        arrows: false,
        delay: 10000
    });

    divHasil.unslider({
        autoplay: true,
        infinite: false,
        arrows: false,
        delay: 20000
    });

    divHasil.on('unslider.change', function(event,index) {
        var total = liHasil.length;
        if (index == (total - 1)) {
            divHasil.data('unslider').stop();
            setTimeout(function () {
                App.blockUI({
                    target: '#page-render',
                    boxed: true,
                    message: 'Processing...',
                });

                window.setTimeout(function () {
                    App.unblockUI('#page-render');
                    window.location.reload();
                }, 3000);
            }, 20000);
        }
    });

    // Cetak Daftar Pemanggil Slide
    $('a.select-slide').click(function(e) {
        e.preventDefault();
        var index = $('a.select-slide').index(this);
        divHasil.data('unslider').animate(index);
    });

                    @endif
                    @else

    @endif

                    @elseif(Request::segment(2) == 'media')
    var myvid = document.getElementById('myvideo');
    var myvids = [
        "{{ url('/apps/video/vid1.mp4') }}",
        "{{ url('/apps/video/vid2.mp4') }}",
        "{{ url('/apps/video/vid3.mp4') }}",
        "{{ url('/apps/video/vid4.mp4') }}",
        "{{ url('/apps/video/vid5.mp4') }}",
        "{{ url('/apps/video/default.mp4') }}"
    ];
    var activeVideo = 0;

    myvid.addEventListener('ended', function(e) {
        // update the active video index
        activeVideo = (++activeVideo) % myvids.length;

        // update the video source and play
        myvid.src = myvids[activeVideo];
        myvid.play();
    });
            @endif

});
@endif