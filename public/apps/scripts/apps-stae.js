function setLiveDateTime(){
    var maincontent = document.getElementById('content-body-custom'),
        timer       = document.getElementById('timer'),
        tmstmps     = maincontent.dataset.attrServer,
        localTime   = +Date.now(),
        timeDiff    = tmstmps - localTime,
        month       = ['janeiro','fevereiro','março','abril','maio','junho','julho','agosto','setembro','outubro','novembr','dezembro'],
        day         = ['domingo','segunda-feira','terça-feira','quarta-feira','quinta-feira','sexta-feira','sábado'];

    return setInterval(function(){
        var time        = new Date(+Date.now() + timeDiff),
            dy          = time.getDay(),
            d           = time.getDate(),
            m           = time.getMonth(),
            y           = time.getFullYear(),
            h           = time.getHours(),
            min         = time.getMinutes(),
            s           = time.getSeconds(),
            hh          = (h < 10 ? '0'+h : h),
            mm          = (min < 10 ? '0'+min : min),
            ss          = (s < 10 ? '0'+s : s),
            now         = day[dy] +', '+ hh +':'+ mm +':'+ ss + ((hh >= 12) ? ' PM' : ' AM');
        timer.innerHTML = now;
    }, 1000);
}

function to2(param){
    var rev     = parseInt(param, 10).toString().split('').reverse().join('');
    var rev2    = '';
    for(var i = 0; i < rev.length; i++){
        rev2  += rev[i];
        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
            rev2 += ',';
        }
    }
    return rev2.split('').reverse().join('');
}

if(typeof(EventSource) !== 'undefined'){
    var hostn   = window.location.hostname;
    var source  = new EventSource('http://stae-info.app/clockinfo');
    source.onmessage = function(event){
        document.getElementById("rsc").innerHTML = event.data + "<br>";
    };
}else{
    document.getElementById("rsc").innerHTML = "Sorry, your browser does not support server-sent events...";
}