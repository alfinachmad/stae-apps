<?php

    /*
    |--------------------------------------------------------------------------
    | ORMKategori.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : ORMKategori.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Support\Model;

    use Illuminate\Database\Eloquent\Model;

    class ORMKategori extends Model {

        public $timestamps,$incrementing  = false;
        protected   $connection = 'oracle',
                    $table      = 'STAE_ASET.tbl_kategori',
                    $primaryKey = 'kode_kategori';

        protected function getDateFormat(){
            return 'U';
        }

        public function barang(){
            $this->belongsTo('ORMBarang');
        }

    }