<?php

    /*
    |--------------------------------------------------------------------------
    | ORMPengguna.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : ORMPengguna.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Support\Model;

    use Illuminate\Database\Eloquent\Model;

    class ORMPengguna extends Model {

        protected   $connection = 'oracle',
                    $primaryKey = 'kode_pengguna',
                    $table      = 'STAE_ASET.tbl_pengguna';
        public      $timestamps     = false,
                    $incrementing   = false;

        protected function getDateFormat(){
            return 'U';
        }

    }