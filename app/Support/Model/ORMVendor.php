<?php

    /*
    |--------------------------------------------------------------------------
    | ORMVendor.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : ORMVendor.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Support\Model;

    use Illuminate\Database\Eloquent\Model;

    class ORMVendor extends Model {

        protected   $connection = 'oracle',
                    $table      = 'STAE_ASET.tbl_vendor';
        public      $timestamps     = false,
                    $incrementing   = false;

        protected function getDateFormat(){
            return 'U';
        }

    }