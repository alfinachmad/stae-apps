<?php

    /*
    |--------------------------------------------------------------------------
    | ORMBarangHMasuk.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : ORMBarangHMasuk.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Support\Model;

    use Illuminate\Database\Eloquent\Model;

    class ORMBarangHMasuk extends Model {

        public $timestamps  = false;
        protected   $connection = 'oracle',
                    $table      = 'STAE_ASET.tbl_hmasuk',
                    $primaryKey = 'id_hmasuk';

        protected function getDateFormat(){
            return 'U';
        }

    }