<?php

    /*
    |--------------------------------------------------------------------------
    | ORMBarang.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : ORMBarang.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Support\Model;

    use Illuminate\Database\Eloquent\Model;

    class ORMBarang extends Model {

        public $timestamps,$incrementing  = false;
        protected   $connection = 'oracle',
                    $table      = 'STAE_ASET.tbl_barang',
                    $primaryKey = 'kode_barang';

        protected function getDateFormat(){
            return 'U';
        }

    }