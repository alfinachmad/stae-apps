<?php

    /*
    |--------------------------------------------------------------------------
    | ORMDepartemen.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : ORMDepartemen.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Support\Model;

    use Illuminate\Database\Eloquent\Model;

    class ORMDepartemen extends Model {

        protected   $connection = 'oracle',
                    $primaryKey = 'kode_departemen',
                    $table      = 'STAE_ASET.tbl_departemen';
        public      $timestamps     = false,
                    $incrementing   = false;


        protected function getDateFormat(){
            return 'U';
        }

    }