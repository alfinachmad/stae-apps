<?php

    /*
    |--------------------------------------------------------------------------
    | ORMBarangSeri.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : ORMBarangSeri.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Support\Model;

    use Illuminate\Database\Eloquent\Model;

    class ORMBarangSeri extends Model {

        public $timestamps  = false;
        protected   $connection = 'oracle',
                    $table      = 'STAE_ASET.tbl_barang_seri',
                    $primaryKey = 'id_barang_seri';

        protected function getDateFormat(){
            return 'U';
        }

    }