<?php

    /*
    |--------------------------------------------------------------------------
    | ORMPenggunaTabulasi.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : ORMPenggunaTabulasi.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Support\Model;

    use Illuminate\Database\Eloquent\Model;

    class ORMPenggunaTabulasi extends Model {

        protected   $connection = 'oracle',
                    $primaryKey = 'kode_pengguna',
                    $table      = 'STAE_2014.muser';
        public      $timestamps     = false,
                    $incrementing   = false;

        protected function getDateFormat(){
            return 'U';
        }

    }