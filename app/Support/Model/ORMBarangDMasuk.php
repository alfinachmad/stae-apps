<?php

    /*
    |--------------------------------------------------------------------------
    | ORMBarangDMasuk.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : ORMBarangDMasuk.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Support\Model;

    use Illuminate\Database\Eloquent\Model;

    class ORMBarangDMasuk extends Model {

        public $timestamps  = false;
        protected   $connection = 'oracle',
                    $table      = 'STAE_ASET.tbl_dmasuk',
                    $primaryKey = 'id_dmasuk';

        protected function getDateFormat(){
            return 'U';
        }

    }