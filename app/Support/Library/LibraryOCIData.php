<?php

    /*
    |--------------------------------------------------------------------------
    | LibraryOCIData.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : LibraryOCIData.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Support\Library;

    use App\Http\Controllers\STAEController,
        Carbon\Carbon;

    class LibraryOCIData extends STAEController {

        private static  $package    = [
                            'tabulasiDistrik'   => 'stae_pemilu.PACKAGE_TABULASI.iDistrik',
                            'tabulasiHasil'     => 'stae_pemilu.PACKAGE_TABULASI.web_info',
                            'infoABD'           => 'STAE_2014.PACKAGE_ELEKTOR.abd_rekap_distrik'
                        ],
                        $idelector  = [
                            'president'         => ['20170320'],
                            'parlement'         => '2017.07.22'
                        ],
                        $color      = [
                            '#C1332A','#3BA3D3','#006233','#26C281','#ED1B24','#8BC4DF','#0E509F',
                            '#008E46','#C8D046','#F0DD23','#3974BA','#31B44A','#151A5E','#1CC4F3',
                            '#A11C21','#D81F25','#F8EC0C','#E96524','#2A4EA2','#298CCD','#ED1B24'
                        ];

        public static function AmbilDataPeriodeRegistrasiPemilih(){
            return parent::ociConnection(1)->table('stae_2014.abd')->select('awal','akhir')->where('aktiv','YA')->get();
        }

        private static function AmbilPackage($param){
            if(is_array($param)):
                extract($param);
                $package    = self::$package[$npackage];
                $bindings   = $nbindings;
                $connection = parent::ociConnection($ndb);
                return $connection->executeProcedure($package,$bindings);
            endif;
        }

        private static function AmbilDistrik(){
            $nparam = [
                'ndb'       => 0,
                'npackage'  => 'tabulasiDistrik',
                'nbindings' => [
                    'kode_distrik'  => 'ALL'
                ]
            ];

            $all    = [
                '19'    => [
                    'ND'    => 'ALL',
                    'KD'    => 'ALL',
                    'NG'    => 'ALL',
                    'KL'    => 'ALL',
                ]
            ];

            $exec   = self::AmbilPackage($nparam);
            $return = [];

            foreach($exec as $key => $value):
                $return[]   = [
                    'ND'    => $value['nama_distrik'],
                    'KD'    => $value['kode_distrik'],
                    'NG'    => $value['nama_negara'],
                    'KL'    => $value['kelompok']
                ];
            endforeach;
            $merge  = array_merge($return,$all);

            return $merge;
        }

        public static function AmbilHasilRegistrasiPemilih(){
            $getPeriode = self::AmbilDataPeriodeRegistrasiPemilih();
            $decode	    = json_decode($getPeriode, true);
            $periode    = $decode[0];
            $awal	    = Carbon::parse($periode['awal'])->format('Y-m-d');
            $akhir	    = Carbon::parse($periode['akhir'])->format('Y-m-d');

            $param      = [1,'ALL',0];
            $nparam     = [
                'ndb'       => 1,
                'npackage'  => 'infoABD',
                'nbindings' => [
                    'group'     => $param[2],
                    'index'     => $param[2],
                    'bahasa'    => $param[2],
                    'kode_distrik'      => $param[1],
                    'kode_subdistrik'   => $param[1],
                    'kode_suko' => $param[1],
                    'kode_aldeia'   => $param[1],
                    'kode_elektor_awal' => $param[1],
                    'kode_elektor_akhir' => $param[1],
                    'tanggal1' => $awal,
                    'tanggal2' => $akhir,
                    'kode_input' => $param[1],
                    'kode_distrik_input' => $param[1],
                    'usia1' => $param[2],
                    'usia2' => $param[2],
                    'usia3' => $param[2],
                    'usia5' => $param[2],
                    'like'  => $param[2],
                    'nama'  => $param[1]
                ]
            ];

            $exec       = self::AmbilPackage($nparam);
            $return     = [];
            $periode    = '';
            foreach($exec as $key => $value):
                $periodeaW  = Carbon::parse($value['periode_awal'])->format('Y-m-d');
                $periodeaK  = Carbon::parse($value['periode_akhir'])->format('Y-m-d');
                $keyReturn  = $value['kelompok'] == 'DALAM NEGERI' ? 'DN' : 'LN';
                $periode    = $periodeaW . ' TO\'O ' . $periodeaK;
                $return[$keyReturn][]   = [
                    'ND'    => $value['nama_distrik'],
                    'MN'    => $value['pria_baru'],
                    'FN'    => $value['wanita_baru'],
                    'MC'    => $value['pria_koreksi'],
                    'FC'    => $value['wanita_koreksi'],
                    'MM'    => $value['pria_pindah'],
                    'FM'    => $value['wanita_pindah'],
                    'MD'    => $value['pria_hapus'],
                    'FD'    => $value['wanita_hapus'],
                    'TMFC'  => $value['total_pria_wanita_koreksi'],
                    'TMFN'  => $value['total_pria_wanita_baru'],
                    'TMFM'  => $value['total_pria_wanita_pindah'],
                    'TMFD'  => $value['total_pria_wanita_hapus']
                ];
            endforeach;
            $return['periode']  = $periode;
            $return['ddReg']    = $exec;
            $return['lastupd']  = parent::defaultTimeZone()->toDateTimeString();
            return $return;
        }

        public static function AmbilDataParlemen(){
            $distrik    = self::AmbilDistrik();
            $return     = [];

            $eleitornas = 0;
            foreach($distrik as $key => $value):
                $nparam     = [
                    'ndb'       => 0,
                    'npackage'  => 'tabulasiHasil',
                    'nbindings' => [
                        'kode_pemilu'   => self::$idelector['parlement'],
                        'kode_distrik'  => $value['KD']
                    ]
                ];
                $exec               = self::AmbilPackage($nparam);
                $return['lastupd']  = parent::defaultTimeZone()->toDateTimeString();
                $return['nextupd']  = parent::defaultTimeZone()->addMinutes(parent::cacheExpire())->toDateTimeString();
                if($value['KD'] != 'ALL'):
                    $eleitornas += $exec[$key]['ta'];
                endif;

                // UNTUK DATA NAMA - NAMA DISTRIK
                $return['distrik'][]    = $value;

                // UNTUK DATA HASIL BERDASARKAN DISTRIK & BERDASARKAN PARTAI
                foreach($exec as $k => $v):
                    $nama               = explode(' - ', $v['nama_peserta']);
                    $pulled             = max(array_column($exec, 'total'));
                    $return['hasil'][$value['KD']]['negara']    = $value['NG'];
                    $return['hasil'][$value['KD']]['distrik']   = $value['ND'];
                    $return['hasil'][$value['KD']]['suara'][]   = [
                        'NAMA'          => ($k == 20 ? $nama[1] . ' ' . $nama[2] : $nama[1]),
                        'NAMA_ACRY'     => $v['nama_singkat'],
                        'TOTAL'         => $v['total'],
                        'TOTAL_ALL'     => $v['total_all'],
                        'PERSEN'        => $v['persen'],
                        'KODE_DISTRIK'  => $v['nama_distrik'] . '-' . $v['kode_distrik'],
                        'COLOR'         => self::$color[$k],
                        'PULLEDOUT'     => ($pulled == $v['total'] ? true : false)
                    ];
                    $return['total'][$key]['ndistrik']  = $value['ND'];
                    $return['total'][$key]['suara'][]   = [
                        'NAMA'          => ($k == 20 ? $nama[1] . ' ' . $nama[2] : $nama[1]),
                        'NAMA_ACRY'     => $v['nama_singkat'],
                        'TOTAL'         => $v['total'],
                        'TOTAL_ALL'     => $v['total_all'],
                        'PERSEN'        => $v['persen'],
                        'KODE_DISTRIK'  => $v['nama_distrik'] . '-' . $v['kode_distrik'],
                        'COLOR'         => self::$color[$k],
                        'PULLEDOUT'     => ($pulled == $v['total'] ? true : false)
                    ];
                    $return['hasil'][$value['KD']]['stats']      = [
                        'NULU'          => $v['tota'],
                        'BRANKU'        => $v['totb'],
                        'FETO'          => $v['vtfeto'],
                        'MANE'          => $v['vtmane'],
                        'REKLAMA'       => $v['totc'],
                        'REZEITADU'     => $v['totd'],
                        'VALIDU'        => $v['tote'],
                        'TELEITOR'      => $v['ta'],
                        'TVOTUS'        => $v['tb'],
                        'PERSETOT'      => $v['tc'],
                        'TAKTA'         => $v['takta'],
                        'TAKTAIN'       => $v['takin'],
                        'TAKTAVE'       => $v['takve'],
                        'TAKTAST'       => $v['takst'],
                        'PVALIDU'       => $v['totpe'],
                        'PBRANKU'       => $v['totpb'],
                        'PNULU'         => $v['totpa'],
                        'PREKLAMA'      => $v['totpc'],
                        'ABANDONADO'    => $v['tab'],
                        'PABANDO'       => ($v['tab'] != 0 ? (($v['tab'] / $v['tb']) * 100) : 0),
                        'PREZEI'        => $v['totpd'],
                        'PFETO'         => ($v['vtfeto'] != 0 ? (($v['vtfeto'] / $v['tb']) * 100) : 0),
                        'PMANE'         => ($v['vtfeto'] != 0 ? (($v['vtmane'] / $v['tb']) * 100) : 0),
                        'PPARTISIPAS'   => ($v['tb'] / $v['ta']) * 100,
                        'LAVOTA'        => $v['ta'] - $v['tb'],
                        'PLAVOTA'       => (($v['ta'] - $v['tb']) / $v['ta']) * 100,
                        'KODE_DISTRIK'  => $v['nama_distrik'] . '-' . $v['kode_distrik']
                    ];

                    // BERDASARKAN PARTAI
                    $return['partai'][$k][] = [
                        'NAMA'          => $v['nama_distrik'],
                        'TOTAL'         => $v['total'],
                        'COLOR'         => self::$color[$k]
                    ];
                endforeach;
            endforeach;
            $return['tnasional']    = $eleitornas;
            return $return;
        }

    }
