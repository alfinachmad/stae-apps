<?php

    /*
    |--------------------------------------------------------------------------
    | LibraryDataExternal.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : LibraryDataExternal.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Support\Library;

    use App\Http\Controllers\STAEController;

    class LibraryDataExternal extends STAEController {

        private $url    = 'http://stae-tabulasi.info/',
                $urlAPI = 'api/informasi/',
                $key    = '718MTIzNDU2121';

        public static function AmbilDataExternal($tipe){
            if(!empty($tipe)):
                $fgc    = file_get_contents($this->url . $this->urlAPI . $this->key . '/' . $tipe);
                dd($fgc);

            endif;
        }

    }