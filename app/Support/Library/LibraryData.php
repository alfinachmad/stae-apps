<?php

    /*
    |--------------------------------------------------------------------------
    | LibraryData.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : LibraryData.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Support\Library;

    use App\Http\Controllers\STAEController,
        Illuminate\Support\Facades\DB;

    class LibraryData extends STAEController {

        public static function AmbilDataBarang(){
            $SQL    = parent::ociConnection(1)->table('STAE_ASET.tbl_barang')
                ->join('STAE_ASET.tbl_kategori','STAE_ASET.tbl_barang.kode_kategori','=','STAE_ASET.tbl_kategori.kode_kategori')
                ->select('tbl_barang.kode_barang AS IDB','tbl_barang.kode_barang AS KB','tbl_barang.kode_kategori AS KKAT',
                    'tbl_barang.nama_barang AS NB','tbl_barang.no_seri_barang AS NSB','tbl_barang.satuan AS UNIT',
                    'tbl_barang.harga AS HRG','tbl_kategori.nama_kategori AS NKAT','tbl_kategori.keterangan_kategori AS KTKAT')
                ->where('tbl_barang.status',1);

            return $SQL;
        }

        public static function AmbilDataSeriBarangBaru(){
            $SQL    = DB::select(DB::raw('SELECT * FROM VBarangSeriBaru'));
            return $SQL;
        }

        public static function AmbilNoBukti($type='IN',$time){
            $SQLCommand = '';
            if($type == 'IN'):
                $SQLCommand .= "MASUK({$time})";
            elseif($type == 'OUT'):
                $SQLCommand .= "KELUAR({$time})";
            elseif($type == 'MUT'):
                $SQLCommand .= "MUTASI({$time})";
            elseif($type == 'TEST'):
                $SQLCommand .= "MASUK1({$time})";
            endif;
            $FullCommand    = 'SELECT STAE_ASET.PBUKTI.'.$SQLCommand.' NOBUKTI FROM dual';

            $SQL    = parent::ociConnection(1)->select(DB::raw($FullCommand));
            return $SQL;
        }

        public static function AmbilDataSeriBarang($status='NEW'){
            $SQL    = parent::ociConnection(1)->table('STAE_ASET.tbl_barang_seri')
                ->join('STAE_ASET.tbl_barang','STAE_ASET.tbl_barang_seri.kode_barang','=','STAE_ASET.tbl_barang.kode_barang')
                ->select('tbl_barang_seri.*','tbl_barang.*','tbl_barang_seri.status AS statusprint');

            if($status == 'NEW'):
                $SQL->where('tbl_barang_seri.status','=','4')
                    ->orWhere('tbl_barang_seri.status','=','3');
            elseif($status == 'PRINT'):
                $SQL->where('tbl_barang_seri.status','=','3');
            endif;

            return $SQL;
        }

        public static function AmbilDataBarangSiapKeluar($d,$q){
            $SQL    = parent::ociConnection(1)->table('STAE_ASET.tbl_barang')
                ->join('STAE_ASET.tbl_barang_seri','STAE_ASET.tbl_barang.kode_barang','=','STAE_ASET.tbl_barang_seri.kode_barang')
                ->select('tbl_barang_seri.kode_departemen AS KD','tbl_barang.kode_barang AS KB','tbl_barang.nama_barang AS NB','tbl_barang.satuan AS ST','tbl_barang.harga AS HRG','tbl_barang_seri.no_seri_aset AS NSA')
                ->where('tbl_barang_seri.status','=','2')
                ->where('tbl_barang_seri.kode_departemen','=',$d)
                ->where('tbl_barang.nama_barang','LIKE',"%{$q}%");
            return $SQL;
        }

    }