<?php

    /*
    |--------------------------------------------------------------------------
    | CheckAuthenticationTab.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : CheckAuthenticationTab.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Middleware;

    use Illuminate\Support\Facades\Session,
        Closure;

    class CheckAuthenticationTab {

        public function handle($request, Closure $next){
            if(Session::has(SESSION_TABULASI)):
                return $next($request);
            else:
                return redirect()->route('stae-sso', ['modul' => 'masuk'])->with('status', $request->url());
            endif;
        }

    }