<?php

    /*
    |--------------------------------------------------------------------------
    | CheckAuthenticationAPI.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : CheckAuthenticationAPI.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Middleware;

    use Closure;

    class CheckAuthenticationAPI {

        public function handle($request, Closure $next){
            $key    = $request->route('key');
            if($key == '718MTIzNDU2121'):
                return $next($request);
            else:
                echo 'NOT ALLOWED';
                die();
            endif;
        }

    }