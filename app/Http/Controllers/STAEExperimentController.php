<?php

    /*
    |--------------------------------------------------------------------------
    | STAEExperimentController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : STAEController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers;

    class STAEExperimentController {

        private $url    = 'http://stae-tabulasi.info/',
                $urlAPI = 'api/informasi/',
                $key    = '718MTIzNDU2121';

        public function index(){
            $fgc    = file_get_contents($this->url . $this->urlAPI . $this->key . '/abd');
            return response()->json($fgc);
        }

    }