<?php

    /*
    |--------------------------------------------------------------------------
    | APIDepartemenControler.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : APIDepartemenControler.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\API;

    use App\Http\Controllers\STAEController,
        App\Support\Model\ORMDepartemen;

    class APIDepartemenController extends STAEController {

        public function aktifsemua($id=''){
            $data   = ORMDepartemen::where('status',1);
            if($id != '' OR !empty($id)):
                $data->where('kode_departemen', $id);
            endif;
            $data->select('kode_departemen AS iddept','kode_departemen AS kdept','nama_departemen AS ndept','nama_ruangan AS nruangan','lantai AS lt','gedung AS gdg');
            return $data->get();
        }

    }