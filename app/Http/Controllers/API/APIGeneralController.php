<?php

    /*
    |--------------------------------------------------------------------------
    | APIGeneralController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : APIGeneralController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\API;

    use App\Http\Controllers\STAEController,
        App\Support\Model\ORMKategori,
        App\Support\Model\ORMVendor,
        App\Support\Model\ORMBarang,
        App\Support\Model\ORMDepartemen,
        App\Support\Library\LibraryData,
        Illuminate\Http\Request;

    class APIGeneralController extends STAEController {

        public function AmbilKodeKategori(Request $request){
            $data   = ORMKategori::where('status',1)
                ->where('nama_kategori','LIKE','%'.$request->get('q').'%')
                ->get();
            $return = [];
            foreach($data as $key => $user):
                $return[]   = [
                    'k'     => implode('-', array_map('ucwords', explode('-', $user->nama_kategori))),
                    'v'     => $user->kode_kategori
                ];
            endforeach;
            return response()->json($return);
        }

        public function AmbilKodeVendor(Request $request){
            $data   = ORMVendor::where('status',1)
                ->where('nama_vendor','LIKE','%'.$request->get('q').'%')
                ->get();
            $return = [];
            foreach($data as $key => $user):
                $return[]   = [
                    'k'     => implode('-', array_map('ucwords', explode('-', $user->nama_vendor))),
                    'v'     => $user->kode_vendor
                ];
            endforeach;
            return response()->json($return);
        }

        public function AmbilKodeDepartemen(Request $request){
            $data   = ORMDepartemen::where('status',1)
                ->where('nama_ruangan','LIKE','%'.$request->get('q').'%')
                ->get();
            $return = [];
            foreach($data as $key => $user):
                $return[]   = [
                    'k'     => implode('-', array_map('ucwords', explode('-', $user->nama_ruangan))),
                    'v'     => $user->kode_departemen
                ];
            endforeach;
            return response()->json($return);
        }

        public function AmbilKodeBarang(Request $request){
            $data   = ORMBarang::where('status',1)
                ->where('nama_barang','LIKE','%'.$request->get('q').'%')
                ->get();
            $return = [];
            foreach($data as $key => $user):
                $return[]   = [
                    'nb'    => implode('-', array_map('ucwords', explode('-', $user->nama_barang))),
                    'kb'    => $user->kode_barang,
                    'nsb'   => $user->no_seri_barang,
                    'st'    => $user->satuan,
                    'hrg'   => $user->harga
                ];
            endforeach;
            return response()->json($return);
        }

        public function AmbilLibraryDataBarangSiapKeluar($d,$q=''){
            $data   = LibraryData::AmbilDataBarangSiapKeluar($d,$q);
            $return = [];
            foreach($data->get() as $key => $value):
                $return[]   = [
                    'NB'    => $value->NB,
                    'KB'    => $value->KB,
                    'ST'    => $value->ST,
                    'HRG'   => $value->HRG,
                    'NSA'   => $value->NSA
                ];
            endforeach;
            return response()->json($return);
        }

    }