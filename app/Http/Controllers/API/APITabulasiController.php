<?php

    /*
    |--------------------------------------------------------------------------
    | APITabulasiController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : APITabulasiController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\API;

    use App\Http\Controllers\STAEController,
        Illuminate\Support\Facades\Cache;

    class APITabulasiController extends STAEController {

        private $cache  = 'apitabulasi';
        public function AmbilDataParlemen2017($tipe){
            $val    = '';
            if(Cache::has(parent::defaultCacheName())):
                $val = Cache::get(parent::defaultCacheName());
            endif;

            $return = '';
            $data   = $val['hasil'];
            $party  = $val['partai'];
            $total  = $val['total'];
            if($tipe == 'nasional'):
                $return = $data['ALL']['suara'];
            elseif($tipe == 'all'):
                $return = $total;
            elseif($tipe == 'party'):
                $return = $party;
            else:
                $return = $val;
            endif;

            return response()->json([$return]);
        }

    }