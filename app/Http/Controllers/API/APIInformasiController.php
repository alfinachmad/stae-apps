<?php

    /*
    |--------------------------------------------------------------------------
    | APIInformasiController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : APIInformasiController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\API;

    use App\Http\Controllers\STAEController;
    use App\Support\Library\LibraryOCIData;

    class APIInformasiController extends STAEController {

        public function index($tipe){
            $text   = '';
            if($tipe == 'tab'):
                $text   = $this->getDataParlemen();
            elseif($tipe == 'abd'):
                $text   = $this->getDataPemilih();
            else:
                $text   = 'NOT FOUND';
            endif;
            return $text;
        }

        private function getDataParlemen(){
            return LibraryOCIData::AmbilDataParlemen();
        }

        private function getDataPemilih(){
            return LibraryOCIData::AmbilHasilRegistrasiPemilih();
        }

    }