<?php

    /*
    |--------------------------------------------------------------------------
    | APIKategoriController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : APIKategoriController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\API;

    use App\Http\Controllers\STAEController,
        App\Support\Model\ORMKategori;

    class APIKategoriController extends STAEController {

        public function aktifsemua($id=''){
            $data   = ORMKategori::where('status',1);
            if($id != '' OR !empty($id)):
                $data->where('kode_kategori', $id);
            endif;
            $data->select('kode_kategori AS idkategori','kode_kategori AS kkategori','nama_kategori AS nkategori','keterangan_kategori AS ktkategori');
            return $data->get();
        }

    }