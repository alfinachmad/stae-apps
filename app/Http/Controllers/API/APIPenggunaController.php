<?php

    /*
    |--------------------------------------------------------------------------
    | APIPenggunaController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : APIPenggunaController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\API;

    use App\Http\Controllers\STAEController,
        App\Support\Model\ORMPengguna;

    class APIPenggunaController extends STAEController {

        public function aktifsemua($id=''){
            $data   = ORMPengguna::where('status',1);
            if($id != '' OR !empty($id)):
                $data->where('kode_pengguna', $id);
            endif;
            $data->select('kode_pengguna AS ipengguna','nama_lengkap AS nlengkap','email AS mail','tipe_pengguna as tpengguna','nama_pengguna AS npengguna');
            return $data->get();
        }

    }