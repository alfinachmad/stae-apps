<?php

    /*
    |--------------------------------------------------------------------------
    | APIVideoController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : APIVideoController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\API;

    use App\Http\Controllers\STAEController,
        Illuminate\Support\Facades\File,
        Illuminate\Http\Request;

    class APIVideoController extends STAEController {

        public function index($type){
            $getDir = File::allFiles('/home/i17m/Videos');
            $files  = [];
            foreach($getDir as $file):
                $files[] = (string)$file;
            endforeach;

            header("Content-Type: video/mp4");
            header("Content-Length: ".filesize($files[$type]));
            readfile($files[$type]);
        }

    }