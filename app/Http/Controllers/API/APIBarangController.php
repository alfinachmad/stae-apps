<?php

    /*
    |--------------------------------------------------------------------------
    | APIKategoriController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : APIKategoriController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\API;

    use App\Http\Controllers\STAEController,
        App\Support\Model\ORMBarang;

    class APIBarangController extends STAEController {

        public function aktifsemua($id=''){
            $data   = ORMBarang::where('status',1);
            if($id != '' OR !empty($id)):
                $data->where('kode_barang', $id);
            endif;
            $data->select('kode_barang AS idbarang','kode_barang AS kbarang','no_seri_barang AS nsbarang','nama_barang AS nbarang','satuan AS unit','harga AS price');
            return $data->get();
        }

    }