<?php

    /*
    |--------------------------------------------------------------------------
    | APIVendorController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : APIVendorController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\API;

    use App\Http\Controllers\STAEController,
        App\Support\Model\ORMVendor;

    class APIVendorController extends STAEController {

        public function aktifsemua($id=''){
            $data   = ORMVendor::where('status',1);
            if($id != '' OR !empty($id)):
                $data->where('kode_vendor', $id);
            endif;
            $data->select('kode_vendor AS idvend','kode_vendor AS kvend','nama_vendor AS nvend','email AS mail','alamat AS alamat','website AS web');
            return $data->get();
        }

    }