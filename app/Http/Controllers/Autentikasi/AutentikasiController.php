<?php

    /*
    |--------------------------------------------------------------------------
    | AutentikasiController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : AutentikasiController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\Autentikasi;

    use App\Http\Controllers\STAEController,
        App\Support\Model\ORMPengguna,
        App\Support\Model\ORMPenggunaTabulasi,
        Illuminate\Support\Facades\Session,
        Illuminate\Database\QueryException,
        Illuminate\Http\Request,
        Illuminate\Support\Facades\Crypt,
        Illuminate\Support\Facades\Validator;

    class AutentikasiController extends STAEController {

        private $message    = [
                    'UNEQPASS'  => ['status' => 'failed', 'title' => 'Gagal Login', 'message' => 'Gagal! Kata Kunci tidak sesuai !'],
                    'WRNGPASS'  => ['status' => 'failed', 'title' => 'Gagal Login', 'message' => 'Nama Pengguna atau Kata Kunci Salah ! '],
                    'UNACTVUS'  => ['status' => 'failed', 'title' => 'Gagal Login', 'message' => 'Gagal! Nama Pengguna tidak tersedia !']
                ];

        public function masuk(){
            $data   = ['tahunCopyright' => parent::year()];

            $url            = [
                'aset'      => 'Aset\POST\PostPenggunaController@PostProsesMasuk',
                'tabulasi'  => 'Autentikasi\AutentikasiController@PostPenggunaTabulasiMasuk'
            ];

            $data['url']    = $url['aset'];
            if(\session()->has('status')):
                $session    = explode('/', \session('status'));
                \session()->put('next',$session[3]);
                \session()->put('url',\session('status'));
                $data['url']    = $url[\session('next')];
            else:
                $data['url']    = $url['aset'];
            endif;

            return view('Autentikasi.Masuk', $data);
        }

        public function keluar(){
            Session::flush();
            return redirect()->route('stae-sso', ['modul' => 'masuk']);
        }

        public function PostProsesMasuk(Request $request){
            $reqAll     = $request->all();
            $permission = [
                'username'  => 'required|min:1',
                'password'  => 'required|min:1',
            ];

            $validator  = Validator::make($request->all(), $permission);
            $next       = $request->session()->all();
            if($validator->fails()):
                return redirect()->route('backend_signin');
            else:
                $redirect   = ['url' => '','message' => ''];
                $uname      = ($reqAll['username'] == '*******' ? '9999999' : $reqAll['username']);
                $getUser    = ORMPenggunaTabulasi::where('AKTIV',1)
                    ->where('AKSES_WEB',1)
                    ->where('KODE_USER',$uname)
                    ->select('NAMA AS nm','KODE_USER AS kode','PASSWORD2 AS passwd')
                    ->get();

                try {
                    if($getUser->count() > 0):
                        $getUser    = $getUser[0];
                        if($getUser->passwd == strtoupper(md5($reqAll['password']))):
                            $data               = [];
                            $data['username']   = $getUser->nm;
                            $data['kode']       = $getUser->kode;
                            $request->session()->put(SESSION_TABULASI,
                                Crypt::encrypt(json_encode($data))
                            );
                            $nextUrl                = (!isset($next['url']) ? redirect()->route('tabDefault') : redirect($next['url']));
                            $redirect['url']        = $nextUrl;
                        else:
                            $message                = $this->message['UNEQPASS'];
                            $redirect['url']        = redirect()->route('stae-sso', ['modul' => 'masuk'])->with('NoticeSessionLogin',$message);
                        endif;
                    else:
                        $getUser = $users  = ORMPengguna::where('status','1')
                            ->where('nama_pengguna', $uname)
                            ->orWhere('email', $uname)
                            ->select('kode_pengguna AS ipengguna','nama_lengkap AS nlengkap','email AS mail','tipe_pengguna as tpengguna','nama_pengguna AS npengguna','kata_kunci AS passwd')
                            ->get();

                        $getUser    = $getUser[0];
                        if($getUser->count() > 0):
                            if(Crypt::decrypt($getUser->passwd) == $reqAll['password']):
                                $data           = [];
                                $data['username']    = $getUser->nlengkap;
                                $data['authorize']   = $getUser->tpengguna;
                                $data['email']       = $getUser->mail;
                                $request->session()->put(SESSION_DEFAULT,
                                    Crypt::encrypt(json_encode($data))
                                );
                                $redirect['url']     = redirect()->route('aset-default');
                            else:
                                $message                = $this->message['UNEQPASS'];
                                $redirect['url']        = redirect()->route('stae-sso', ['modul' => 'masuk'])->with('NoticeSessionLogin',$message);
                            endif;
                        else:
                            $message            = $this->message['UNACTVUS'];
                            $redirect['url']    = redirect()->route('stae-sso', ['modul' => 'masuk'])->with('NoticeSessionLogin',$message);
                        endif;
                    endif;
                }catch (QueryException $e){
                    $message            = $this->message['WRNGPASS'] . $e->getMessage() .'-'. $e->getCode();
                    $redirect['url']    = redirect()->route('stae-sso', ['modul' => 'masuk'])->with('NoticeSessionLogin',$message);
                }
                return $redirect['url'];
            endif;
        }

    }