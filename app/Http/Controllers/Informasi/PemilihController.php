<?php

    /*
    |--------------------------------------------------------------------------
    | PemilihController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : PemilihController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\Informasi;

    use App\Http\Controllers\STAEController,
        App\Support\Library\LibraryOCIData,
        Carbon\Carbon,
        Illuminate\Support\Facades\Cache,
        Illuminate\Support\Facades\Storage;

    class PemilihController extends STAEController {

        public function dalamnegeri(){
            $data       = [];
            $data_reg   = $this->rekapitulasipemilih();
            $data['title_page']     = 'Register National Information';
            $data['subtitle']       = self::title('INFO');
            $data['subMainTitle']   = 'NACIONAL';
            $data['mcrtime']    = parent::microtime();
            $data['periode']    = $this->periode();
            $data['regNew']     = $data_reg['DN'];
            $data['refreshSec'] = 60;
            $data['refreshNxt'] = 'registrasi-luar-negeri';
            $data['lastupd']    = $data_reg['lastupd'];
            return view($this->defaultNonAset(), $data);
        }

        public function luarnegeri(){
            $data       = [];
            $data_reg   = $this->rekapitulasipemilih();
            $data['title_page'] = 'Register National Information';
            $data['subtitle']       = self::title('INFO');
            $data['subMainTitle']   = 'ESTRANJEIRU';
            $data['mcrtime']    = parent::microtime();
            $data['periode']    = $this->periode();
            $data['regNew']     = $data_reg['LN'];
            $data['refreshSec'] = 40;
            $data['refreshNxt'] = 'total-registrasi';
            $data['lastupd']    = $data_reg['lastupd'];
            #dd($data_reg);
            return view($this->defaultNonAset(), $data);
        }

        public function keseluruhan(){
            $data       = [];
            $data_reg   = $this->rekapitulasipemilih();
            $data['title_page'] = 'Register National Information';
            $data['subtitle']       = self::title('INFO');
            $data['subMainTitle']   = 'ALL';
            $data['mcrtime']    = parent::microtime();
            $data['periode']    = $this->periode();
            $data['regNew']     = $data_reg;
            $data['refreshSec'] = 60;
            $data['refreshNxt'] = 'registrasi-dalam-negeri';
            $data['lastupd']    = $data_reg['lastupd'];
            #dd($data_reg);
            return view($this->defaultNonAset(), $data);
        }

        private function periode(){
            $data   = LibraryOCIData::AmbilDataPeriodeRegistrasiPemilih();
            $decode = json_decode($data, true);
            $awal   = Carbon::parse($decode[0]['awal'])->format('d F');
            $akhir  = Carbon::parse($decode[0]['akhir'])->format('d F');
            $return = $awal . ' TO\'O '. $akhir;
            return strtoupper($return);
        }

        private function rekapitulasipemilih(){
            if(Cache::has('STAEABD2017')):
                $cache  = Cache::get('STAEABD2017');
                $fileJ  = Storage::get('STAE');
                return $cache;
            endif;
        }

    }