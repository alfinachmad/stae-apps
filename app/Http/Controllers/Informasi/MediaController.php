<?php

    /*
    |--------------------------------------------------------------------------
    | MediaController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : MediaController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\Informasi;

    use App\Http\Controllers\STAEController,
        Carbon\Carbon,
        Illuminate\Support\Facades\File;

    class MediaController extends STAEController {

        public function index(){
            $getDir = File::allFiles('/home/i17m/Videos');
            $files  = [];
            foreach($getDir as $file):
                $files[] = (string)$file;
            endforeach;

            $data   = [
                'title_page'    => 'Media Informasi',
                'subtitle'      => self::title('MEDIA'),
                'dtfile'        => $files
            ];
            return view($this->defaultNonAset(),$data);
        }

        public function countingDown(){
            $year   = '2017';
            $date   = parent::defaultTimeZone();
            $now    = Carbon::now(new \DateTimeZone('Asia/Dili'));
            $future = Carbon::create(2017,7,22, 19,0,0, 'Asia/Dili');
            $second = $future->diffInSeconds($now);
            $data   = [
                'title_page'    => 'Counting Down Pemilu Parlemen Tahun 2017',
                'subtitle'      => self::title('PR') . $year,
                'dttime'        => $second
            ];
            return view($this->defaultNonAset(), $data);
        }

        public function _clockinfo(){
            header('Content-Type: text/event-stream');
            header('Cache-Control: no-cache');

            $time = parent::defaultTimeZone();
            echo "data: {$time}\n\n";
            flush();
        }

    }