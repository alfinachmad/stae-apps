<?php

    /*
    |--------------------------------------------------------------------------
    | STAEController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : STAEController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers;

    use App\Support\Library\LibraryOCIData,
        Carbon\Carbon,
        Illuminate\Support\Facades\Cache,Illuminate\Support\Facades\Storage,
        Illuminate\Support\Facades\Route;
    use Illuminate\Database\QueryException;

    class STAEController extends Controller {

        private static  $dbprofile  = ['oracleTab','oracle','oraclePDidik'],
                        $subtitle   = 'REZULTADU PROVIZÓRIU APURAMENTU MUNISIPAL / RAEOA / DIASPORA ',
                        $mainServer = 'http://stae-tabulasi.info/';

        public function __construct(){
            $tabulasi   = Cache::remember(self::defaultCacheName(), self::cacheExpire(), function(){
                $data   = LibraryOCIData::AmbilDataParlemen();
                Storage::put('STAE',json_encode($data));
                return $data;
            });
            $regpemilih = Cache::remember('STAEABD2017', 1, function(){
                $data   = LibraryOCIData::AmbilHasilRegistrasiPemilih();
                Storage::put('STAEABD',json_encode($data));
                return $data;
            });
        }

        public function beranda(){
            $year   = self::year();
            $data   = [
                'title_page'    => 'BERANDA',
                'subtitle'      => self::title('PR') . $year,
            ];
            return view('Beranda', $data);
        }

        protected static function routeList(){
            $listRoutes = Route::getRoutes();
            $return     = [];
            $apiURL     = '';
            foreach($listRoutes as $value):
                $listRoute[]    = $value->getPath();
                $routeRegex     = preg_grep("/(^tabulasi)/", $listRoute);
                $apiURL         = preg_grep("/(^api\/tabulasi)/", $listRoute);
                $apiURL         = array_values($apiURL);
                $endRoute       = [];
                foreach($routeRegex as $k => $v):
                    $explRoute  = explode('/', $routeRegex[$k]);
                    $endRoute[] = end($explRoute);
                endforeach;
            endforeach;
            $implode            = implode(',', array_unique($endRoute));
            $return['route']    = $implode;
            $return['apiroute'] = str_replace('{tipe?}/hasil','',$apiURL[0]);
            return $return;
        }

        protected static function defaultSubTitle(){
            return self::$subtitle;
        }

        protected static function defaultAset(){
            return 'Aset.asetIndex';
        }

        protected static function defaultNonAset(){
            return 'STAEIndex';
        }

        protected static function defaultTimeZone(){
            return Carbon::now(new \DateTimeZone('Asia/Dili'));
        }

        protected static function defaultCacheName(){
            return 'hasiltabulasi2017';
        }

        protected static function year(){
            return self::defaultTimeZone()->format('Y');
        }

        protected static function browser(){
            return $_SERVER['HTTP_USER_AGENT'];
        }

        protected static function idUsers(){
            return [1 => 'Administrator', 2 => 'Viewer', 3 => 'Operator', 9 => 'Super Administrator'];
        }

        protected static function microtime(){
            return time() * 1000;
        }

        protected static function title($title){
            if($title == 'P'):
                return 'ELEISAUN PREZIDENSIAL ';
            elseif($title == 'PR'):
                return 'ELEISAUN PARLAMENTAR ';
            elseif($title == 'INFO'):
                return 'INFORMASAUN JERAL';
            elseif($title == 'MEDIA'):
                return 'MEDIA INFORMASAUN';
            endif;
        }

        protected static function ociConnection($type){
            $name   = self::$dbprofile[$type];
            try {
                \DB::connection($name)->getPdo();
                $db = \DB::connection($name);
                if($db->getDatabaseName()):
                    return $db;
                endif;
            }catch (\Exception $e){
                die('Database is not Connected');
            }
        }

        protected static function cacheExpire(){
            return getenv('CACHE_EXPIRE');
        }

    }

