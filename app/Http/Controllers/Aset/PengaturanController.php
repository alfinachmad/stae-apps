<?php

    /*
    |--------------------------------------------------------------------------
    | PengaturanController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : PengaturanController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\Aset;

    use App\Http\Controllers\STAEController;
    use App\Support\Model\ORMPengguna;

    class PengaturanController extends STAEController {

        public function pengguna(){
            $data   = [
                'titleDoc'          => 'Pengaturan Pengguna',
                'tahunCopyright'    => parent::year(),
                'data'              => ORMPengguna::where('STATUS',1)->paginate(5),
                'level'             => parent::idUsers(),
                'numrows'           => ORMPengguna::count()
            ];
            #dd(ORMPengguna::where('status','1')->toSql());
            return view($this->defaultAset(), $data);
        }

        public function bahasa(){
            $data   = [
                'titleDoc'          => 'Pengaturan Bahasa',
                'tahunCopyright'    => parent::year()
            ];
            return view($this->defaultAset(), $data);
        }

        public function sqlconsole(){
            $data   = [
                'titleDoc'          => 'SQL Console Live',
                'tahunCopyright'    => parent::year()
            ];
            return view($this->defaultAset(), $data);
        }

        public function halaman(){
            $data   = [
                'titleDoc'          => 'Pengaturan Halaman',
                'tahunCopyright'    => parent::year()
            ];
            return view($this->defaultAset(), $data);
        }

    }