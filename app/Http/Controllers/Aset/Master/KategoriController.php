<?php

    /*
    |--------------------------------------------------------------------------
    | KategoriController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : KategoriController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\Aset\Master;

    use App\Http\Controllers\STAEController,
        App\Support\Model\ORMKategori,
        Carbon\Carbon,
        Illuminate\Http\Request,
        Illuminate\Support\Facades\Input,
        Illuminate\Support\Facades\Validator;

    class KategoriController extends STAEController {

        private $attr   = ['name-data' => 'Kategori', 'name-function' => 'kategori', 'name-modul' => 'master', 'name-primary' => 'kode_kategori'];
        public function index(){
            $data   = [
                'titleDoc'          => 'Data ' . $this->attr['name-data'],
                'tahunCopyright'    => Carbon::createFromDate()->format('Y'),
                'data'              => ORMKategori::where('status',1)->paginate(10),
                'numrows'           => ORMKategori::count(),
                'field'             => $this->attr['name-data'],
                'modul'             => $this->attr['name-function']
            ];
            return view($this->defaultAset(), $data);
        }

        public function ProsesTambahKategori(Request $request){
            $var        = [];
            $rules      = [];
            $temp       = [];
            $name       = 'tambah'.$this->attr['name-data'];
            $variable   = Input::get($name);
            $reqAll     = $request->all();
            $browser    = $_SERVER['HTTP_USER_AGENT'];

            $permission = [
                0   => ['kode_kategori', 'required|Between:3,10|unique:oracle.STAE_ASET.tbl_kategori,kode_kategori'],
                1   => ['nama_kategori', 'required|Between:3,200'],
                2   => ['keterangan_kategori', 'required|Between:3,200']
            ];
            $var['_token']  = $reqAll['_token'];

            foreach($variable as $keyVar):
                $temp[] = $keyVar;
            endforeach;
            for($i=0;$i<count($temp);$i++):
                $rules[$permission[$i][0]]  = $permission[$i][1];
                $var[$permission[$i][0]]    = $temp[$i];
            endfor;

            $validator  = Validator::make($var,$rules);
            if($validator->fails()):
                $error  = array_merge($reqAll,$validator->errors()->all());
                $message    = ['status' => 'failed', 'title' => 'Gagal Input', 'message' => 'Anda gagal membuat Data '.$this->attr['name-data'].' baru ! '.$error[0]];
                return redirect()->route('aset-master', ['modul' => $this->attr['name-function']])->with('NoticeSession',$message);
            else:
                #dd('OK');

                try {
                    $ORM = new ORMKategori();
                    $ORM->kode_kategori         = $var['kode_kategori'];
                    $ORM->nama_kategori         = strtolower($var['nama_kategori']);
                    $ORM->keterangan_kategori   = strtolower($var['keterangan_kategori']);
                    $ORM->waktu_buat            = Carbon::now();
                    $ORM->clog                  = $browser . '|' . $request->ip();
                    $ORM->save();
                }catch (QueryException $e){
                    $message    = ['status' => 'failed', 'title' => 'Gagal Input', 'message' => 'Anda gagal membuat Data '.$this->attr['name-data'].' baru ! '.$e->getCode(), $e->getCode()];
                    return redirect()->route('aset-master', ['modul' => $this->attr['name-function']])->with('NoticeSession',$message);
                }
                $message    = ['status' => 'success', 'title' => 'Sukses Input', 'message' => 'Sukses! Anda berhasil membuat Data '.$this->attr['name-data'].' baru !'];
                return redirect()->route('aset-master', ['modul' => $this->attr['name-function']])->with('NoticeSession',$message);
            endif;
        }

        public function ProsesUbahKategori(Request $request){
            $var        = [];
            $rules      = [];
            $temp       = [];
            $name       = 'ubahKategori';
            $variable   = Input::get($name);
            $reqAll     = $request->all();
            $browser    = $_SERVER['HTTP_USER_AGENT'];

            $permission = [
                0   => ['kode_kategori', 'required|Between:3,10|unique:oracle.STAE_ASET.tbl_kategori,kode_kategori,'.$variable[1].',kode_kategori'],
                1   => ['id_kategori', 'required'],
                2   => ['nama_kategori', 'required|Between:3,200'],
                3   => ['keterangan_kategori', 'required|Between:3,200']
            ];

            foreach($variable as $keyVar):
                $temp[] = $keyVar;
            endforeach;
            for($i=0;$i<count($temp);$i++):
                $rules[$permission[$i][0]]  = $permission[$i][1];
                $var[$permission[$i][0]]    = $temp[$i];
            endfor;

            $validator  = Validator::make($var,$rules);
            if($validator->fails()):
                $error  = array_merge($reqAll,$validator->errors()->all());
                $message    = ['status' => 'failed', 'title' => 'Gagal Ubah', 'message' => 'Anda gagal memperbarui Data '.$this->attr['name-data'].' ! '.$error[0]];
                return redirect()->route('aset-master', ['modul' => $this->attr['name-function']])->with('NoticeSession',$message);
            else:
                try {
                    $updates        = [
                        'nama_kategori'         => strtolower($var['nama_kategori']),
                        'kode_kategori'         => strtoupper($var['kode_kategori']),
                        'keterangan_kategori'   => strtolower($var['keterangan_kategori']),
                        'waktu_ubah'            => Carbon::now(),
                        'clog'                  => $browser . '|' . $request->ip()
                    ];
                    ORMKategori::where($this->attr['name-primary'],$variable[1])->update($updates);
                }catch (QueryException $e){
                    $message    = ['status' => 'failed', 'title' => 'Gagal Memperbarui', 'message' => 'Anda gagal memperbarui Data '.$this->attr['name-data'].' ! '.$e->getCode(), $e->getCode()];
                    return redirect()->route('aset-master', ['modul' => $this->attr['name-function']])->with('NoticeSession',$message);
                }
                $message    = ['status' => 'success', 'title' => 'Sukses Memperbarui', 'message' => 'Sukses! Anda berhasil memperbarui Data '.$this->attr['name-data'].' !'];
                return redirect()->route('aset-master', ['modul' => $this->attr['name-function']])->with('NoticeSession',$message);
            endif;
        }

        public function ProsesHapusKategori(Request $request){
            $reqAll     = $request->all();
            $id         = $reqAll['id'];
            try {
                ORMKategori::where($this->attr['name-primary'],$id)->update(['status' => '0', 'waktu_ubah' => Carbon::now()]);
            }catch (QueryException $e){
                $message    = ['status' => 'failed', 'title' => 'Gagal Hapus', 'message' => 'Gagal menghapus Data '.$this->attr['name-data'].' ! '.$e->getMessage(), $e->getCode()];
                return redirect()->route('aset-master', ['modul' => $this->attr['name-function']])->with('NoticeSession',$message);
            }
            $message    = ['status' => 'success', 'title' => 'Sukses Hapus', 'message' => 'Sukses! Menghapus Data '.$this->attr['name-data'].' !'];
            return redirect()->route('aset-master', ['modul' => $this->attr['name-function']])->with('NoticeSession',$message);
        }

    }