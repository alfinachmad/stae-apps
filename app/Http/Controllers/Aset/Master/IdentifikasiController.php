<?php

    /*
    |--------------------------------------------------------------------------
    | IdentifikasiController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : IdentifikasiController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\Aset\Master;

    use App\Http\Controllers\STAEController,
        App\Support\Model\ORMBarangSeri,
        Carbon\Carbon;
    use App\Support\Library\LibraryData;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use Illuminate\Support\Facades\Input;

    class IdentifikasiController extends STAEController {

        private $attr   = ['name-data' => 'Identifikasi', 'name-function' => 'identifikasi', 'name-modul' => 'master', 'name-primary' => 'kode_barang'];
        public function index(){
            $SQL        = LibraryData::AmbilDataSeriBarang();
            $data   = [
                'titleDoc'          => 'Data ' . $this->attr['name-data'],
                'tahunCopyright'    => Carbon::createFromDate()->format('Y'),
                'data'              => $SQL->paginate(10),
                'numrows'           => count($SQL),
                'field'             => $this->attr['name-data'],
                'modul'             => $this->attr['name-function']
            ];
            return view($this->defaultAset(), $data);
        }

    }