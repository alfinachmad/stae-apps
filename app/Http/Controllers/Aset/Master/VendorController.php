<?php

    /*
    |--------------------------------------------------------------------------
    | VendorController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : VendorController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\Aset\Master;

    use App\Http\Controllers\STAEController,
        Carbon\Carbon,
        App\Support\Model\ORMVendor,
        Illuminate\Http\Request,
        Illuminate\Support\Facades\Input,
        Illuminate\Support\Facades\Validator;

    class VendorController extends STAEController {

        private $attr   = ['name-data' => 'Vendor', 'name-function' => 'vendor', 'name-modul' => 'master', 'name-primary' => 'kode_vendor'];
        public function index(){
            $data   = [
                'titleDoc'          => 'Data Vendor',
                'tahunCopyright'    => Carbon::createFromDate()->format('Y'),
                'data'              => ORMVendor::where('status',1)->paginate(10),
                'numrows'           => ORMVendor::count(),
                'field'             => $this->attr['name-data'],
                'modul'             => $this->attr['name-function']
            ];
            return view($this->defaultAset(), $data);
        }

        public function ProsesTambahVendor(Request $request){
            $var        = [];
            $rules      = [];
            $temp       = [];
            $name       = 'tambahVendor';
            $variable   = Input::get($name);
            $reqAll     = $request->all();
            $browser    = $_SERVER['HTTP_USER_AGENT'];

            $permission = [
                0   => ['kode_vendor', 'required|Between:3,10|unique:oracle.STAE_ASET.tbl_vendor,kode_vendor'],
                1   => ['nama_vendor', 'required|Between:3,200'],
                2   => ['alamat', 'required|Between:3,200'],
                3   => ['email', 'required|Between:3,100'],
                4   => ['website', 'required|Between:3,100']
            ];
            $var['_token']  = $reqAll['_token'];

            foreach($variable as $keyVar):
                $temp[] = $keyVar;
            endforeach;
            for($i=0;$i<count($temp);$i++):
                $rules[$permission[$i][0]]  = $permission[$i][1];
                $var[$permission[$i][0]]    = $temp[$i];
            endfor;

            $validator  = Validator::make($var,$rules);
            if($validator->fails()):
                $error  = array_merge($reqAll,$validator->errors()->all());
                $message    = ['status' => 'failed', 'title' => 'Gagal Input', 'message' => 'Anda gagal membuat Data Vendor baru ! '.$error[0]];
                return redirect()->route('aset-master', ['modul' => 'vendor'])->with('NoticeSession',$message);
            else:
                #dd('OK');

                try {
                    $ORM = new ORMVendor();
                    $ORM->kode_vendor       = $var['kode_vendor'];
                    $ORM->nama_vendor       = strtolower($var['nama_vendor']);
                    $ORM->alamat            = strtolower($var['alamat']);
                    $ORM->email             = strtolower($var['email']);
                    $ORM->website           = strtolower($var['website']);
                    $ORM->waktu_buat        = Carbon::now();
                    $ORM->clog              = $browser . '|' . $request->ip();
                    $ORM->save();
                }catch (QueryException $e){
                    $message    = ['status' => 'failed', 'title' => 'Gagal Input', 'message' => 'Anda gagal membuat Data Vendor baru ! '.$e->getCode(), $e->getCode()];
                    return redirect()->route('aset-master', ['modul' => 'vendor'])->with('NoticeSession',$message);
                }
                $message    = ['status' => 'success', 'title' => 'Sukses Input', 'message' => 'Sukses! Anda berhasil membuat Data Vendor baru !'];
                return redirect()->route('aset-master', ['modul' => 'vendor'])->with('NoticeSession',$message);
            endif;
        }

        public function ProsesUbahVendor(Request $request){
            $var        = [];
            $rules      = [];
            $temp       = [];
            $name       = 'ubahVendor';
            $variable   = Input::get($name);
            $reqAll     = $request->all();
            $browser    = $_SERVER['HTTP_USER_AGENT'];

            $permission = [
                0   => ['kode_vendor', 'required|Between:3,10|unique:oracle.STAE_ASET.tbl_vendor,kode_vendor,'.$variable[1].',kode_vendor'],
                1   => ['id_vendor','required'],
                2   => ['nama_vendor', 'required|Between:3,200'],
                3   => ['alamat', 'required|Between:3,200'],
                4   => ['email', 'required|Between:3,100'],
                5   => ['website', 'required|Between:3,100']
            ];

            foreach($variable as $keyVar):
                $temp[] = $keyVar;
            endforeach;
            for($i=0;$i<count($temp);$i++):
                $rules[$permission[$i][0]]  = $permission[$i][1];
                $var[$permission[$i][0]]    = $temp[$i];
            endfor;

            $validator  = Validator::make($var,$rules);
            if($validator->fails()):
                $error  = array_merge($reqAll,$validator->errors()->all());
                $message    = ['status' => 'failed', 'title' => 'Gagal Ubah', 'message' => 'Anda gagal memperbarui Data Departemen ! '.$error[0]];
                return redirect()->route('aset-master', ['modul' => 'vendor'])->with('NoticeSession',$message);
            else:
                try {
                    $updates        = [
                        'nama_vendor'       => strtolower($var['nama_vendor']),
                        'kode_vendor'       => strtoupper($var['kode_vendor']),
                        'alamat'            => strtolower($var['alamat']),
                        'email'             => strtolower($var['email']),
                        'website'           => strtolower($var['website']),
                        'waktu_ubah'        => Carbon::now(),
                        'clog'              => $browser . '|' . $request->ip()
                    ];
                    ORMVendor::where('kode_vendor',$variable[1])->update($updates);
                }catch (QueryException $e){
                    $message    = ['status' => 'failed', 'title' => 'Gagal Memperbarui', 'message' => 'Anda gagal memperbarui Data Vendor ! '.$e->getCode(), $e->getCode()];
                    return redirect()->route('aset-master', ['modul' => 'vendor'])->with('NoticeSession',$message);
                }
                $message    = ['status' => 'success', 'title' => 'Sukses Memperbarui', 'message' => 'Sukses! Anda berhasil memperbarui Data Vendor !'];
                return redirect()->route('aset-master', ['modul' => 'vendor'])->with('NoticeSession',$message);
            endif;
        }

        public function ProsesHapusVendor(Request $request){
            $reqAll     = $request->all();
            $id         = $reqAll['id'];
            try {
                ORMVendor::where('kode_vendor',$id)->update(['status' => '0', 'waktu_ubah' => Carbon::now()]);
            }catch (QueryException $e){
                $message    = ['status' => 'failed', 'title' => 'Gagal Hapus', 'message' => 'Gagal menghapus Data Vendor ! '.$e->getMessage(), $e->getCode()];
                return redirect()->route('aset-master', ['modul' => 'vendor'])->with('NoticeSession',$message);
            }
            $message    = ['status' => 'success', 'title' => 'Sukses Hapus', 'message' => 'Sukses! Menghapus Data Vendor!'];
            return redirect()->route('aset-master', ['modul' => 'vendor'])->with('NoticeSession',$message);
        }

    }