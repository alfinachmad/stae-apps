<?php

    /*
    |--------------------------------------------------------------------------
    | BarangController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : BarangController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\Aset\Master;

    use App\Http\Controllers\STAEController,
        App\Support\Model\ORMBarang,
        App\Support\Library\LibraryData,
        App\Support\Model\ORMBarangSeri,
        Carbon\Carbon,
        Illuminate\Http\Request,
        Illuminate\Support\Facades\Validator,
        Illuminate\Support\Facades\Input,
        Illuminate\Database\QueryException;

    class BarangController extends STAEController {

        private $attr   = ['name-data' => 'Barang', 'name-function' => 'barang', 'name-modul' => 'master', 'name-primary' => 'kode_barang'];
        public function index(){
            $SQL    = LibraryData::AmbilDataBarang(5);
            $data   = [
                'titleDoc'          => 'Data ' . $this->attr['name-data'],
                'tahunCopyright'    => Carbon::createFromDate()->format('Y'),
                'data'              => $SQL->paginate(5),
                'numrows'           => count($SQL),
                'field'             => $this->attr['name-data'],
                'modul'             => $this->attr['name-function']
            ];
            return view($this->defaultAset(), $data);
        }

        public function ProsesIsiNoSeri(Request $request){
            $browser    = parent::browser();
            $varText    = Input::get('tambahNoSeri');
            $varFile    = Input::file('media-gambar');
            $countFile  = count($varFile);

            $validText  = Validator::make($varText, [
                $varText[0] => ['no_seri_aset', 'required|Between:3,50'],
                $varText[1] => ['no_seri_rdtl', 'required|Between:3,50'],
                $varText[2] => ['no_seri_alat', 'required|Between:3,50'],
            ]);

            $validFile  = Validator::make(['file' => $varFile[0]], [
                'file'  => 'required|image|mimes:jpeg,png,jpg|max:2048'
            ]);

            if($validText->fails() AND $validFile->fails()):
                $error  = array_merge($reqAll,$validator->errors()->all());
                $message    = ['status' => 'failed', 'title' => 'Gagal Input', 'message' => 'Anda gagal mengisi Nomor Seri ! '.$error[0]];
                return redirect()->route('aset-master', ['modul' => 'identifikasi'])->with('NoticeSession',$message);
            else:
                try{
                    $filename   = strtoupper($varText[0]) . '.' . $varFile[0]->getClientOriginalExtension();
                    $updates    = [
                        'no_seri_rdtl'  => strtoupper($varText[1]),
                        'no_seri_alat'  => strtoupper($varText[2]),
                        'gambar'        => $filename,
                        'status'        => '3'
                    ];
                    ORMBarangSeri::where('no_seri_aset', $varText[0])->update($updates);
                    $varFile[0]->move(public_path('upload/seri'), $filename);

                } catch (QueryException $e){
                    $message    = ['status' => 'failed', 'title' => 'Gagal Memperbarui', 'message' => 'Anda gagal mengisi Data Nomor Seri ! '.$e->getMessage(), $e->getCode()];
                    return redirect()->route('aset-master', ['modul' => 'identifikasi'])->with('NoticeSession',$message);
                }
                $message    = ['status' => 'success', 'title' => 'Sukses Memperbarui', 'message' => 'Sukses! Anda berhasil mengisi No. Seri !'];
                return redirect()->route('aset-master', ['modul' => 'identifikasi'])->with('NoticeSession',$message);
            endif;
        }

        public function ProsesTambahBarang(Request $request){
            $var        = [];
            $rules      = [];
            $temp       = [];
            $name       = 'tambah'.$this->attr['name-data'];
            $variable   = Input::get($name);
            $reqAll     = $request->all();
            $browser    = $_SERVER['HTTP_USER_AGENT'];

            $permission = [
                0   => ['kode_barang', 'required|Between:3,10|unique:oracle.STAE_ASET.tbl_barang,kode_barang'],
                1   => ['nama_barang', 'required|Between:3,200'],
                2   => ['nama_kategori', 'Between:3,200'],
                3   => ['kode_kategori', 'required|Between:3,10'],
                4   => ['no_seri_barang', 'required|Between:1,100'],
                5   => ['satuan', 'required|Between:1,10'],
                6   => ['harga', 'required|numeric|Between:1,99999999']
            ];
            $var['_token']  = $reqAll['_token'];

            foreach($variable as $keyVar):
                $temp[] = $keyVar;
            endforeach;
            for($i=0;$i<count($temp);$i++):
                $rules[$permission[$i][0]]  = $permission[$i][1];
                $var[$permission[$i][0]]    = $temp[$i];
            endfor;

            $validator  = Validator::make($var,$rules);
            #dd($var);
            if($validator->fails()):
                $error  = array_merge($reqAll,$validator->errors()->all());
                dd($error);
                $message    = ['status' => 'failed', 'title' => 'Gagal Input', 'message' => 'Anda gagal membuat Data '.$this->attr['name-data'].' baru ! '.$error[0]];
                return redirect()->route('aset-master', ['modul' => $this->attr['name-function']])->withInput($variable)->with('NoticeSession',$message);
            else:
                #dd('OK');

                try {
                    $ORM = new ORMBarang();
                    $ORM->kode_barang           = $var['kode_barang'];
                    $ORM->kode_kategori         = strtoupper($var['kode_kategori']);
                    $ORM->no_seri_barang        = $var['no_seri_barang'];
                    $ORM->nama_barang           = strtolower($var['nama_barang']);
                    $ORM->satuan                = strtolower($var['satuan']);
                    $ORM->harga                 = strtolower($var['harga']);
                    $ORM->waktu_buat            = Carbon::now();
                    $ORM->clog                  = $browser . '|' . $request->ip();
                    $ORM->save();
                }catch (QueryException $e){
                    $message    = ['status' => 'failed', 'title' => 'Gagal Input', 'message' => 'Anda gagal membuat Data '.$this->attr['name-data'].' baru ! '.$e->getCode(), $e->getCode()];
                    return redirect()->route('aset-master', ['modul' => $this->attr['name-function']])->with('NoticeSession',$message);
                }
                $message    = ['status' => 'success', 'title' => 'Sukses Input', 'message' => 'Sukses! Anda berhasil membuat Data '.$this->attr['name-data'].' baru !'];
                return redirect()->route('aset-master', ['modul' => $this->attr['name-function']])->with('NoticeSession',$message);
            endif;
        }

        public function ProsesUbahBarang(Request $request){
            $var        = [];
            $rules      = [];
            $temp       = [];
            $name       = 'ubah'.$this->attr['name-data'];
            $variable   = Input::get($name);
            $reqAll     = $request->all();
            $browser    = $_SERVER['HTTP_USER_AGENT'];

            $permission = [

                0   => ['id_barang', 'required'],
                1   => ['nama_barang', 'required|Between:3,200'],
                2   => ['nama_kategori', 'Between:3,200'],
                3   => ['kode_kategori', 'required|Between:3,10'],
                4   => ['no_seri_barang', 'required|Between:3,100'],
                5   => ['satuan', 'required|Between:3,10'],
                6   => ['harga', 'required|numeric']
            ];

            foreach($variable as $keyVar):
                $temp[] = $keyVar;
            endforeach;
            for($i=0;$i<count($temp);$i++):
                $rules[$permission[$i][0]]  = $permission[$i][1];
                $var[$permission[$i][0]]    = $temp[$i];
            endfor;

            $validator  = Validator::make($var,$rules);
            $errorInput = array_merge([$validator->fails(),$variable],$validator->errors()->all());
            #dd($errorInput);
            if($validator->fails()):
                $error  = array_merge($reqAll,$validator->errors()->all());
                $message    = ['status' => 'failed', 'title' => 'Gagal Ubah', 'message' => 'Anda gagal memperbarui Data '.$this->attr['name-data'].' ! '.$error[0]];
                return redirect()->route('aset-master', ['modul' => $this->attr['name-function']])->with('NoticeSession',$message);
            else:
                try {
                    $updates        = [
                        'nama_barang'           => strtolower($var['nama_barang']),
                        'kode_kategori'         => strtoupper($var['kode_kategori']),
                        'no_seri_barang'        => $var['no_seri_barang'],
                        'satuan'                => strtolower($var['satuan']),
                        'harga'                 => strtolower($var['harga']),
                        'waktu_ubah'            => Carbon::now(),
                        'clog'                  => $browser . '|' . $request->ip()
                    ];
                    ORMBarang::where($this->attr['name-primary'],$variable[0])->update($updates);
                }catch (QueryException $e){
                    $message    = ['status' => 'failed', 'title' => 'Gagal Memperbarui', 'message' => 'Anda gagal memperbarui Data '.$this->attr['name-data'].' ! '.$e->getCode(), $e->getCode()];
                    return redirect()->route('aset-master', ['modul' => $this->attr['name-function']])->with('NoticeSession',$message);
                }
                $message    = ['status' => 'success', 'title' => 'Sukses Memperbarui', 'message' => 'Sukses! Anda berhasil memperbarui Data '.$this->attr['name-data'].' !'];
                return redirect()->route('aset-master', ['modul' => $this->attr['name-function']])->with('NoticeSession',$message);
            endif;
        }

        public function ProsesHapusBarang(Request $request){
            $reqAll     = $request->all();
            $id         = $reqAll['id'];
            try {
                ORMBarang::where($this->attr['name-primary'],$id)->update(['status' => '0', 'waktu_ubah' => Carbon::now()]);
            }catch (QueryException $e){
                $message    = ['status' => 'failed', 'title' => 'Gagal Hapus', 'message' => 'Gagal menghapus Data '.$this->attr['name-data'].' ! '.$e->getMessage(), $e->getCode()];
                return redirect()->route('aset-master', ['modul' => $this->attr['name-function']])->with('NoticeSession',$message);
            }
            $message    = ['status' => 'success', 'title' => 'Sukses Hapus', 'message' => 'Sukses! Menghapus Data '.$this->attr['name-data'].' !'];
            return redirect()->route('aset-master', ['modul' => $this->attr['name-function']])->with('NoticeSession',$message);
        }

    }