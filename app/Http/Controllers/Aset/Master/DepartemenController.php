<?php

    /*
    |--------------------------------------------------------------------------
    | DepartemenController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : DepartemenController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\Aset\Master;

    use App\Http\Controllers\STAEController,
        App\Support\Model\ORMDepartemen,
        Illuminate\Http\Request,
        Carbon\Carbon,
        Illuminate\Support\Facades\Input,
        Illuminate\Support\Facades\Validator;

    class DepartemenController extends STAEController {

        private $attr   = ['name-data' => 'Departemen', 'name-function' => 'departemen', 'name-modul' => 'master', 'name-primary' => 'id_departemen'];
        public function index(){
            $data   = [
                'titleDoc'          => 'Data Departemen',
                'tahunCopyright'    => parent::year(),
                'data'              => ORMDepartemen::where('status',1)->paginate(10),
                'numrows'           => ORMDepartemen::count(),
                'field'             => $this->attr['name-data'],
                'modul'             => $this->attr['name-function']
            ];
            return view($this->defaultAset(), $data);
        }

        public function ProsesTambahDepartemen(Request $request){
            $var        = [];
            $rules      = [];
            $temp       = [];
            $name       = 'tambahDepartemen';
            $variable   = Input::get($name);
            $reqAll     = $request->all();
            $browser    = $_SERVER['HTTP_USER_AGENT'];

            #dd($variable);

            $permission = [
                0   => ['kode_departemen', 'unique:oracle.STAE_ASET.tbl_departemen,kode_departemen'],
                1   => ['nama_departemen', 'required|Between:3,200'],
                2   => ['nama_ruangan', 'required|Between:3,200'],
                3   => ['gedung', 'required|Between:3,100']
            ];
            $var['_token']  = $reqAll['_token'];

            foreach($variable as $keyVar):
                $temp[] = $keyVar;
            endforeach;
            for($i=0;$i<count($temp);$i++):
                $rules[$permission[$i][0]]  = $permission[$i][1];
                $var[$permission[$i][0]]    = $temp[$i];
            endfor;

            $validator  = Validator::make($var,$rules);
            if($validator->fails()):
                $error  = array_merge($reqAll,$validator->errors()->all());
                $message    = ['status' => 'failed', 'title' => 'Gagal Input', 'message' => 'Anda gagal membuat Data Departemen baru ! '.$error[0]];
                return redirect()->route('aset-master', ['modul' => 'departemen'])->with('NoticeSession',$message);
            else:
                #dd('OK');

                try {
                    $ORM = new ORMDepartemen();
                    $ORM->kode_departemen   = $var['kode_departemen'];
                    $ORM->nama_departemen   = strtolower($var['nama_departemen']);
                    $ORM->nama_ruangan      = strtolower($var['nama_ruangan']);
                    $ORM->gedung            = strtolower($var['gedung']);
                    $ORM->waktu_buat        = Carbon::now();
                    $ORM->clog              = $browser . '|' . $request->ip();
                    $ORM->save();
                }catch (QueryException $e){
                    $message    = ['status' => 'failed', 'title' => 'Gagal Input', 'message' => 'Anda gagal membuat Data Departemen baru ! '.$e->getCode(), $e->getCode()];
                    return redirect()->route('aset-master', ['modul' => 'departemen'])->with('NoticeSession',$message);
                }
                $message    = ['status' => 'success', 'title' => 'Sukses Input', 'message' => 'Sukses! Anda berhasil membuat Data Departemen baru !'];
                return redirect()->route('aset-master', ['modul' => 'departemen'])->with('NoticeSession',$message);
            endif;
        }

        public function ProsesUbahDepartemen(Request $request){
            $var        = [];
            $rules      = [];
            $temp       = [];
            $name       = 'ubahDepartemen';
            $variable   = Input::get($name);
            $reqAll     = $request->all();
            $browser    = $_SERVER['HTTP_USER_AGENT'];

            $permission = [
                0   => ['kode_departemen', 'required|Between:3,6|unique:oracle.STAE_ASET.tbl_departemen,kode_departemen,'.$variable[1].',kode_departemen'],
                1   => ['id_departemen', 'required'],
                2   => ['nama_departemen', 'required|Between:3,200'],
                3   => ['nama_ruangan', 'required|Between:3,200'],
                4   => ['gedung', 'required|Between:3,100']
            ];

            foreach($variable as $keyVar):
                $temp[] = $keyVar;
            endforeach;
            for($i=0;$i<count($temp);$i++):
                $rules[$permission[$i][0]]  = $permission[$i][1];
                $var[$permission[$i][0]]    = $temp[$i];
            endfor;

            $validator  = Validator::make($var,$rules);
            if($validator->fails()):
                $error  = array_merge($reqAll,$validator->errors()->all());
                $message    = ['status' => 'failed', 'title' => 'Gagal Ubah', 'message' => 'Anda gagal memperbarui Data Departemen ! '.$error[0]];
                return redirect()->route('aset-master', ['modul' => 'departemen'])->with('NoticeSession',$message);
            else:
                try {
                    $updates        = [
                        'nama_departemen'   => strtolower($var['nama_departemen']),
                        'kode_departemen'   => (string) $var['kode_departemen'],
                        'nama_ruangan'      => strtolower($var['nama_ruangan']),
                        'gedung'            => strtolower($var['gedung']),
                        'waktu_ubah'        => Carbon::now(),
                        'clog'              => $browser . '|' . $request->ip()
                    ];
                    ORMDepartemen::where('kode_departemen',$variable[1])->update($updates);
                }catch (QueryException $e){
                    $message    = ['status' => 'failed', 'title' => 'Gagal Memperbarui', 'message' => 'Anda gagal memperbarui Data Departemen ! '.$e->getCode(), $e->getCode()];
                    return redirect()->route('aset-master', ['modul' => 'departemen'])->with('NoticeSession',$message);
                }
                $message    = ['status' => 'success', 'title' => 'Sukses Memperbarui', 'message' => 'Sukses! Anda berhasil memperbarui Data Departemen !'];
                return redirect()->route('aset-master', ['modul' => 'departemen'])->with('NoticeSession',$message);
            endif;
        }

        public function ProsesHapusDepartemen(Request $request){
            $reqAll     = $request->all();
            $id         = $reqAll['id'];
            try {
                ORMDepartemen::where('kode_departemen',$id)->update(['status' => '0', 'waktu_ubah' => Carbon::now()]);
            }catch (QueryException $e){
                $message    = ['status' => 'failed', 'title' => 'Gagal Hapus', 'message' => 'Gagal menghapus Data Vendor ! '.$e->getMessage(), $e->getCode()];
                return redirect()->route('aset-master', ['modul' => 'departemen'])->with('NoticeSession',$message);
            }
            $message    = ['status' => 'success', 'title' => 'Sukses Hapus', 'message' => 'Sukses! Menghapus Data Vendor !'];
            return redirect()->route('aset-master', ['modul' => 'departemen'])->with('NoticeSession',$message);
        }

    }