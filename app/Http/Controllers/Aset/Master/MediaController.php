<?php

    /*
    |--------------------------------------------------------------------------
    | MediaController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : MediaController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\Aset\Master;

    use App\Http\Controllers\STAEController,
        Illuminate\Http\Request;
    use App\Support\Model\ORMDetailBarang;
    use Carbon\Carbon;
    use Illuminate\Database\QueryException;
    use Illuminate\Support\Facades\Input;
    use Illuminate\Support\Facades\Validator;

    class MediaController extends STAEController {

        private $attr   = ['name-data' => 'Media', 'name-function' => 'media', 'name-modul' => 'master', 'name-primary' => 'kode_barang'];
        public function index(){
            $data   = [
                'titleDoc'          => 'Data ' . $this->attr['name-data'],
                'tahunCopyright'    => parent::year(),
                'field'             => $this->attr['name-data'],
                'modul'             => $this->attr['name-function']
            ];
            return view($this->defaultAset(), $data);
        }

        public function ProsesTambahMediaBarang(Request $request){
            $kb         = Input::get('image-kb');
            $input      = Input::file('media-gambar');
            $files      = ['media-gambar' => $input];
            $fcount     = count($input);
            $browser    = $_SERVER['HTTP_USER_AGENT'];

            $uploadcount    = 0;
            $file           = $files['media-gambar'];
            for($i=0;$i<$fcount;$i++):
                $rules          = ['file' => 'required|image|mimes:jpeg,png,jpg|max:2048'];
                $validator      = Validator::make(array('file'=> $file[$i]),$rules);
                if($validator->passes()):
                    $indexes        = str_pad($i+1, 2, '0', STR_PAD_LEFT);
                    $filename       = $kb . '-'.$indexes.'.'.$file[$i]->getClientOriginalExtension();
                    try {
                        $ORM                = new ORMDetailBarang();
                        $ORM->no_urut       = $i+1;
                        $ORM->gambar        = $filename;
                        $ORM->kode_barang   = $kb;
                        $ORM->waktu_buat    = Carbon::now();
                        $ORM->clog          = $browser . '|' . $request->ip();
                        $upload_success     = $file[$i]->move(public_path('upload'), $filename);
                        $ORM->save();
                    }catch (QueryException $e){
                        $message    = ['status' => 'failed', 'title' => 'Gagal Input', 'message' => 'Anda gagal membuat Data '.$this->attr['name-data'].' baru ! '.$e->getMessage(), $e->getCode()];
                        return redirect()->route('aset-master', ['modul' => $this->attr['name-function']])->with('NoticeSession',$message);
                    }
                    $uploadcount    += $i;
                endif;
            endfor;
            if($uploadcount == $fcount):
                $message    = ['status' => 'success', 'title' => 'Sukses Input', 'message' => 'Sukses! Anda berhasil membuat Data '.$this->attr['name-data'].' baru !'];
                return redirect()->route('aset-master', ['modul' => 'barang'])->with('NoticeSession',$message);
            else:
                $message    = ['status' => 'success', 'title' => 'Sukses Input', 'message' => 'Sukses! Anda berhasil membuat Data '.$this->attr['name-data'].' baru !'];
                return redirect()->route('aset-master', ['modul' => 'barang'])->with('NoticeSession',$message);
            endif;
        }

    }