<?php

    /*
    |--------------------------------------------------------------------------
    | CetakController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : CetakController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\Aset\Master;

    use App\Http\Controllers\STAEController;
    use App\Support\Library\LibraryData;
    use App\Support\Model\ORMBarangSeri;
    use Illuminate\Database\QueryException;
    use Illuminate\Support\Facades\Crypt;

    class CetakController extends STAEController {

        public function cetaknoseri($idcrypt){
            if($idcrypt == 'all'):
                $SQL    = LibraryData::AmbilDataSeriBarang('PRINT');
                $data   = $SQL->get();
                foreach($data as $key => $val):
                    echo '|-------------------------------------------------------------------------|<br/>';
                    echo '|&nbsp;&nbsp;' . $val->no_seri_aset . ' | ' . $val->no_seri_rdtl . ' | ' . $val->no_seri_alat . '&nbsp;&nbsp;&nbsp;|<br/>';
                endforeach;
                echo '|-------------------------------------------------------------------------|<br/>';

                try {
                    ORMBarangSeri::where('status','3')->update(['status' => '2']);
                } catch (QueryException $e){
                    echo $e->getMessage();
                }

            else:
                $decrypt    = Crypt::decrypt($idcrypt);
                $jsondec    = json_decode($decrypt, true);
                $SQL        = ORMBarangSeri::where('no_seri_aset',$jsondec[0]);
                if($SQL->get()[0]->status == 2):
                    echo 'Anda sudah mencetak label';
                else:
                    echo '|-------------------------------------------------------------------------|<br/>';
                    echo '|&nbsp;&nbsp;' . $jsondec[0] . ' | ' . $jsondec[1] . ' | ' . $jsondec[2] . '&nbsp;&nbsp;&nbsp;|<br/>';
                    echo '|-------------------------------------------------------------------------|<br/>';

                    try {
                        ORMBarangSeri::where('no_seri_aset',$jsondec[0])->update(['status' => '2']);
                    } catch (QueryException $e){
                        echo $e->getMessage();
                    }
                endif;
            endif;
        }

    }