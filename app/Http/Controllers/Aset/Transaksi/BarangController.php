<?php

    /*
    |--------------------------------------------------------------------------
    | BarangController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : BarangController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\Aset\Transaksi;

    use App\Http\Controllers\STAEController,
        Illuminate\Http\Request;
    use App\Support\Library\LibraryData;
    use App\Support\Library\LibraryOCIData;
    use App\Support\Model\ORMBarangDMasuk;
    use App\Support\Model\ORMBarangHMasuk;
    use Carbon\Carbon;
    use Illuminate\Database\QueryException;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Input;
    use Illuminate\Support\Facades\Validator;

    class BarangController extends STAEController {

        private $attr   = ['name-data' => 'Barang Masuk', 'name-function' => 'barang-masuk', 'name-modul' => 'transaksi', 'name-primary' => 'no_bukti'];
        public function index(){
            $now    = Carbon::now();
            $data   = [
                'titleDoc'          => [
                    'barang-masuk'  => 'Input Barang Masuk',
                    'barang-keluar' => 'Input Barang Keluar'
                ],
                'nobuktimasuk'      => $this->AmbilNoBukti('IN',$now->format('Y, ym')),
                'nobuktikeluar'     => $this->AmbilNoBukti('OUT',$now->format('Y, ym')),
                'tanggalhariini'    => $now->format('d / F / Y'),
                'tahunCopyright'    => parent::year()
            ];
            return view($this->defaultAset(), $data);
        }

        public function ProsesBarangMasuk(Request $request){
            $param      = $request->all();
            $variable   = Input::get('header-masuk');
            $var        = [];
            $rules      = [];
            $temp       = [];
            $hmasuk     = $param['header-masuk'];
            $dmasuk     = $param['data-masuk'];
            $browser    = $_SERVER['HTTP_USER_AGENT'];
            $temp       = [];

            $parseDate  = Carbon::createFromFormat('d / F / Y', $hmasuk[1]);
            $hmasuk[1]  = $parseDate->format('Y-m-d');

            $rhmasuk    = [
                0   => 'required|Between:3,20',
                1   => 'required|date',
                2   => 'required|Between:3,10',
                3   => 'required|Between:3,10',
                4   => 'required|Between:3,220'
            ];

            $nobukti    = $this->AmbilNoBukti($hmasuk[1]);

            $temp   = [];
            foreach($dmasuk as $key => $value):
                $temp[$key] = [
                    'no_bukti'      => $nobukti,
                    'no_urut'       => $value[0],
                    'kode_barang'   => $value[1],
                    'harga'         => str_replace('$ ','',$value[5]),
                    'jumlah'        => $value[4]
                ];
            endforeach;

            #dd($temp);

            $validator  = Validator::make($hmasuk,$rhmasuk);
            if($validator->fails()):
                $error  = array_merge($reqAll,$validator->errors()->all());
                $message    = ['status' => 'failed', 'title' => 'Gagal Ubah', 'message' => 'Anda gagal memperbarui Transaksi '.$this->attr['name-data'].' ! '.$error[0]];
                return redirect()->route('aset-transaksi', ['modul' => $this->attr['name-function']])->with('NoticeSession',$message);
            else:
                try {
                    $ORMH                   = new ORMBarangHMasuk();
                    $ORMD                   = new ORMBarangDMasuk();

                    $ORMH->no_bukti         = $nobukti;
                    $ORMH->tanggal          = $hmasuk[1];
                    $ORMH->kode_vendor      = $hmasuk[2];
                    $ORMH->kode_departemen  = $hmasuk[3];
                    $ORMH->keterangan       = $hmasuk[4];
                    $ORMH->waktu_buat       = Carbon::now();
                    $ORMH->log              = $browser . '|' . $request->ip();

                    $ORMH->save();
                    DB::table('tbl_dmasuk')->insert($temp);

                } catch (QueryException $e){
                    $message    = ['status' => 'failed', 'title' => 'Gagal Input', 'message' => 'Anda gagal membuat Data '.$this->attr['name-data'].' baru ! '.$e->getCode() . ':' . $e->getMessage(), $e->getCode()];
                    return redirect()->route('aset-transaksi', ['modul' => $this->attr['name-function']])->with('NoticeSession',$message);
                }
                $message    = ['status' => 'success', 'title' => 'Sukses Input', 'message' => 'Sukses! Anda berhasil membuat Data '.$this->attr['name-data'].' baru! NB : '.$nobukti];
                return redirect()->route('aset-transaksi', ['modul' => $this->attr['name-function']])->with('NoticeSession',$message);
            endif;
        }

        public function ProsesBarangKeluar(Request $request){
            dd($request->all());
        }

        private function AmbilNoBukti($type='IN',$time='NOW'){
            return LibraryData::AmbilNoBukti($type,$time);
        }

    }