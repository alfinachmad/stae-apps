<?php

    /*
    |--------------------------------------------------------------------------
    | MutasiController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : DepartemenController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\Aset;

    use App\Http\Controllers\STAEController;

    class MutasiController extends STAEController {

        public function index(){
            $data   = [
                'titleDoc'          => 'Data Mutasi',
                'tahunCopyright'    => parent::year()
            ];
            return view($this->defaultAset(), $data);
        }

        public function tambah(){
            $data   = [
                'titleDoc'          => 'Input Data Mutasi',
                'tahunCopyright'    => parent::year()
            ];
            return view($this->defaultAset(), $data);
        }

    }