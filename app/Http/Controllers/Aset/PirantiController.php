<?php

    /*
    |--------------------------------------------------------------------------
    | PirantiController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : PirantiController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\Aset;

    use App\Http\Controllers\STAEController,
        Carbon\Carbon;

    class PirantiController extends STAEController {

        public function index(){
            $data   = [
                'tahunCopyright' => Carbon::createFromDate()->format('Y')
            ];
            return view($this->defaultAset(), $data);
        }

        public function ubah(){
            return __FUNCTION__;
        }

        public function hapus(){
            return __FUNCTION__;
        }

    }