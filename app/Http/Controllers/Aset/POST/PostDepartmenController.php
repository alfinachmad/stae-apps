<?php

    /*
    |--------------------------------------------------------------------------
    | PostDepartemenController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : PostDepartemenController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\Aset\POST;

    use App\Http\Controllers\STAEController,
        Illuminate\Http\Request;

    class PostDepartmenController extends STAEController {

        public function PostTambahDepartemen(Request $request){

        }

        public function PostUbahDepartemen(Request $request){

        }

        public function PostHapusDepartemen(Request $request){

        }

    }