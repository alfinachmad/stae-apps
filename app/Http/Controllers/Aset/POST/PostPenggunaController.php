<?php

    /*
    |--------------------------------------------------------------------------
    | PostPenggunaController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : PostPenggunaController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\Aset\POST;

    use App\Http\Controllers\STAEController,
        App\Support\Model\ORMPengguna,
        Carbon\Carbon,
        Illuminate\Support\Facades\Crypt,
        Illuminate\Support\Facades\Input,
        Illuminate\Support\Facades\Validator,
        Illuminate\Database\QueryException,
        Illuminate\Http\Request;

    class PostPenggunaController extends STAEController {

        public function PostTambahPengguna(Request $request){
            $var        = [];
            $rules      = [];
            $temp       = [];
            $name       = 'tambahPengguna';
            $variable   = Input::get($name);
            $reqAll     = $request->all();
            $browser    = $_SERVER['HTTP_USER_AGENT'];

            #dd($variable);

            $permission = [
                0   => ['nama_lengkap', 'required|Between:3,75'],
                1   => ['email', 'unique:oracle.STAE_ASET.tbl_pengguna,email'],
                2   => ['password', 'required|Between:3,16'],
                3   => ['tipe_user', 'required|Between:1,1']
            ];
            $var['_token']  = $reqAll['_token'];

            foreach($variable as $keyVar):
                $temp[] = $keyVar;
            endforeach;
            for($i=0;$i<count($temp);$i++):
                $rules[$permission[$i][0]]  = $permission[$i][1];
                $var[$permission[$i][0]]    = $temp[$i];
            endfor;

            $validator  = Validator::make($var,$rules);
            if($validator->fails()):
                $error  = array_merge($reqAll,$validator->errors()->all());
                $message    = ['status' => 'failed', 'title' => 'Gagal Input', 'message' => 'Anda gagal membuat pengguna baru ! '.$error[0]];
                return redirect()->route('aset-pengaturan', ['modul' => 'pengguna'])->with('NoticeSession',$message);
            else:
                #dd('OK');

                try {
                    $ORMPengguna = new ORMPengguna();
                    $ORMPengguna->kode_pengguna    = strtoupper(substr($var['nama_lengkap'],0,4).str_random(3)).'-'.rand(1,99);
                    $ORMPengguna->nama_lengkap     = strtolower($var['nama_lengkap']);
                    $ORMPengguna->nama_pengguna    = strtoupper(substr($var['nama_lengkap'],0,4).str_random(4));
                    $ORMPengguna->tipe_pengguna    = $var['tipe_user'];
                    $ORMPengguna->email            = strtolower($var['email']);
                    $ORMPengguna->kata_kunci       = Crypt::encrypt($var['password']);
                    $ORMPengguna->waktu_buat       = Carbon::now();
                    $ORMPengguna->dibuat_oleh      = 9;
                    $ORMPengguna->clog              = $browser . '|' . $request->ip();
                    $ORMPengguna->save();
                }catch (QueryException $e){
                    $message    = ['status' => 'failed', 'title' => 'Gagal Input', 'message' => 'Anda gagal membuat pengguna baru ! '.$e->getMessage(), $e->getCode()];
                    return redirect()->route('aset-pengaturan', ['modul' => 'pengguna'])->with('NoticeSession',$message);
                }
                $message    = ['status' => 'success', 'title' => 'Sukses Input', 'message' => 'Sukses! Anda berhasil membuat pengguna baru !'];
                return redirect()->route('aset-pengaturan', ['modul' => 'pengguna'])->with('NoticeSession',$message);
            endif;
        }

        public function PostUbahPengguna(Request $request){
            $var        = [];
            $rules      = [];
            $temp       = [];
            $name       = 'ubahPengguna';
            $variable   = Input::get($name);
            $reqAll     = $request->all();
            $browser    = $_SERVER['HTTP_USER_AGENT'];

            $permission = [
                0   => ['nama_pengguna', 'required'],
                1   => ['id_pengguna', 'required'],
                2   => ['nama_lengkap', 'required|Between:3,75'],
                3   => ['email', 'sometimes|required|email|unique:oracle.STAE_ASET.tbl_pengguna,email,'.$variable[1].',kode_pengguna'],
                4   => ['tipe_user', 'required|Between:1,1']
            ];

            foreach($variable as $keyVar):
                $temp[] = $keyVar;
            endforeach;
            for($i=0;$i<count($temp);$i++):
                $rules[$permission[$i][0]]  = $permission[$i][1];
                $var[$permission[$i][0]]    = $temp[$i];
            endfor;

            $validator  = Validator::make($var,$rules);
            if($validator->fails()):
                $error  = array_merge($reqAll,$validator->errors()->all());
                $message    = ['status' => 'failed', 'title' => 'Gagal Ubah', 'message' => 'Anda gagal memperbarui data pengguna ! '.$error[0]];
                return redirect()->route('aset-pengaturan', ['modul' => 'pengguna'])->with('NoticeSession',$message);
            else:
                try {
                    $updates        = [
                        'nama_lengkap'  => strtolower($var['nama_lengkap']),
                        'tipe_pengguna' => $var['tipe_user'],
                        'email'         => strtolower($var['email']),
                        'waktu_ubah'    => Carbon::now(),
                        'clog'          => $browser . '|' . $request->ip()
                    ];
                    ORMPengguna::where('nama_pengguna',$variable[0])->update($updates);
                }catch (QueryException $e){
                    $message    = ['status' => 'failed', 'title' => 'Gagal Memperbarui', 'message' => 'Anda gagal memperbarui data pengguna ! '.$e->getCode(), $e->getCode()];
                    return redirect()->route('aset-pengaturan', ['modul' => 'pengguna'])->with('NoticeSession',$message);
                }
                $message    = ['status' => 'success', 'title' => 'Sukses Memperbarui', 'message' => 'Sukses! Anda berhasil memperbarui data pengguna !'];
                return redirect()->route('aset-pengaturan', ['modul' => 'pengguna'])->with('NoticeSession',$message);
            endif;
        }

        public function PostHapusPengguna(Request $request){
            $reqAll     = $request->all();
            $id         = $reqAll['id'];
            try {
                ORMPengguna::where('nama_pengguna',$id)->update(['status' => '0', 'waktu_ubah' => Carbon::now()]);
            }catch (QueryException $e){
                $message    = ['status' => 'failed', 'title' => 'Gagal Hapus', 'message' => 'Gagal menghapus data ! '.$e->getMessage(), $e->getCode()];
                return redirect()->route('aset-pengaturan', ['modul' => 'pengguna'])->with('NoticeSession',$message);
            }
            $message    = ['status' => 'success', 'title' => 'Sukses Hapus', 'message' => 'Sukses! Menghapus data pengguna !'];
            return redirect()->route('aset-pengaturan', ['modul' => 'pengguna'])->with('NoticeSession',$message);
        }

        public function PostProsesMasuk(Request $request){
            $reqAll     = $request->all();
            $permission = [
                'username'  => 'required|min:1',
                'password'  => 'required|min:1',
            ];

            $validator  = Validator::make($request->all(), $permission);            
            if($validator->fails()):
                return redirect()->route('backend_signin');
            else:
                // SELECT id_pengguna,nama_pengguna,nama_lengkap,email,tipe_pengguna,kata_kunci FROM tbl_pengguna WHERE status = '1' AND (nama_pengguna = '' OR email = '');
                $users  = ORMPengguna::where('status','1')
                    ->where('nama_pengguna', $reqAll['username'])
                    ->orWhere('email', $reqAll['username'])
                    ->select('kode_pengguna AS ipengguna','nama_lengkap AS nlengkap','email AS mail','tipe_pengguna as tpengguna','nama_pengguna AS npengguna','kata_kunci AS passwd')
                    ->get();

                if($users->count() > 0):
                    $users      = $users[0];
                    $password   = $users->passwd;
                    try {
                        if(Crypt::decrypt($password) == $reqAll['password']):
                            ORMPengguna::where('nama_pengguna',$users->npengguna)->update(['terakhir_login' => Carbon::now()]);
                            $dataSession    = [];
                            $dataSession['username']    = $users->nlengkap;
                            $dataSession['authorize']   = $users->tpengguna;
                            $dataSession['email']       = $users->mail;
                            $request->session()->put(SESSION_DEFAULT,
                                Crypt::encrypt(json_encode($dataSession))
                            );
                        else:
                            $message    = ['status' => 'failed', 'title' => 'Gagal Login', 'message' => 'Gagal! Kata Kunci tidak sesuai !'];
                            return redirect()->route('stae-sso', ['modul' => 'masuk'])->with('NoticeSessionLogin',$message);
                        endif;
                    }catch (QueryException $e){
                        $message    = ['status' => 'failed', 'title' => 'Gagal Login', 'message' => 'Nama Pengguna atau Kata Kunci Salah ! '.$e->getMessage(), $e->getCode()];
                        return redirect()->route('stae-sso', ['modul' => 'masuk'])->with('NoticeSessionLogin',$message);
                    }
                    return redirect()->route('aset-default');
                else:
                    $message    = ['status' => 'failed', 'title' => 'Gagal Login', 'message' => 'Gagal! Nama Pengguna tidak tersedia!'];
                    return redirect()->route('stae-sso', ['modul' => 'masuk'])->with('NoticeSessionLogin',$message);
                endif;
            endif;
        }

    }