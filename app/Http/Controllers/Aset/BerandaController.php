<?php

    /*
    |--------------------------------------------------------------------------
    | BerandaController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : BerandaController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\Aset;

    use App\Http\Controllers\STAEController;

    class BerandaController extends STAEController {


        public function beranda(){
            $data   = [
                'titleDoc'          => 'Beranda',
                'tahunCopyright'    => parent::year()
            ];
            return view($this->defaultAset(), $data);
        }

    }