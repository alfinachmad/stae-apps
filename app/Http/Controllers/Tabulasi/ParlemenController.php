<?php

    /*
    |--------------------------------------------------------------------------
    | ParlemenController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : ParlemenController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\Tabulasi;

    use App\Http\Controllers\STAEController,
        App\Support\Library\LibraryOCIData,
        Illuminate\Support\Facades\Cache,
        Illuminate\Support\Facades\Storage;

    class ParlemenController extends STAEController {

        private $statusdev  = false;
        
        public function hasil2012(){
            $year   = '2012';
            $data   = [
                'title_page'    => parent::defaultSubTitle(),
                'subtitle'      => self::title('PR') . $year,
                'municipiu'     => LibraryOCIData::AmbilDataNamaDistrik()
            ];
            return view($this->defaultNonAset(), $data);
        }

        public function hasil2017(){
            $year       = '2017';

            $cache      = '';
            $fileJ      = '';
            if(Cache::has(parent::defaultCacheName())):
                $cache  = Cache::get(parent::defaultCacheName());
                $fileJ  = Storage::get('STAE');
            endif;
            $fileJSON   = json_decode($fileJ, true);
            $lastPATH   = explode('/', request()->path());

            $data       = $fileJSON;
            $originData = (end($lastPATH) != 'livetable' ? $data : LibraryOCIData::AmbilDataParlemen());
            $bindings   = [
                'title_page'    => parent::defaultSubTitle(),
                'subtitle'      => self::title('PR') . $year,
                'dtdistrik'     => ['origin' => $data['distrik'], 'json' => json_encode($data['distrik'], JSON_NUMERIC_CHECK)],
                'dthasil'       => ['origin' => $data['hasil'], 'json' => json_encode($data['hasil'], JSON_NUMERIC_CHECK)],
                'dthasilpartai' => ['origin' => $data['partai'], 'json' => json_encode($data['partai'], JSON_NUMERIC_CHECK)],
                'dttotnasional' => $data['tnasional'],
                'dtstatusdev'   => $this->statusdev,
                'dt'            => $originData,
                'dtlstgupd'     => $data['lastupd'],
                'dtroute'       => parent::routeList()['route'],
                'dtapi'         => parent::routeList()['apiroute'],
                'dtsegment'     => request()->segment(4),
                'dtchart'       => [
                    'bar'   => [
                        'write'     => 'municipiu-bar',
                        'text'      => '[[NAMA_ACRY]] : <b> [[TOTAL]] </b> ',
                        'category'  => 'NAMA_ACRY',
                        'data'      => 'chartData[0][index][\'suara\'];',
                        'dataDef'   => 'chartData[0][0][\'suara\'];',
                        'validate'  => 'chart.validateData();',
                        'animate'   => 'chart.animateAgain();',
                        'apiURL'    => route('api-tabulasi-parlemen2017', ['tipe' => 'all'])
                    ],
                    'party' => [
                        'write'     => 'partai-bar',
                        'text'      => '[[NAMA]] : <b> [[TOTAL]] </b> ',
                        'category'  => 'NAMA',
                        'data'      => 'chartData[0][index];',
                        'dataDef'   => 'chartData[0][0];',
                        'validate'  => 'chart.validateData();',
                        'animate'   => 'chart.animateAgain();',
                        'apiURL'    => route('api-tabulasi-parlemen2017', ['tipe' => 'party'])
                    ],
                    'tabular' => [
                        'write'     => 'nasional-bar',
                        'text'      => '[[NAMA_ACRY]] : <b> [[TOTAL]] </b> ',
                        'category'  => 'NAMA_ACRY',
                        'data'      => '\'\';',
                        'dataDef'   => 'chartData[0];',
                        'validate'  => false,
                        'animate'   => false,
                        'apiURL'    => route('api-tabulasi-parlemen2017', ['tipe' => 'nasional'])
                    ]
                ]
            ];

            return view(parent::defaultNonAset(), $bindings);
        }

    }
