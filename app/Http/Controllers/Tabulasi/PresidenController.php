<?php

    /*
    |--------------------------------------------------------------------------
    | PresidenController.php
    |--------------------------------------------------------------------------
    | SECRETARIADO TÉCNICO DE ADMINISTRAÇÃO ELEITORAL (STAE) - REPUBLIC OF EAST TIMOR
    | @filename : PresidenController.php
    | @author   : alfin.apps@outlook.com
    | @since    : April 2017
    | @codename : kalbuadi - Operasi Umi-Tuti
    |
    */

    namespace App\Http\Controllers\Tabulasi;

    use App\Http\Controllers\STAEController;

    class PresidenController extends STAEController {

        public function hasil2012($periode='R1'){
            return 'Hasil Presiden '.$periode.' 2012';
        }

        public function hasil2017($periode='R1'){
            return 'Hasil Presiden '.$periode.' 2017';
        }

    }