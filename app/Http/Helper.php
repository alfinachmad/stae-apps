<?php

    function getMicrotime(){
        return time() * 1000;
    }

    function getContentExternal($param) {
        if(is_array($param) AND !empty($param)):
            #$regex  = preg_match_all('/{(.*?)}/', $param['url'], $matches);
            $url        = $param['url'];
            $key        = $param['key'];
            $tipe       = $param['tipe'];
            $return     = str_replace('{key}', $key, str_replace('{tipe}', $tipe, $url));
            return $return;
        endif;
    }